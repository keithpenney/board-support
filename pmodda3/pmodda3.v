module pmodda3(inout [3:0] PMOD
,input csn
,input din
,input ldacn
,input sclk
);
assign PMOD={sclk,ldacn,din,csn};
endmodule
