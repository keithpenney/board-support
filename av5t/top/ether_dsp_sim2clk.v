// Synchrotrone Trieste AD (FLLRF)
`define SERDES_5T
`define FLLRF
`define HAS_SYSMON
`define CAVITY_SIM
//`define SAME_CLOCKS
`define MY_IP {8'd128, 8'd3, 8'd128, 8'd173}  // 128.3.128.173 (lrd4) Bldg. 71
`define MY_MAC 48'h00105ad155b3               // fictitious
`include "ether_fllrf.vh"
