// Synchrotrone Trieste AD (FLLRF)
`define SERDES_5T
`define FLLRF
`define HAS_SYSMON
//`define SAME_CLOCKS

//`define MY_IP {8'd192, 8'd168, 8'd111, 8'd73}  // 192.168.111.73 Trieste
//`define MY_IP {8'd10, 8'd5, 8'd4, 8'd90}       // 10.5.4.90 Trieste
`define MY_IP {8'd128, 8'd3, 8'd128, 8'd173}     // 128.3.128.173 (lrd4) Bldg. 46

`define MY_MAC 48'h00105ad155b3                  // fictitious
`include "ether_fllrf.vh"
