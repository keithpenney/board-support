`timescale 1ns / 100ps

module lcdacpcie(ipciex8.pin pcie,ilcdac.hw lcdacpcie,con14 J1,con12 J2);
lcdacpcie_v lcdacpcie_v(
`include "pcie_vinst.vh"
,.lcdacpcie(lcdacpcie)
,.J1_p1(J1.p1),.J1_p2(J1.p2),.J1_p3(J1.p3),.J1_p4(J1.p4),.J1_p5(J1.p5),.J1_p6(J1.p6),.J1_p7(J1.p7),.J1_p8(J1.p8),.J1_p9(J1.p9),.J1_p10(J1.p10),.J1_p11(J1.p11),.J1_p12(J1.p12),.J1_p13(J1.p13),.J1_p14(J1.p14)
,.J2_p1(J2.p1),.J2_p2(J2.p2),.J2_p3(J2.p3),.J2_p4(J2.p4),.J2_p5(J2.p5),.J2_p6(J2.p6),.J2_p7(J2.p7),.J2_p8(J2.p8),.J2_p9(J2.p9),.J2_p10(J2.p10),.J2_p11(J2.p11),.J2_p12(J2.p12)
);
endmodule

module lcdacpcie_v(
`include "pcie_vport.vh"
,ilcdac.hw lcdacpcie
,inout J1_p1,inout J1_p2,inout J1_p3,inout J1_p4,inout J1_p5,inout J1_p6,inout J1_p7,inout J1_p8,inout J1_p9,inout J1_p10,inout J1_p11,inout J1_p12,inout J1_p13,inout J1_p14
,inout J2_p1,inout J2_p2,inout J2_p3,inout J2_p4,inout J2_p5,inout J2_p6,inout J2_p7,inout J2_p8,inout J2_p9,inout J2_p10,inout J2_p11,inout J2_p12
);
ipciex8 pcie();
con14 J1();
con12 J2();
`include "pcie_via.vh"
viapairs #(.WIDTH(14)) via_J1({J1.p1,J1_p1,J1.p2,J1_p2,J1.p3,J1_p3,J1.p4,J1_p4,J1.p5,J1_p5,J1.p6,J1_p6,J1.p7,J1_p7,J1.p8,J1_p8,J1.p9,J1_p9,J1.p10,J1_p10,J1.p11,J1_p11,J1.p12,J1_p12,J1.p13,J1_p13,J1.p14,J1_p14});
viapairs #(.WIDTH(12)) via_J2({J2.p1,J2_p1,J2.p2,J2_p2,J2.p3,J2_p3,J2.p4,J2_p4,J2.p5,J2_p5,J2.p6,J2_p6,J2.p7,J2_p7,J2.p8,J2_p8,J2.p9,J2_p9,J2.p10,J2_p10,J2.p11,J2_p11,J2.p12,J2_p12});
lcdacpcie_i lcdacpcie_i(.pcie(pcie.pin),.lcdacpcie(lcdacpcie),.J1(J1),.J2(J2));
endmodule

module lcdacpcie_i(ipciex8.pin pcie,ilcdac.hw lcdacpcie,con14 J1,con12 J2);
`include "lcdacpcie.vh"
endmodule
