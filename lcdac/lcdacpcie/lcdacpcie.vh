obufbus #(.WIDTH(8))
obufdpn(.O({pcie.A36,pcie.A40,pcie.A44,pcie.A48,pcie.A30,pcie.A26,pcie.A22,pcie.A17}),.I(lcdacpcie.dp_c2m_n));
obufbus #(.WIDTH(8))
obufdpp(.O({pcie.A35,pcie.A39,pcie.A43,pcie.A47,pcie.A29,pcie.A25,pcie.A21,pcie.A16}),.I(lcdacpcie.dp_c2m_p));
wire [7:0] flip=8'b01011010;

wire dac_sync_;
IBUFDS ibufds_dacsync(.I(J1.p14),.IB(J1.p13),.O(dac_sync_));
assign lcdacpcie.dac_sync = ~dac_sync_;
OBUFDS obufds_lmk_clkin0(.I(~lcdacpcie.lmk_clkin0),.O(J2.p2),.OB(J2.p1));
OBUFDS obufds_fmc_sysref(.I(~lcdacpcie.fmc_sysref),.O(J1.p11),.OB(J1.p9));
wire lmk_dclkout2_;
IBUFGDS ibufds2(.I(J1.p1),.IB(J1.p2),.O(lmk_dclkout2_));
assign lcdacpcie.lmk_dclkout2 = ~lmk_dclkout2_;
IBUFDS_GTE2 ibufds8(.I(pcie.A13),.IB(pcie.A14),.CEB(1'b0),.O(lcdacpcie.lmk_dclkout8),.ODIV2());
IBUFGDS ibufds3(.I(J2.p11),.IB(J2.p12),.O(lcdacpcie.lmk_sdclkout3));

obufbus #(.WIDTH(3))
obufdac(.O({J2.p10,J2.p8,J2.p6}),.I({lcdacpcie.dac_txen_vadj,~lcdacpcie.dac_reset,lcdacpcie.dac_sdenb}));
obufbus #(.WIDTH(3))
obuflmk(.O({J2.p3,J2.p7,J2.p9}),.I({lcdacpcie.lmk_reset,lcdacpcie.lmk_sync,lcdacpcie.lmk_cs}));
obufbus #(.WIDTH(2))
obuflmkdac(.O({J2.p4,J1.p10}),.I({lcdacpcie.lmk_dac_sck,lcdacpcie.lmk_dac_sdio}));
IBUF ibuf_dac_sdo(.O(lcdacpcie.dac_sdo),.I(J1.p12));
IBUF ibuf_lmk_sdo(.O(lcdacpcie.lmk_sdo),.I(J2.p5));
