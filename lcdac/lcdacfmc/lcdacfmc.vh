obufbus #(.WIDTH(8))
obufdpn(.O({fmcpin.B33,fmcpin.A35,fmcpin.B37,fmcpin.A39,fmcpin.A31,fmcpin.A27,fmcpin.A23,fmcpin.C3}),.I(lcdacfmc.dp_c2m_n));
obufbus #(.WIDTH(8))
obufdpp(.O({fmcpin.B32,fmcpin.A34,fmcpin.B36,fmcpin.A38,fmcpin.A30,fmcpin.A26,fmcpin.A22,fmcpin.C2}),.I(lcdacfmc.dp_c2m_p));
wire [7:0] flip=8'b01011010;

wire dac_sync_;
IBUFDS ibufds_dacsync(.I(fmcpin.H7),.IB(fmcpin.H8),.O(dac_sync_));
assign lcdacfmc.dac_sync = ~dac_sync_;
OBUFDS obufds_lmk_clkin0(.I(lcdacfmc.lmk_clkin0),.O(fmcpin.D8),.OB(fmcpin.D9));
OBUFDS obufds_fmc_sysref(.I(lcdacfmc.fmc_sysref),.O(fmcpin.G9),.OB(fmcpin.G10));
IBUFGDS ibufds2(.I(fmcpin.H4),.IB(fmcpin.H5),.O(lcdacfmc.lmk_dclkout2));
wire lmk_dclkout8_;
IBUFDS_GTE2 ibufds8(.I(fmcpin.D4),.IB(fmcpin.D5),.CEB(1'b0),.O(lmk_dclkout8_),.ODIV2());
assign lcdacfmc.lmk_dclkout8 = ~lmk_dclkout8_;
IBUFDS_GTE2 ibufds10(.I(fmcpin.B20),.IB(fmcpin.B21),.CEB(1'b0),.O(lcdacfmc.lmk_dclkout10),.ODIV2());
IBUFGDS ibufds3(.I(fmcpin.G2),.IB(fmcpin.G3),.O(lcdacfmc.lmk_sdclkout3));

obufbus #(.WIDTH(3))
obufdac(.O({fmcpin.D14,fmcpin.H16,fmcpin.G16}),.I({lcdacfmc.dac_txen_vadj,~lcdacfmc.dac_reset,lcdacfmc.dac_sdenb}));
obufbus #(.WIDTH(3))
obuflmk(.O({fmcpin.H10,fmcpin.D12,fmcpin.G15}),.I({lcdacfmc.lmk_reset,lcdacfmc.lmk_sync,lcdacfmc.lmk_cs}));
obufbus #(.WIDTH(2))
obuflmkdac(.O({fmcpin.D15,fmcpin.D18}),.I({lcdacfmc.lmk_dac_sck,lcdacfmc.lmk_dac_sdio}));
IBUF ibuf_dac_sdo(.O(lcdacfmc.dac_sdo),.I(fmcpin.D17));
IBUF ibuf_lmk_sdo(.O(lcdacfmc.lmk_sdo),.I(fmcpin.D11));

viapairs #(.WIDTH(8))
via_con14({
J1.p1,fmcpin.C22
,J1.p2,fmcpin.C23
,J1.p9,fmcpin.H35
,J1.p11,fmcpin.H34
,J1.p10,fmcpin.H37
,J1.p12,fmcpin.H38
,J1.p13,fmcpin.K20
,J1.p14,fmcpin.K19
});
viapairs #(.WIDTH(12))
via_con12({J2.p1,fmcpin.D21
,J2.p2,fmcpin.D20
,J2.p3,fmcpin.C10
,J2.p4,fmcpin.E7
,J2.p5,fmcpin.C11
,J2.p6,fmcpin.E6
,J2.p7,fmcpin.K8
,J2.p8,fmcpin.J6
,J2.p9,fmcpin.K7
,J2.p10,fmcpin.J7
,J2.p11,fmcpin.G27
,J2.p12,fmcpin.G28
});
