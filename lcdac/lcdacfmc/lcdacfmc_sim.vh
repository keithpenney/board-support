assign lcdacfmc.dp_c2m_p[0] = fmcpin.C2;
assign lcdacfmc.dp_c2m_n[0] = fmcpin.C3;
assign lcdacfmc.dp_c2m_p[1] = fmcpin.A22;
assign lcdacfmc.dp_c2m_n[1] = fmcpin.A23;
assign lcdacfmc.dp_c2m_p[2] = fmcpin.A26;
assign lcdacfmc.dp_c2m_n[2] = fmcpin.A27;
assign lcdacfmc.dp_c2m_p[3] = fmcpin.A30;
assign lcdacfmc.dp_c2m_n[3] = fmcpin.A31;
assign lcdacfmc.dp_c2m_p[4] = fmcpin.A38;
assign lcdacfmc.dp_c2m_n[4] = fmcpin.A39;
assign lcdacfmc.dp_c2m_p[5] = fmcpin.B36;
assign lcdacfmc.dp_c2m_n[5] = fmcpin.B37;
assign lcdacfmc.dp_c2m_p[6] = fmcpin.A34;
assign lcdacfmc.dp_c2m_n[6] = fmcpin.A35;
assign lcdacfmc.dp_c2m_p[7] = fmcpin.B32;
assign lcdacfmc.dp_c2m_n[7] = fmcpin.B33;
assign fmcpin.H7 = ~lcdacfmc.dac_sync;//swap
assign fmcpin.H8 = lcdacfmc.dac_sync;//swap
assign lcdacfmc.lmk_clkin0 = fmcpin.D8;//~D9
assign lcdacfmc.fmc_sysref = fmcpin.G9;//~G10
assign fmcpin.H4 = lcdacfmc.lmk_dclkout2;
assign fmcpin.H5 = ~lcdacfmc.lmk_dclkout2;
assign fmcpin.D4 = ~lcdacfmc.lmk_dclkout8;//swap
assign fmcpin.D5 = lcdacfmc.lmk_dclkout8;//swap
assign fmcpin.B20 = lcdacfmc.lmk_dclkout10;
assign fmcpin.B21 = ~lcdacfmc.lmk_dclkout10;
assign fmcpin.G2 = lcdacfmc.lmk_sdclkout3;
assign fmcpin.G3 = ~lcdacfmc.lmk_sdclkout3;
assign lcdacfmc.dac_txen_vadj = fmcpin.D14;
assign lcdacfmc.dac_reset = fmcpin.H16;
assign lcdacfmc.dac_sdenb = fmcpin.G16;
assign lcdacfmc.lmk_reset = fmcpin.H10;
assign lcdacfmc.lmk_sync = fmcpin.D12;
assign lcdacfmc.lmk_cs = fmcpin.G15;
assign lcdacfmc.lmk_dac_sck = fmcpin.D15;
assign lcdacfmc.lmk_dac_sdio = fmcpin.D18;
assign fmcpin.D17 = lcdacfmc.dac_sdo;
assign fmcpin.D11 = lcdacfmc.lmk_sdo;
/*
viapairs #(.WIDTH(8))
via_con14({
J1.p1,fmcpin.C22
,J1.p2,fmcpin.C23
,J1.p9,fmcpin.H35
,J1.p11,fmcpin.H34
,J1.p10,fmcpin.H37
,J1.p12,fmcpin.H38
,J1.p13,fmcpin.K20
,J1.p14,fmcpin.K19
});
viapairs #(.WIDTH(12))
via_con12({J2.p1,fmcpin.D21
,J2.p2,fmcpin.D20
,J2.p3,fmcpin.C10
,J2.p4,fmcpin.E7
,J2.p5,fmcpin.C11
,J2.p6,fmcpin.E6
,J2.p7,fmcpin.K8
,J2.p8,fmcpin.J6
,J2.p9,fmcpin.K7
,J2.p10,fmcpin.J7
,J2.p11,fmcpin.G27
,J2.p12,fmcpin.G28
});
*/
