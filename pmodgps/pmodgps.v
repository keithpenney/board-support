module pmodgps(inout [3:0] PMOD
,output J2_1
,input J2_2
,output f3d
,input rx
,output tx
,output pps
,input rstn
,output rtcm
);
assign {pps,tx,f3d}={PMOD[3],PMOD[2],PMOD[0]};
assign PMOD[1]=rx;
assign J2_1=rstn;
assign rtcm=J2_2;
endmodule
