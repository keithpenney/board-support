assign fpga.A12=fpga_a12;
assign fpga.A13=fpga_a13;
assign fpga.AA41=fpga_aa41;
assign fpga.AA42=fpga_aa42;
assign fpga.AB39=fpga_ab39;
assign fpga.AB40=fpga_ab40;
assign fpga.AC41=fpga_ac41;
assign fpga.AC42=fpga_ac42;
assign fpga.B39=fpga_b39;
assign fpga.B40=fpga_b40;
assign fpga.C41=fpga_c41;
assign fpga.C42=fpga_c42;
assign fpga.D11=fpga_d11;
assign fpga.D39=fpga_d39;
assign fpga.D40=fpga_d40;
assign fpga.E10=fpga_e10;
assign fpga.E11=fpga_e11;
assign fpga.E41=fpga_e41;
assign fpga.E42=fpga_e42;
assign fpga.E9=fpga_e9;
assign fpga.F39=fpga_f39;
assign fpga.F40=fpga_f40;
assign fpga.G11=fpga_g11;
assign fpga.G12=fpga_g12;
assign fpga.G41=fpga_g41;
assign fpga.G42=fpga_g42;
assign fpga.H34=fpga_h34;
assign fpga.H35=fpga_h35;
assign fpga.J41=fpga_j41;
assign fpga.J42=fpga_j42;
assign fpga.K34=fpga_k34;
assign fpga.K35=fpga_k35;
assign fpga.L14=fpga_l14;
assign fpga.L41=fpga_l41;
assign fpga.L42=fpga_l42;
assign fpga.M34=fpga_m34;
assign fpga.M35=fpga_m35;
assign fpga.N41=fpga_n41;
assign fpga.N42=fpga_n42;
assign fpga.P34=fpga_p34;
assign fpga.P35=fpga_p35;
assign fpga.R41=fpga_r41;
assign fpga.R42=fpga_r42;
assign fpga.T34=fpga_t34;
assign fpga.T35=fpga_t35;
assign fpga.U41=fpga_u41;
assign fpga.U42=fpga_u42;
assign fpga.V34=fpga_v34;
assign fpga.V35=fpga_v35;
assign fpga.W41=fpga_w41;
assign fpga.W42=fpga_w42;
assign fpga.Y34=fpga_y34;
assign fpga.Y35=fpga_y35;
assign fpga.Y39=fpga_y39;
assign fpga.Y40=fpga_y40;
assign fpga_a36=fpga.A36;
assign fpga_a37=fpga.A37;
assign fpga_aa36=fpga.AA36;
assign fpga_aa37=fpga.AA37;
assign fpga_an14=fpga.AN14;
assign fpga_ap14=fpga.AP14;
assign fpga_ap16=fpga.AP16;
assign fpga_ap21=fpga.AP21;
assign fpga_ar21=fpga.AR21;
assign fpga_au16=fpga.AU16;
assign fpga_av18=fpga.AV18;
assign fpga_av21=fpga.AV21;
assign fpga_aw12=fpga.AW12;
assign fpga_aw18=fpga.AW18;
assign fpga_ay16=fpga.AY16;
assign fpga_b26=fpga.B26;
assign fpga_ba19=fpga.BA19;
assign fpga_bb12=fpga.BB12;
assign fpga_c13=fpga.C13;
assign fpga_c36=fpga.C36;
assign fpga_c37=fpga.C37;
assign fpga_d12=fpga.D12;
assign fpga_d13=fpga.D13;
assign fpga_d14=fpga.D14;
assign fpga_e24=fpga.E24;
assign fpga_e25=fpga.E25;
assign fpga_e36=fpga.E36;
assign fpga_e37=fpga.E37;
assign fpga_f34=fpga.F34;
assign fpga_f35=fpga.F35;
assign fpga_g26=fpga.G26;
assign fpga_g36=fpga.G36;
assign fpga_g37=fpga.G37;
assign fpga_h38=fpga.H38;
assign fpga_h39=fpga.H39;
assign fpga_j23=fpga.J23;
assign fpga_j36=fpga.J36;
assign fpga_j37=fpga.J37;
assign fpga_k38=fpga.K38;
assign fpga_k39=fpga.K39;
assign fpga_l15=fpga.L15;
assign fpga_l24=fpga.L24;
assign fpga_l36=fpga.L36;
assign fpga_l37=fpga.L37;
assign fpga_m38=fpga.M38;
assign fpga_m39=fpga.M39;
assign fpga_n36=fpga.N36;
assign fpga_n37=fpga.N37;
assign fpga_p21=fpga.P21;
assign fpga_p38=fpga.P38;
assign fpga_p39=fpga.P39;
assign fpga_r36=fpga.R36;
assign fpga_r37=fpga.R37;
assign fpga_t38=fpga.T38;
assign fpga_t39=fpga.T39;
assign fpga_u36=fpga.U36;
assign fpga_u37=fpga.U37;
assign fpga_v38=fpga.V38;
assign fpga_v39=fpga.V39;
viapairs #(.WIDTH(374))
viapires ({
fpga_a10,fpga.A10
,fpga_a14,fpga.A14
,fpga_a15,fpga.A15
,fpga_a17,fpga.A17
,fpga_a18,fpga.A18
,fpga_a19,fpga.A19
,fpga_a20,fpga.A20
,fpga_a22,fpga.A22
,fpga_a23,fpga.A23
,fpga_a24,fpga.A24
,fpga_a25,fpga.A25
,fpga_a27,fpga.A27
,fpga_a28,fpga.A28
,fpga_a29,fpga.A29
,fpga_a30,fpga.A30
,fpga_a32,fpga.A32
,fpga_a9,fpga.A9
,fpga_ah18,fpga.AH18
,fpga_ah19,fpga.AH19
,fpga_ah20,fpga.AH20
,fpga_ah21,fpga.AH21
,fpga_ah24,fpga.AH24
,fpga_aj14,fpga.AJ14
,fpga_aj17,fpga.AJ17
,fpga_aj18,fpga.AJ18
,fpga_aj21,fpga.AJ21
,fpga_aj22,fpga.AJ22
,fpga_aj23,fpga.AJ23
,fpga_aj24,fpga.AJ24
,fpga_aj26,fpga.AJ26
,fpga_ak14,fpga.AK14
,fpga_ak15,fpga.AK15
,fpga_ak16,fpga.AK16
,fpga_ak17,fpga.AK17
,fpga_ak19,fpga.AK19
,fpga_ak20,fpga.AK20
,fpga_ak21,fpga.AK21
,fpga_ak22,fpga.AK22
,fpga_ak24,fpga.AK24
,fpga_ak25,fpga.AK25
,fpga_ak26,fpga.AK26
,fpga_al14,fpga.AL14
,fpga_al15,fpga.AL15
,fpga_al17,fpga.AL17
,fpga_al18,fpga.AL18
,fpga_al19,fpga.AL19
,fpga_al20,fpga.AL20
,fpga_al22,fpga.AL22
,fpga_al23,fpga.AL23
,fpga_al24,fpga.AL24
,fpga_al25,fpga.AL25
,fpga_am13,fpga.AM13
,fpga_am15,fpga.AM15
,fpga_am16,fpga.AM16
,fpga_am17,fpga.AM17
,fpga_am18,fpga.AM18
,fpga_am20,fpga.AM20
,fpga_am21,fpga.AM21
,fpga_am22,fpga.AM22
,fpga_am23,fpga.AM23
,fpga_am25,fpga.AM25
,fpga_am26,fpga.AM26
,fpga_an13,fpga.AN13
,fpga_an15,fpga.AN15
,fpga_an16,fpga.AN16
,fpga_an18,fpga.AN18
,fpga_an19,fpga.AN19
,fpga_an20,fpga.AN20
,fpga_an21,fpga.AN21
,fpga_an23,fpga.AN23
,fpga_an24,fpga.AN24
,fpga_an25,fpga.AN25
,fpga_an26,fpga.AN26
,fpga_ap10,fpga.AP10
,fpga_ap11,fpga.AP11
,fpga_ap12,fpga.AP12
,fpga_ap13,fpga.AP13
,fpga_ap17,fpga.AP17
,fpga_ap18,fpga.AP18
,fpga_ap19,fpga.AP19
,fpga_ap22,fpga.AP22
,fpga_ap23,fpga.AP23
,fpga_ap24,fpga.AP24
,fpga_ap26,fpga.AP26
,fpga_ar10,fpga.AR10
,fpga_ar11,fpga.AR11
,fpga_ar12,fpga.AR12
,fpga_ar14,fpga.AR14
,fpga_ar15,fpga.AR15
,fpga_ar16,fpga.AR16
,fpga_ar17,fpga.AR17
,fpga_ar19,fpga.AR19
,fpga_ar20,fpga.AR20
,fpga_ar22,fpga.AR22
,fpga_ar24,fpga.AR24
,fpga_ar25,fpga.AR25
,fpga_ar26,fpga.AR26
,fpga_at10,fpga.AT10
,fpga_at12,fpga.AT12
,fpga_at13,fpga.AT13
,fpga_at14,fpga.AT14
,fpga_at15,fpga.AT15
,fpga_at17,fpga.AT17
,fpga_at18,fpga.AT18
,fpga_at19,fpga.AT19
,fpga_at20,fpga.AT20
,fpga_at22,fpga.AT22
,fpga_at23,fpga.AT23
,fpga_at24,fpga.AT24
,fpga_at25,fpga.AT25
,fpga_au10,fpga.AU10
,fpga_au11,fpga.AU11
,fpga_au12,fpga.AU12
,fpga_au13,fpga.AU13
,fpga_au15,fpga.AU15
,fpga_au17,fpga.AU17
,fpga_au18,fpga.AU18
,fpga_au20,fpga.AU20
,fpga_au21,fpga.AU21
,fpga_au22,fpga.AU22
,fpga_au23,fpga.AU23
,fpga_au25,fpga.AU25
,fpga_av10,fpga.AV10
,fpga_av11,fpga.AV11
,fpga_av13,fpga.AV13
,fpga_av14,fpga.AV14
,fpga_av15,fpga.AV15
,fpga_av16,fpga.AV16
,fpga_av19,fpga.AV19
,fpga_av20,fpga.AV20
,fpga_av23,fpga.AV23
,fpga_av24,fpga.AV24
,fpga_av25,fpga.AV25
,fpga_av9,fpga.AV9
,fpga_aw11,fpga.AW11
,fpga_aw13,fpga.AW13
,fpga_aw14,fpga.AW14
,fpga_aw16,fpga.AW16
,fpga_aw17,fpga.AW17
,fpga_aw19,fpga.AW19
,fpga_aw21,fpga.AW21
,fpga_aw22,fpga.AW22
,fpga_aw23,fpga.AW23
,fpga_aw24,fpga.AW24
,fpga_aw9,fpga.AW9
,fpga_ay10,fpga.AY10
,fpga_ay11,fpga.AY11
,fpga_ay12,fpga.AY12
,fpga_ay14,fpga.AY14
,fpga_ay15,fpga.AY15
,fpga_ay17,fpga.AY17
,fpga_ay19,fpga.AY19
,fpga_ay20,fpga.AY20
,fpga_ay21,fpga.AY21
,fpga_ay22,fpga.AY22
,fpga_ay24,fpga.AY24
,fpga_ay25,fpga.AY25
,fpga_ay9,fpga.AY9
,fpga_b10,fpga.B10
,fpga_b11,fpga.B11
,fpga_b12,fpga.B12
,fpga_b13,fpga.B13
,fpga_b15,fpga.B15
,fpga_b16,fpga.B16
,fpga_b17,fpga.B17
,fpga_b18,fpga.B18
,fpga_b20,fpga.B20
,fpga_b21,fpga.B21
,fpga_b22,fpga.B22
,fpga_b23,fpga.B23
,fpga_b25,fpga.B25
,fpga_b27,fpga.B27
,fpga_b28,fpga.B28
,fpga_b30,fpga.B30
,fpga_b31,fpga.B31
,fpga_b32,fpga.B32
,fpga_ba10,fpga.BA10
,fpga_ba12,fpga.BA12
,fpga_ba13,fpga.BA13
,fpga_ba14,fpga.BA14
,fpga_ba15,fpga.BA15
,fpga_ba17,fpga.BA17
,fpga_ba18,fpga.BA18
,fpga_ba20,fpga.BA20
,fpga_ba22,fpga.BA22
,fpga_ba23,fpga.BA23
,fpga_ba24,fpga.BA24
,fpga_ba25,fpga.BA25
,fpga_ba9,fpga.BA9
,fpga_bb10,fpga.BB10
,fpga_bb11,fpga.BB11
,fpga_bb13,fpga.BB13
,fpga_bb15,fpga.BB15
,fpga_bb16,fpga.BB16
,fpga_bb17,fpga.BB17
,fpga_bb18,fpga.BB18
,fpga_bb20,fpga.BB20
,fpga_bb21,fpga.BB21
,fpga_bb22,fpga.BB22
,fpga_bb23,fpga.BB23
,fpga_bb25,fpga.BB25
,fpga_bb9,fpga.BB9
,fpga_c10,fpga.C10
,fpga_c11,fpga.C11
,fpga_c14,fpga.C14
,fpga_c15,fpga.C15
,fpga_c16,fpga.C16
,fpga_c18,fpga.C18
,fpga_c19,fpga.C19
,fpga_c20,fpga.C20
,fpga_c21,fpga.C21
,fpga_c23,fpga.C23
,fpga_c24,fpga.C24
,fpga_c25,fpga.C25
,fpga_c26,fpga.C26
,fpga_c28,fpga.C28
,fpga_c29,fpga.C29
,fpga_c30,fpga.C30
,fpga_c31,fpga.C31
,fpga_c9,fpga.C9
,fpga_d16,fpga.D16
,fpga_d17,fpga.D17
,fpga_d18,fpga.D18
,fpga_d19,fpga.D19
,fpga_d21,fpga.D21
,fpga_d22,fpga.D22
,fpga_d23,fpga.D23
,fpga_d24,fpga.D24
,fpga_d26,fpga.D26
,fpga_d27,fpga.D27
,fpga_d28,fpga.D28
,fpga_d29,fpga.D29
,fpga_d9,fpga.D9
,fpga_e12,fpga.E12
,fpga_e14,fpga.E14
,fpga_e15,fpga.E15
,fpga_e16,fpga.E16
,fpga_e17,fpga.E17
,fpga_e19,fpga.E19
,fpga_e20,fpga.E20
,fpga_e21,fpga.E21
,fpga_e22,fpga.E22
,fpga_e26,fpga.E26
,fpga_e27,fpga.E27
,fpga_e29,fpga.E29
,fpga_e30,fpga.E30
,fpga_f10,fpga.F10
,fpga_f12,fpga.F12
,fpga_f13,fpga.F13
,fpga_f14,fpga.F14
,fpga_f15,fpga.F15
,fpga_f17,fpga.F17
,fpga_f18,fpga.F18
,fpga_f19,fpga.F19
,fpga_f20,fpga.F20
,fpga_f22,fpga.F22
,fpga_f23,fpga.F23
,fpga_f24,fpga.F24
,fpga_f25,fpga.F25
,fpga_f27,fpga.F27
,fpga_f28,fpga.F28
,fpga_f29,fpga.F29
,fpga_f30,fpga.F30
,fpga_f9,fpga.F9
,fpga_g10,fpga.G10
,fpga_g13,fpga.G13
,fpga_g15,fpga.G15
,fpga_g16,fpga.G16
,fpga_g17,fpga.G17
,fpga_g18,fpga.G18
,fpga_g20,fpga.G20
,fpga_g21,fpga.G21
,fpga_g22,fpga.G22
,fpga_g23,fpga.G23
,fpga_g25,fpga.G25
,fpga_g27,fpga.G27
,fpga_g28,fpga.G28
,fpga_g30,fpga.G30
,fpga_h10,fpga.H10
,fpga_h11,fpga.H11
,fpga_h13,fpga.H13
,fpga_h14,fpga.H14
,fpga_h15,fpga.H15
,fpga_h16,fpga.H16
,fpga_h18,fpga.H18
,fpga_h19,fpga.H19
,fpga_h20,fpga.H20
,fpga_h21,fpga.H21
,fpga_h23,fpga.H23
,fpga_h24,fpga.H24
,fpga_h25,fpga.H25
,fpga_h26,fpga.H26
,fpga_h28,fpga.H28
,fpga_h29,fpga.H29
,fpga_h30,fpga.H30
,fpga_h9,fpga.H9
,fpga_j11,fpga.J11
,fpga_j12,fpga.J12
,fpga_j13,fpga.J13
,fpga_j14,fpga.J14
,fpga_j16,fpga.J16
,fpga_j17,fpga.J17
,fpga_j18,fpga.J18
,fpga_j19,fpga.J19
,fpga_j21,fpga.J21
,fpga_j22,fpga.J22
,fpga_j24,fpga.J24
,fpga_j26,fpga.J26
,fpga_j27,fpga.J27
,fpga_j28,fpga.J28
,fpga_j29,fpga.J29
,fpga_k11,fpga.K11
,fpga_k12,fpga.K12
,fpga_k14,fpga.K14
,fpga_k15,fpga.K15
,fpga_k16,fpga.K16
,fpga_k17,fpga.K17
,fpga_k19,fpga.K19
,fpga_k20,fpga.K20
,fpga_k21,fpga.K21
,fpga_k22,fpga.K22
,fpga_k24,fpga.K24
,fpga_k25,fpga.K25
,fpga_k26,fpga.K26
,fpga_k27,fpga.K27
,fpga_k29,fpga.K29
,fpga_l17,fpga.L17
,fpga_l18,fpga.L18
,fpga_l19,fpga.L19
,fpga_l20,fpga.L20
,fpga_l22,fpga.L22
,fpga_l23,fpga.L23
,fpga_l25,fpga.L25
,fpga_l27,fpga.L27
,fpga_l28,fpga.L28
,fpga_l29,fpga.L29
,fpga_m14,fpga.M14
,fpga_m15,fpga.M15
,fpga_m16,fpga.M16
,fpga_m17,fpga.M17
,fpga_m18,fpga.M18
,fpga_m20,fpga.M20
,fpga_m21,fpga.M21
,fpga_m22,fpga.M22
,fpga_m23,fpga.M23
,fpga_m25,fpga.M25
,fpga_m26,fpga.M26
,fpga_m27,fpga.M27
,fpga_m28,fpga.M28
,fpga_n14,fpga.N14
,fpga_n15,fpga.N15
,fpga_n16,fpga.N16
,fpga_n18,fpga.N18
,fpga_n19,fpga.N19
,fpga_n20,fpga.N20
,fpga_n21,fpga.N21
,fpga_n23,fpga.N23
,fpga_n24,fpga.N24
,fpga_n25,fpga.N25
,fpga_n26,fpga.N26
,fpga_p17,fpga.P17
,fpga_p19,fpga.P19
,fpga_p23,fpga.P23
,fpga_p24,fpga.P24
,fpga_p27,fpga.P27
,fpga_r17,fpga.R17
,fpga_r19,fpga.R19
,fpga_r20,fpga.R20
,fpga_r21,fpga.R21
,fpga_r22,fpga.R22
,fpga_r24,fpga.R24
,fpga_r25,fpga.R25
,fpga_r26,fpga.R26
,fpga_r27,fpga.R27
}
);
