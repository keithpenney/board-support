module cmoda7(xc7a35tcpg236pkg fpga,hw.hw hw);
//assign hw.sysclk=fpga.L17;
wire sysclk;
IBUF sysclkibuf(.I(fpga.L17),.O(sysclk));
BUFG bufg(.I(sysclk),.O(hw.sysclk));
assign {fpga.C16,fpga.A17}=hw.led;
assign {fpga.C17,fpga.B16,fpga.B17}=hw.rgb;
assign hw.btn={fpga.B18,fpga.A18};
assign hw.VP_0=fpga.A12;
assign hw.VN_0=fpga.B13;
viapairs #(.WIDTH(4)) pmod4321({hw.pmod4321[0],fpga.G17
,hw.pmod4321[1],fpga.G19
,hw.pmod4321[2],fpga.N18
,hw.pmod4321[3],fpga.L18
});
viapairs #(.WIDTH(4)) pmoda987({hw.pmoda987[0],fpga.H17
,hw.pmoda987[1],fpga.H19
,hw.pmoda987[2],fpga.J19
,hw.pmoda987[3],fpga.K18
});
assign hw.xa_n[0]=fpga.G2;
assign hw.xa_p[0]=fpga.G3;
assign hw.xa_n[1]=fpga.J2;
assign hw.xa_p[1]=fpga.H2;
viapairs #(.WIDTH(48-4)) gpio({hw.pio[0],fpga.M3
,hw.pio[ 1],fpga.L3
,hw.pio[ 2],fpga.A16
,hw.pio[ 3],fpga.K3
,hw.pio[ 4],fpga.C15
,hw.pio[ 5],fpga.H1
,hw.pio[ 6],fpga.A15
,hw.pio[ 7],fpga.B15
,hw.pio[ 8],fpga.A14
,hw.pio[ 9],fpga.J3
,hw.pio[10],fpga.J1
,hw.pio[11],fpga.K2
,hw.pio[12],fpga.L1
,hw.pio[13],fpga.L2
,hw.pio[14],fpga.M1
//,hw.pio[15],fpga.
//,hw.pio[16],fpga.
,hw.pio[17],fpga.N3
,hw.pio[18],fpga.P3
,hw.pio[19],fpga.M2
,hw.pio[20],fpga.N1
,hw.pio[21],fpga.N2
,hw.pio[22],fpga.P1
,hw.pio[23],fpga.R3
//,hw.pio[24],fpga.
//,hw.pio[25],fpga.
,hw.pio[26],fpga.T3
,hw.pio[27],fpga.R2
,hw.pio[28],fpga.T1
,hw.pio[29],fpga.T2
,hw.pio[30],fpga.U1
,hw.pio[31],fpga.W2
,hw.pio[32],fpga.V2
,hw.pio[33],fpga.W3
,hw.pio[34],fpga.V3
,hw.pio[35],fpga.W5
,hw.pio[36],fpga.V4
,hw.pio[37],fpga.U4
,hw.pio[38],fpga.V5
,hw.pio[39],fpga.W4
,hw.pio[40],fpga.U5
,hw.pio[41],fpga.U2
,hw.pio[42],fpga.W6
,hw.pio[43],fpga.U3
,hw.pio[44],fpga.U7
,hw.pio[45],fpga.W7
,hw.pio[46],fpga.U8
,hw.pio[47],fpga.V8
});

assign hw.uart_txd=fpga.J17;
assign fpga.J18=hw.uart_rxd;

assign hw.crypto_sda=fpga.D17;

assign fpga.K17=hw.qspi_cs;
viapairs #(.WIDTH(4)) qspi_d({
hw.qspi_d[0],fpga.D18
,hw.qspi_d[1],fpga.D19
,hw.qspi_d[2],fpga.G18
,hw.qspi_d[3],fpga.F18
});

viapairs #(.WIDTH(19+8+3)) mem({hw.memadr[0],fpga.M18
,hw.memadr[1],fpga.M19
,hw.memadr[2],fpga.K17
,hw.memadr[3],fpga.N17
,hw.memadr[4],fpga.P17
,hw.memadr[5],fpga.P18
,hw.memadr[6],fpga.R18
,hw.memadr[7],fpga.W19
,hw.memadr[8],fpga.U19
,hw.memadr[9],fpga.V19
,hw.memadr[0],fpga.W18
,hw.memadr[1],fpga.T17
,hw.memadr[2],fpga.T18
,hw.memadr[3],fpga.U17
,hw.memadr[4],fpga.U18
,hw.memadr[5],fpga.V16
,hw.memadr[6],fpga.W16
,hw.memadr[7],fpga.W17
,hw.memadr[8],fpga.V15
,hw.memdb[0],fpga.W15
,hw.memdb[1],fpga.W13
,hw.memdb[2],fpga.W14
,hw.memdb[3],fpga.U15
,hw.memdb[4],fpga.U16
,hw.memdb[5],fpga.V13
,hw.memdb[6],fpga.V14
,hw.memdb[7],fpga.U14
,hw.ramoen,fpga.P19
,hw.ramwen,fpga.R19
,hw.ramcen,fpga.N19
});
endmodule
