create_clock -period 5.384 -name sgmiiclk -waveform {0.000 2.692} [get_ports {fpga\.F5}]
create_clock -period 8.000 -name clkk5 -waveform {0.000 4.00} [get_ports {fpga\.K5}]
create_clock -period 8.000 -name clkh5 -waveform {0.000 4.00} [get_ports {fpga\.H5}]
create_clock -period 8.000 -name clkd5 -waveform {0.000 4.00} [get_ports {fpga\.D5}]
