proc marble_io { } {
	dict set pinio modulename "marble"
	set outlist { fpga.A22 fpga.A3 fpga.A4 fpga.AA10 fpga.AA12 fpga.AA13 fpga.AA15 fpga.AA9 fpga.AB10 fpga.AB11 fpga.AB12 fpga.AB7 fpga.AB9 fpga.AC11 fpga.AC12 fpga.AC13 fpga.AC19 fpga.AC21 fpga.AC3 fpga.AC7 fpga.AC8 fpga.AD10 fpga.AD11 fpga.AD13 fpga.AD4 fpga.AD8 fpga.AE10 fpga.AE12 fpga.AE13 fpga.AE8 fpga.AF10 fpga.AF12 fpga.AF13 fpga.AF17 fpga.AF8 fpga.AF9 fpga.B16 fpga.B17 fpga.B19 fpga.B1 fpga.B22 fpga.B24 fpga.B2 fpga.B9 fpga.C23 fpga.C9 fpga.D1 fpga.D2 fpga.D8 fpga.D9 fpga.F10 fpga.F1 fpga.F2 fpga.H11 fpga.H12 fpga.H1 fpga.H2 fpga.K15 fpga.K1 fpga.K2 fpga.M1 fpga.M2 fpga.P1 fpga.P2 fpga.U7 fpga.V12 fpga.V9 fpga.W1 fpga.W15 fpga.Y12 fpga.Y13 }
	set inlist { fpga.A18 fpga.A25 fpga.AB21 fpga.AD21 fpga.AE21 fpga.B5 fpga.B6 fpga.C16 fpga.C3 fpga.C4 fpga.D11 fpga.D5 fpga.D6 fpga.E11 fpga.E3 fpga.E4 fpga.F5 fpga.F6 fpga.G11 fpga.G3 fpga.G4 fpga.H5 fpga.H6 fpga.H8 fpga.H9 fpga.J10 fpga.J11 fpga.J14 fpga.J3 fpga.J4 fpga.J8 fpga.K5 fpga.K6 fpga.L3 fpga.L4 fpga.M16 fpga.N3 fpga.N4 fpga.R3 fpga.R4 }
	dict set pinio inlist $inlist
	dict set pinio outlist $outlist
	return $pinio
}
if { ![info exists pinioproc] } {
	global pinioproc
	set pinioproc {}
}
puts "running pinioproc"
puts $pinioproc
lappend pinioproc marble_io
