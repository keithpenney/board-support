module marble#(parameter MUX1_MMC=0,parameter MUX2_MMC=0,parameter MUX3_MMC=1)(fpga fpga
,ifmarble.hw hw
,iffmc.masterpin fmc1
,iffmc.masterpin fmc2
,ifqsfp qsfp1
,ifqsfp qsfp2
//,ifwr wr
//,ifrgmii rgmii
);
// ddr3
// NC pins, hw.ddr3.cke1, hw.ddr3.clk1_n, hw.ddr3.clk1_p, hw.ddr3.odt1, hw.ddr3.s1_b, hw.ddr3.temp_event
assign {fpga.AD13,fpga.AB12,fpga.AE8,fpga.AA12,fpga.AD11,fpga.AE10,fpga.AE13,fpga.AF13,fpga.AC11,fpga.AB11,fpga.AC12,fpga.AD10,fpga.AA10,fpga.AA9,fpga.AB10,fpga.AC8}=hw.ddr3.a;
assign {fpga.AC13,fpga.AD8,fpga.AF10}=hw.ddr3.ba;
assign {fpga.U7, fpga.W1, fpga.AD4, fpga.AC3, fpga.AA15, fpga.AC19, fpga.W15, fpga.AF17}=hw.ddr3.dm;

assign fpga.AF8=hw.ddr3.cas_b;
assign fpga.AB7=hw.ddr3.ras_b;
assign fpga.AF9=hw.ddr3.we_b;
assign fpga.AA13=hw.ddr3.cke0;
assign fpga.AE12=hw.ddr3.clk0_n;
assign fpga.AF12=hw.ddr3.clk0_p;

assign fpga.AB9=hw.ddr3.odt0;
assign fpga.Y12=hw.ddr3.reset_b;
assign fpga.AC7=hw.ddr3.s0_b;

viapairs #(.WIDTH(80)) ddr3_via({hw.ddr3.d[0], fpga.AF20,hw.ddr3.d[1], fpga.AF19,hw.ddr3.d[10], fpga.V18,hw.ddr3.d[11], fpga.V19,hw.ddr3.d[12], fpga.V16,hw.ddr3.d[13], fpga.W16,hw.ddr3.d[14], fpga.V14,hw.ddr3.d[15], fpga.W14,hw.ddr3.d[16], fpga.AA20,hw.ddr3.d[17], fpga.AD19,hw.ddr3.d[18], fpga.AB17,hw.ddr3.d[19], fpga.AC17,hw.ddr3.d[2], fpga.AE17,hw.ddr3.d[20], fpga.AA19,hw.ddr3.d[21], fpga.AB19,hw.ddr3.d[22], fpga.AD18,hw.ddr3.d[23], fpga.AC18,hw.ddr3.d[24], fpga.AA18,hw.ddr3.d[25], fpga.AB16,hw.ddr3.d[26], fpga.AA14,hw.ddr3.d[27], fpga.AD14,hw.ddr3.d[28], fpga.AB15,hw.ddr3.d[29], fpga.AA17,hw.ddr3.d[3], fpga.AE15,hw.ddr3.d[30], fpga.AC14,hw.ddr3.d[31], fpga.AB14,hw.ddr3.d[32], fpga.AD6,hw.ddr3.d[33], fpga.AB6,hw.ddr3.d[34], fpga.Y6,hw.ddr3.d[35], fpga.AC4,hw.ddr3.d[36], fpga.AC6,hw.ddr3.d[37], fpga.AB4,hw.ddr3.d[38], fpga.AA4,hw.ddr3.d[39], fpga.Y5,hw.ddr3.d[4], fpga.AD16,hw.ddr3.d[40], fpga.AF2,hw.ddr3.d[41], fpga.AE2,hw.ddr3.d[42], fpga.AE1,hw.ddr3.d[43], fpga.AD1,hw.ddr3.d[44], fpga.AE5,hw.ddr3.d[45], fpga.AE6,hw.ddr3.d[46], fpga.AF3,hw.ddr3.d[47], fpga.AE3,hw.ddr3.d[48], fpga.AA3,hw.ddr3.d[49], fpga.AC2,hw.ddr3.d[5], fpga.AD15,hw.ddr3.d[50], fpga.V2,hw.ddr3.d[51], fpga.V1,hw.ddr3.d[52], fpga.AB2,hw.ddr3.d[53], fpga.Y3,hw.ddr3.d[54], fpga.Y2,hw.ddr3.d[55], fpga.Y1,hw.ddr3.d[56], fpga.W3,hw.ddr3.d[57], fpga.V4,hw.ddr3.d[58], fpga.U2,hw.ddr3.d[59], fpga.U1,hw.ddr3.d[6], fpga.AF15,hw.ddr3.d[60], fpga.V6,hw.ddr3.d[61], fpga.V3,hw.ddr3.d[62], fpga.U6,hw.ddr3.d[63], fpga.U5,hw.ddr3.d[7], fpga.AF14,hw.ddr3.d[8], fpga.V17,hw.ddr3.d[9], fpga.Y17,hw.ddr3.dqs_n[0], fpga.AF18,hw.ddr3.dqs_p[0], fpga.AE18,hw.ddr3.dqs_n[1], fpga.W19,hw.ddr3.dqs_p[1], fpga.W18,hw.ddr3.dqs_n[2], fpga.AE20,hw.ddr3.dqs_p[2], fpga.AD20,hw.ddr3.dqs_n[3], fpga.Y16,hw.ddr3.dqs_p[3], fpga.Y15,hw.ddr3.dqs_n[4], fpga.AB5,hw.ddr3.dqs_p[4], fpga.AA5,hw.ddr3.dqs_n[5], fpga.AE5,hw.ddr3.dqs_p[5], fpga.AF5,hw.ddr3.dqs_n[6], fpga.AC1,hw.ddr3.dqs_p[6], fpga.AB1,hw.ddr3.dqs_n[7], fpga.W5,hw.ddr3.dqs_p[7], fpga.W6});

// fmc1 LPC
viapairs #(.WIDTH(72)) fmc1_via({
fpga.E17,fmc1.H5,fpga.F17,fmc1.H4,fpga.D18,fmc1.G3,fpga.E18,fmc1.G2,fpga.H18,fmc1.G7,fpga.H17,fmc1.G6,fpga.F18,fmc1.D9,fpga.G17,fmc1.D8,fpga.J20,fmc1.H8,fpga.K20,fmc1.H7,fpga.L18,fmc1.G10,fpga.M17,fmc1.G9,fpga.G20,fmc1.H11,fpga.H19,fmc1.H10,fpga.E20,fmc1.D12,fpga.F19,fmc1.D11,fpga.L20,fmc1.C11,fpga.L19,fmc1.C10,fpga.D20,fmc1.H14,fpga.D19,fmc1.H13,fpga.F20,fmc1.G13,fpga.G19,fmc1.G12,fpga.J19,fmc1.D15,fpga.J18,fmc1.D14,fpga.G16,fmc1.C15,fpga.H16,fmc1.C14,fpga.K18,fmc1.H17,fpga.L17,fmc1.H16,fpga.F15,fmc1.G16,fpga.G15,fmc1.G15,fpga.D16,fmc1.D18,fpga.D15,fmc1.D17,fpga.E16,fmc1.C19,fpga.E15,fmc1.C18,fpga.J16,fmc1.H20,fpga.J15,fmc1.H19,fpga.K17,fmc1.G19,fpga.K16,fmc1.G18,fpga.D10,fmc1.D21,fpga.E10,fmc1.D20,fpga.C11,fmc1.C23,fpga.C12,fmc1.C22,fpga.G14,fmc1.H23,fpga.H14,fmc1.H22,fpga.A15,fmc1.G22,fpga.B15,fmc1.G21,fpga.D13,fmc1.H26,fpga.D14,fmc1.H25,fpga.A14,fmc1.G25,fpga.B14,fmc1.G24,fpga.F12,fmc1.D24,fpga.G12,fmc1.D23,fpga.A8,fmc1.H29,fpga.A9,fmc1.H28,fpga.G9,fmc1.G28,fpga.G10,fmc1.G27,fpga.E12,fmc1.D27,fpga.E13,fmc1.D26,fpga.F13,fmc1.C27,fpga.F14,fmc1.C26,fpga.H13,fmc1.H32,fpga.J13,fmc1.H31,fpga.F8,fmc1.G31,fpga.F9,fmc1.G30,fpga.B11,fmc1.H35,fpga.B12,fmc1.H34,fpga.A12,fmc1.G34,fpga.A13,fmc1.G33,fpga.C13,fmc1.H38,fpga.C14,fmc1.H37,fpga.A10,fmc1.G37,fpga.B10,fmc1.G36
	});


// fmc2 HPC
viapairs #(.WIDTH(120)) fmc2_via({fpga.AA24,fmc2.H5,fpga.Y23,fmc2.H4,fpga.E23,fmc2.G3,fpga.F22,fmc2.G2,fpga.N22,fmc2.F5,fpga.N21,fmc2.F4,fpga.P21,fmc2.E3,fpga.R21,fmc2.E2,fpga.U20,fmc2.K8,fpga.U19,fmc2.K7,fpga.T19,fmc2.J7,fpga.T18,fmc2.J6,fpga.R17,fmc2.F8,fpga.R16,fmc2.F7,fpga.N17,fmc2.E7,fpga.P16,fmc2.E6,fpga.P18,fmc2.K11,fpga.R18,fmc2.K10,fpga.T25,fmc2.J10,fpga.T24,fmc2.J9,fpga.T23,fmc2.F11,fpga.T22,fmc2.F10,fpga.T17,fmc2.E10,fpga.U17,fmc2.E9,fpga.K26,fmc2.K14,fpga.K25,fmc2.K13,fpga.M19,fmc2.J13,fpga.N18,fmc2.J12,fpga.L24,fmc2.F14,fpga.M24,fmc2.F13,fpga.R20,fmc2.E13,fpga.T20,fmc2.E12,fpga.P20,fmc2.J16,fpga.P19,fmc2.J15,fpga.P25,fmc2.F17,fpga.R25,fmc2.F16,fpga.P26,fmc2.E16,fpga.R26,fmc2.E15,fpga.R23,fmc2.K17,fpga.R22,fmc2.K16,fpga.M22,fmc2.J19,fpga.M21,fmc2.J18,fpga.M20,fmc2.F20,fpga.N19,fmc2.F19,fpga.N23,fmc2.E19,fpga.P23,fmc2.E18,fpga.N24,fmc2.K20,fpga.P24,fmc2.K19,fpga.M26,fmc2.J22,fpga.N26,fmc2.J21,fpga.L25,fmc2.K23,fpga.M25,fmc2.K22,fpga.AA22,fmc2.G7,fpga.Y22,fmc2.G6,fpga.AB24,fmc2.D9,fpga.AA23,fmc2.D8,fpga.AF22,fmc2.H8,fpga.AE22,fmc2.H7,fpga.AE26,fmc2.G10,fpga.AD26,fmc2.G9,fpga.W21,fmc2.H11,fpga.V21,fmc2.H10,fpga.AC26,fmc2.D12,fpga.AB26,fmc2.D11,fpga.AD24,fmc2.C11,fpga.AD23,fmc2.C10,fpga.AC22,fmc2.H14,fpga.AB22,fmc2.H13,fpga.AC24,fmc2.G13,fpga.AC23,fmc2.G12,fpga.V26,fmc2.D15,fpga.U26,fmc2.D14,fpga.AF23,fmc2.C15,fpga.AE23,fmc2.C14,fpga.W24,fmc2.H17,fpga.W23,fmc2.H16,fpga.AB25,fmc2.G16,fpga.AA25,fmc2.G15,fpga.V24,fmc2.D18,fpga.V23,fmc2.D17,fpga.U25,fmc2.C19,fpga.U24,fmc2.C18,fpga.V22,fmc2.H20,fpga.U22,fmc2.H19,fpga.W26,fmc2.G19,fpga.W25,fmc2.G18,fpga.F23,fmc2.D21,fpga.G22,fmc2.D20,fpga.F24,fmc2.C23,fpga.G24,fmc2.C22,fpga.J23,fmc2.H23,fpga.K23,fmc2.H22,fpga.K22,fmc2.G22,fpga.L22,fmc2.G21,fpga.H22,fmc2.H26,fpga.J21,fmc2.H25,fpga.D25,fmc2.G25,fpga.E25,fmc2.G24,fpga.H24,fmc2.D24,fpga.H23,fmc2.D23,fpga.J25,fmc2.H29,fpga.J24,fmc2.H28,fpga.D24,fmc2.G28,fpga.D23,fmc2.G27,fpga.E26,fmc2.D27,fpga.F25,fmc2.D26,fpga.G21,fmc2.C27,fpga.H21,fmc2.C26,fpga.G26,fmc2.H32,fpga.G25,fmc2.H31,fpga.H26,fmc2.G31,fpga.J26,fmc2.G30,fpga.C26,fmc2.H35,fpga.D26,fmc2.H34,fpga.E22,fmc2.G34,fpga.E21,fmc2.G33,fpga.A20,fmc2.H38,fpga.B20,fmc2.H37,fpga.B21,fmc2.G37,fpga.C21,fmc2.G36});


viapairs #(.WIDTH(8)) pmod1_via({fpga.C17,hw.pmod1.p[7],fpga.C19,hw.pmod1.p[6],fpga.C18,hw.pmod1.p[5],fpga.K21,hw.pmod1.p[4],fpga.D21,hw.pmod1.p[3],fpga.L23,hw.pmod1.p[2],fpga.C22,hw.pmod1.p[1],fpga.C24,hw.pmod1.p[0]});

viapairs #(.WIDTH(8)) pmod2_via({fpga.W9,hw.pmod2.p[7],fpga.Y8,hw.pmod2.p[6],fpga.AA8,hw.pmod2.p[5],fpga.V8,hw.pmod2.p[4],fpga.AF7,hw.pmod2.p[3],fpga.Y7,hw.pmod2.p[2],fpga.V7,hw.pmod2.p[1],fpga.AE7,hw.pmod2.p[0]});

assign fpga.V9=hw.wr.vcxo_en;
viapairs #(.WIDTH(5)) wr_via({hw.wr.dac2_sync,fpga.Y11,hw.wr.dac1_sync,fpga.W10,hw.wr.dac_sclk,fpga.V11,hw.wr.dac_din,fpga.Y10,hw.wr.clk20_vcxo,fpga.W11});

BUFG clkin0_bufg(.I(fpga.G11),.O(hw.clkin0));
BUFG clkin12_bufg(.I(fpga.D11),.O(hw.clkin12));
BUFG clkin3_bufg(.I(fpga.J14),.O(hw.clkin3));


assign {fpga.D9,fpga.D8,fpga.H12,fpga.H11}=hw.rgmii.txd;
assign hw.rgmii.rxd={fpga.H9,fpga.H8,fpga.J8,fpga.J10};
assign fpga.F10=hw.rgmii.tx_clk;
assign hw.rgmii.rx_clk=fpga.E11;
assign fpga.C9=hw.rgmii.tx_en;
assign hw.rgmii.rx_dv=fpga.J11;
assign fpga.B9=hw.rgmii.phy_rst_n;

assign fpga.B16=hw.iic.scl;
viapairs #(.WIDTH(1)) iicsda_via({fpga.A17,hw.iic.sda});

assign fpga.K15=hw.FPGA_RxD;
assign hw.FPGA_TxD=fpga.C16;
assign hw.FPGA_RTS=fpga.M16; // ?? direction??
assign hw.FPGA_INT=fpga.A18;
assign fpga.B19=hw.i2c_fpga_sw_rst;

assign fpga.B17=hw.EXP_INT;
assign fpga.AC21=hw.FPGA_MISO;
assign hw.FPGA_MOSI=fpga.AB21;
assign hw.FPGA_SSEL=fpga.AD21;
assign hw.FPGA_SCK=fpga.AE21;


assign fpga.Y13=hw.LD16;
assign fpga.V12=hw.LD17;

assign fpga.C23=hw.cfg_fcs;
assign fpga.B24=hw.cfg_mosi;
assign fpga.B22=hw.cfg_d02;
assign fpga.A22=hw.cfg_d03;
assign hw.cfg_din=fpga.A25;

IBUFDS_GTE2 ibufgds_mgt115refclk0(.I(fpga.H6),.IB(fpga.H5),.O(hw.mgt115refclk0),.CEB(1'b0));
IBUFDS_GTE2 ibufgds_mgt115refclk1(.I(fpga.K6),.IB(fpga.K5),.O(hw.mgt115refclk1),.CEB(1'b0));
IBUFDS_GTE2 ibufgds_mgt116refclk0(.I(fpga.D6),.IB(fpga.D5),.O(hw.mgt116refclk0),.CEB(1'b0));
IBUFDS_GTE2 ibufgds_mgt116refclk1(.I(fpga.F6),.IB(fpga.F5),.O(hw.mgt116refclk1),.CEB(1'b0));
wire [7:0] mgttx_p;
wire [7:0] mgtrx_p;
wire [7:0] mgttx_n;
wire [7:0] mgtrx_n;
assign fpga.F2=mgttx_p[0];
assign fpga.F1=mgttx_n[0];
assign fpga.D2=mgttx_p[1];
assign fpga.D1=mgttx_n[1];
assign fpga.B2=mgttx_p[2];
assign fpga.B1=mgttx_n[2];
assign fpga.A4=mgttx_p[3];
assign fpga.A3=mgttx_n[3];
assign mgtrx_p[0]=fpga.G4;
assign mgtrx_n[0]=fpga.G3;
assign mgtrx_p[1]=fpga.E4;
assign mgtrx_n[1]=fpga.E3;
assign mgtrx_p[2]=fpga.C4;
assign mgtrx_n[2]=fpga.C3;
assign mgtrx_p[3]=fpga.B6;
assign mgtrx_n[3]=fpga.B5;
assign fpga.P2=mgttx_p[4];
assign fpga.P1=mgttx_n[4];
assign fpga.M2=mgttx_p[5];
assign fpga.M1=mgttx_n[5];
assign fpga.K2=mgttx_p[6];
assign fpga.K1=mgttx_n[6];
assign fpga.H2=mgttx_p[7];
assign fpga.H1=mgttx_n[7];
assign mgtrx_p[4]=fpga.R4;
assign mgtrx_n[4]=fpga.R3;
assign mgtrx_p[5]=fpga.N4;
assign mgtrx_n[5]=fpga.N3;
assign mgtrx_p[6]=fpga.L4;
assign mgtrx_n[6]=fpga.L3;
assign mgtrx_p[7]=fpga.J4;
assign mgtrx_n[7]=fpga.J3;

assign mgttx_p[0]=qsfp1.tx_p[2];
assign mgttx_n[0]=qsfp1.tx_n[2];
assign mgttx_p[1]=qsfp1.tx_p[0];
assign mgttx_n[1]=qsfp1.tx_n[0];
assign mgttx_p[2]=qsfp1.tx_p[1];
assign mgttx_n[2]=qsfp1.tx_n[1];
assign mgttx_p[3]=qsfp1.tx_p[3];
assign mgttx_n[3]=qsfp1.tx_n[3];

assign qsfp1.rx_p[2]=mgtrx_p[0];
assign qsfp1.rx_n[2]=mgtrx_n[0];
assign qsfp1.rx_p[0]=mgtrx_p[1];
assign qsfp1.rx_n[0]=mgtrx_n[1];
assign qsfp1.rx_p[1]=mgtrx_p[2];
assign qsfp1.rx_n[1]=mgtrx_n[2];
assign qsfp1.rx_p[3]=mgtrx_p[3];
assign qsfp1.rx_n[3]=mgtrx_n[3];

assign mgttx_p[4]=MUX3_MMC ? qsfp2.tx_p[2]: fmc2.C2;
assign mgttx_n[4]=MUX3_MMC ? qsfp2.tx_n[2]: fmc2.C3;
assign mgttx_p[5]=MUX3_MMC ? qsfp2.tx_p[0]: fmc2.A22;
assign mgttx_n[5]=MUX3_MMC ? qsfp2.tx_n[0]: fmc2.A23;
assign mgttx_p[6]=MUX3_MMC ? qsfp2.tx_p[1]: MUX1_MMC ? fmc1.C2:fmc2.A26;
assign mgttx_n[6]=MUX3_MMC ? qsfp2.tx_n[1]: MUX1_MMC ? fmc1.C3:fmc2.A27;
assign mgttx_p[7]=MUX3_MMC ? qsfp2.tx_p[3]: MUX2_MMC ? fmc2.A30:fmc1.A22;
assign mgttx_n[7]=MUX3_MMC ? qsfp2.tx_n[3]: MUX2_MMC ? fmc2.A31:fmc1.A23;
assign qsfp2.rx_p[2]=MUX3_MMC ? mgtrx_p[4] : 1'bz;
assign qsfp2.rx_n[2]=MUX3_MMC ? mgtrx_n[4] : 1'bz;
assign qsfp2.rx_p[0]=MUX3_MMC ? mgtrx_p[5] : 1'bz;
assign qsfp2.rx_n[0]=MUX3_MMC ? mgtrx_n[5] : 1'bz;
assign qsfp2.rx_p[1]=MUX3_MMC ? mgtrx_p[6] : 1'bz;
assign qsfp2.rx_n[1]=MUX3_MMC ? mgtrx_n[6] : 1'bz;
assign qsfp2.rx_p[4]=MUX3_MMC ? mgtrx_p[7] : 1'bz;
assign qsfp2.rx_n[4]=MUX3_MMC ? mgtrx_n[7] : 1'bz;
assign fmc2.C6=MUX3_MMC ? 1'bz :mgtrx_p[4];
assign fmc2.C7=MUX3_MMC ? 1'bz :mgtrx_n[4];
assign fmc2.A2=MUX3_MMC ? 1'bz :mgtrx_p[5];
assign fmc2.A3=MUX3_MMC ? 1'bz :mgtrx_n[5];
assign fmc2.A6=MUX3_MMC ? 1'bz :MUX1_MMC ? 1'bz : mgtrx_p[6];
assign fmc2.A7=MUX3_MMC ? 1'bz :MUX1_MMC ? 1'bz : mgtrx_n[6];
assign fmc2.A10=MUX3_MMC ? 1'bz :MUX2_MMC ? mgtrx_p[7] :1'bz;
assign fmc2.A11=MUX3_MMC ? 1'bz :MUX2_MMC ? mgtrx_n[7] :1'bz;

assign fmc1.C6=MUX3_MMC ? 1'bz :MUX1_MMC ? mgtrx_p[6] :1'bz;
assign fmc1.C7=MUX3_MMC ? 1'bz :MUX1_MMC ? mgtrx_n[6] :1'bz;
assign fmc1.A2=MUX3_MMC ? 1'bz :MUX2_MMC ? 1'bz :mgtrx_p[7];
assign fmc1.A3=MUX3_MMC ? 1'bz :MUX2_MMC ? 1'bz :mgtrx_n[7];

endmodule


/*module hwifsim(hwif.sim hw
,input clk100
,input clk125
,input usersi570c0
,input usersi570c1
,input clk104_pl_sysref
,input clk104_pl_clk
);
assign hw.clk100=clk100;
assign hw.clk125=clk125;
assign hw.usersi570c0=usersi570c0;
assign hw.usersi570c1=usersi570c1;
assign hw.clk104_pl_sysref=clk104_pl_sysref;
assign hw.clk104_pl_clk=clk104_pl_clk;

endmodule
*/
module marble_sim#(parameter MUX1_MMC=0,parameter MUX2_MMC=0,parameter MUX3_MMC=1)(fpga fpga
,ifmarble.sim hw
,iffmc.masterpin fmc1
,iffmc.masterpin fmc2
,ifqsfp qsfp1
,ifqsfp qsfp2
);
assign fpga.H6= hw.mgt115refclk0;
assign fpga.H5=~hw.mgt115refclk0;
assign fpga.K6= hw.mgt115refclk1;
assign fpga.K5=~hw.mgt115refclk1;
assign fpga.D6= hw.mgt116refclk0;
assign fpga.D5=~hw.mgt116refclk0;
assign fpga.F6= hw.mgt116refclk1;
assign fpga.F5=~hw.mgt116refclk1;
assign fpga.G11=hw.clkin0;
assign fpga.D11=hw.clkin12;
assign fpga.J14=hw.clkin3;
viapairs #(.WIDTH(80)) ddr3_via({hw.ddr3.d[0], fpga.AF20,hw.ddr3.d[1], fpga.AF19,hw.ddr3.d[10], fpga.V18,hw.ddr3.d[11], fpga.V19,hw.ddr3.d[12], fpga.V16,hw.ddr3.d[13], fpga.W16,hw.ddr3.d[14], fpga.V14,hw.ddr3.d[15], fpga.W14,hw.ddr3.d[16], fpga.AA20,hw.ddr3.d[17], fpga.AD19,hw.ddr3.d[18], fpga.AB17,hw.ddr3.d[19], fpga.AC17,hw.ddr3.d[2], fpga.AE17,hw.ddr3.d[20], fpga.AA19,hw.ddr3.d[21], fpga.AB19,hw.ddr3.d[22], fpga.AD18,hw.ddr3.d[23], fpga.AC18,hw.ddr3.d[24], fpga.AA18,hw.ddr3.d[25], fpga.AB16,hw.ddr3.d[26], fpga.AA14,hw.ddr3.d[27], fpga.AD14,hw.ddr3.d[28], fpga.AB15,hw.ddr3.d[29], fpga.AA17,hw.ddr3.d[3], fpga.AE15,hw.ddr3.d[30], fpga.AC14,hw.ddr3.d[31], fpga.AB14,hw.ddr3.d[32], fpga.AD6,hw.ddr3.d[33], fpga.AB6,hw.ddr3.d[34], fpga.Y6,hw.ddr3.d[35], fpga.AC4,hw.ddr3.d[36], fpga.AC6,hw.ddr3.d[37], fpga.AB4,hw.ddr3.d[38], fpga.AA4,hw.ddr3.d[39], fpga.Y5,hw.ddr3.d[4], fpga.AD16,hw.ddr3.d[40], fpga.AF2,hw.ddr3.d[41], fpga.AE2,hw.ddr3.d[42], fpga.AE1,hw.ddr3.d[43], fpga.AD1,hw.ddr3.d[44], fpga.AE5,hw.ddr3.d[45], fpga.AE6,hw.ddr3.d[46], fpga.AF3,hw.ddr3.d[47], fpga.AE3,hw.ddr3.d[48], fpga.AA3,hw.ddr3.d[49], fpga.AC2,hw.ddr3.d[5], fpga.AD15,hw.ddr3.d[50], fpga.V2,hw.ddr3.d[51], fpga.V1,hw.ddr3.d[52], fpga.AB2,hw.ddr3.d[53], fpga.Y3,hw.ddr3.d[54], fpga.Y2,hw.ddr3.d[55], fpga.Y1,hw.ddr3.d[56], fpga.W3,hw.ddr3.d[57], fpga.V4,hw.ddr3.d[58], fpga.U2,hw.ddr3.d[59], fpga.U1,hw.ddr3.d[6], fpga.AF15,hw.ddr3.d[60], fpga.V6,hw.ddr3.d[61], fpga.V3,hw.ddr3.d[62], fpga.U6,hw.ddr3.d[63], fpga.U5,hw.ddr3.d[7], fpga.AF14,hw.ddr3.d[8], fpga.V17,hw.ddr3.d[9], fpga.Y17,hw.ddr3.dqs_n[0], fpga.AF18,hw.ddr3.dqs_p[0], fpga.AE18,hw.ddr3.dqs_n[1], fpga.W19,hw.ddr3.dqs_p[1], fpga.W18,hw.ddr3.dqs_n[2], fpga.AE20,hw.ddr3.dqs_p[2], fpga.AD20,hw.ddr3.dqs_n[3], fpga.Y16,hw.ddr3.dqs_p[3], fpga.Y15,hw.ddr3.dqs_n[4], fpga.AB5,hw.ddr3.dqs_p[4], fpga.AA5,hw.ddr3.dqs_n[5], fpga.AE5,hw.ddr3.dqs_p[5], fpga.AF5,hw.ddr3.dqs_n[6], fpga.AC1,hw.ddr3.dqs_p[6], fpga.AB1,hw.ddr3.dqs_n[7], fpga.W5,hw.ddr3.dqs_p[7], fpga.W6});

// fmc1 LPC
viapairs #(.WIDTH(72)) fmc1_via({
fpga.E17,fmc1.H5,fpga.F17,fmc1.H4,fpga.D18,fmc1.G3,fpga.E18,fmc1.G2,fpga.H18,fmc1.G7,fpga.H17,fmc1.G6,fpga.F18,fmc1.D9,fpga.G17,fmc1.D8,fpga.J20,fmc1.H8,fpga.K20,fmc1.H7,fpga.L18,fmc1.G10,fpga.M17,fmc1.G9,fpga.G20,fmc1.H11,fpga.H19,fmc1.H10,fpga.E20,fmc1.D12,fpga.F19,fmc1.D11,fpga.L20,fmc1.C11,fpga.L19,fmc1.C10,fpga.D20,fmc1.H14,fpga.D19,fmc1.H13,fpga.F20,fmc1.G13,fpga.G19,fmc1.G12,fpga.J19,fmc1.D15,fpga.J18,fmc1.D14,fpga.G16,fmc1.C15,fpga.H16,fmc1.C14,fpga.K18,fmc1.H17,fpga.L17,fmc1.H16,fpga.F15,fmc1.G16,fpga.G15,fmc1.G15,fpga.D16,fmc1.D18,fpga.D15,fmc1.D17,fpga.E16,fmc1.C19,fpga.E15,fmc1.C18,fpga.J16,fmc1.H20,fpga.J15,fmc1.H19,fpga.K17,fmc1.G19,fpga.K16,fmc1.G18,fpga.D10,fmc1.D21,fpga.E10,fmc1.D20,fpga.C11,fmc1.C23,fpga.C12,fmc1.C22,fpga.G14,fmc1.H23,fpga.H14,fmc1.H22,fpga.A15,fmc1.G22,fpga.B15,fmc1.G21,fpga.D13,fmc1.H26,fpga.D14,fmc1.H25,fpga.A14,fmc1.G25,fpga.B14,fmc1.G24,fpga.F12,fmc1.D24,fpga.G12,fmc1.D23,fpga.A8,fmc1.H29,fpga.A9,fmc1.H28,fpga.G9,fmc1.G28,fpga.G10,fmc1.G27,fpga.E12,fmc1.D27,fpga.E13,fmc1.D26,fpga.F13,fmc1.C27,fpga.F14,fmc1.C26,fpga.H13,fmc1.H32,fpga.J13,fmc1.H31,fpga.F8,fmc1.G31,fpga.F9,fmc1.G30,fpga.B11,fmc1.H35,fpga.B12,fmc1.H34,fpga.A12,fmc1.G34,fpga.A13,fmc1.G33,fpga.C13,fmc1.H38,fpga.C14,fmc1.H37,fpga.A10,fmc1.G37,fpga.B10,fmc1.G36
	});


// fmc2 HPC
viapairs #(.WIDTH(120)) fmc2_via({fpga.AA24,fmc2.H5,fpga.Y23,fmc2.H4,fpga.E23,fmc2.G3,fpga.F22,fmc2.G2,fpga.N22,fmc2.F5,fpga.N21,fmc2.F4,fpga.P21,fmc2.E3,fpga.R21,fmc2.E2,fpga.U20,fmc2.K8,fpga.U19,fmc2.K7,fpga.T19,fmc2.J7,fpga.T18,fmc2.J6,fpga.R17,fmc2.F8,fpga.R16,fmc2.F7,fpga.N17,fmc2.E7,fpga.P16,fmc2.E6,fpga.P18,fmc2.K11,fpga.R18,fmc2.K10,fpga.T25,fmc2.J10,fpga.T24,fmc2.J9,fpga.T23,fmc2.F11,fpga.T22,fmc2.F10,fpga.T17,fmc2.E10,fpga.U17,fmc2.E9,fpga.K26,fmc2.K14,fpga.K25,fmc2.K13,fpga.M19,fmc2.J13,fpga.N18,fmc2.J12,fpga.L24,fmc2.F14,fpga.M24,fmc2.F13,fpga.R20,fmc2.E13,fpga.T20,fmc2.E12,fpga.P20,fmc2.J16,fpga.P19,fmc2.J15,fpga.P25,fmc2.F17,fpga.R25,fmc2.F16,fpga.P26,fmc2.E16,fpga.R26,fmc2.E15,fpga.R23,fmc2.K17,fpga.R22,fmc2.K16,fpga.M22,fmc2.J19,fpga.M21,fmc2.J18,fpga.M20,fmc2.F20,fpga.N19,fmc2.F19,fpga.N23,fmc2.E19,fpga.P23,fmc2.E18,fpga.N24,fmc2.K20,fpga.P24,fmc2.K19,fpga.M26,fmc2.J22,fpga.N26,fmc2.J21,fpga.L25,fmc2.K23,fpga.M25,fmc2.K22,fpga.AA22,fmc2.G7,fpga.Y22,fmc2.G6,fpga.AB24,fmc2.D9,fpga.AA23,fmc2.D8,fpga.AF22,fmc2.H8,fpga.AE22,fmc2.H7,fpga.AE26,fmc2.G10,fpga.AD26,fmc2.G9,fpga.W21,fmc2.H11,fpga.V21,fmc2.H10,fpga.AC26,fmc2.D12,fpga.AB26,fmc2.D11,fpga.AD24,fmc2.C11,fpga.AD23,fmc2.C10,fpga.AC22,fmc2.H14,fpga.AB22,fmc2.H13,fpga.AC24,fmc2.G13,fpga.AC23,fmc2.G12,fpga.V26,fmc2.D15,fpga.U26,fmc2.D14,fpga.AF23,fmc2.C15,fpga.AE23,fmc2.C14,fpga.W24,fmc2.H17,fpga.W23,fmc2.H16,fpga.AB25,fmc2.G16,fpga.AA25,fmc2.G15,fpga.V24,fmc2.D18,fpga.V23,fmc2.D17,fpga.U25,fmc2.C19,fpga.U24,fmc2.C18,fpga.V22,fmc2.H20,fpga.U22,fmc2.H19,fpga.W26,fmc2.G19,fpga.W25,fmc2.G18,fpga.F23,fmc2.D21,fpga.G22,fmc2.D20,fpga.F24,fmc2.C23,fpga.G24,fmc2.C22,fpga.J23,fmc2.H23,fpga.K23,fmc2.H22,fpga.K22,fmc2.G22,fpga.L22,fmc2.G21,fpga.H22,fmc2.H26,fpga.J21,fmc2.H25,fpga.D25,fmc2.G25,fpga.E25,fmc2.G24,fpga.H24,fmc2.D24,fpga.H23,fmc2.D23,fpga.J25,fmc2.H29,fpga.J24,fmc2.H28,fpga.D24,fmc2.G28,fpga.D23,fmc2.G27,fpga.E26,fmc2.D27,fpga.F25,fmc2.D26,fpga.G21,fmc2.C27,fpga.H21,fmc2.C26,fpga.G26,fmc2.H32,fpga.G25,fmc2.H31,fpga.H26,fmc2.G31,fpga.J26,fmc2.G30,fpga.C26,fmc2.H35,fpga.D26,fmc2.H34,fpga.E22,fmc2.G34,fpga.E21,fmc2.G33,fpga.A20,fmc2.H38,fpga.B20,fmc2.H37,fpga.B21,fmc2.G37,fpga.C21,fmc2.G36});

assign hw.rgmii.txd={fpga.D9,fpga.D8,fpga.H12,fpga.H11};
assign {fpga.H9,fpga.H8,fpga.J8,fpga.J10}=hw.rgmii.rxd;
assign hw.rgmii.tx_clk=fpga.F10;
assign fpga.E11=hw.rgmii.rx_clk;
assign hw.rgmii.tx_en=fpga.C9;
assign fpga.J11=hw.rgmii.rx_dv;
assign hw.rgmii.phy_rst_n=fpga.B9;

endmodule


