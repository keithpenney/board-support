set_property IOSTANDARD LVDS [get_ports {fpga_A14}]
set_property IOSTANDARD LVDS [get_ports {fpga_A15}]
set_property IOSTANDARD LVDS [get_ports {fpga_A16}]
set_property IOSTANDARD LVDS [get_ports {fpga_A17}]
set_property IOSTANDARD LVDS [get_ports {fpga_A19}]
set_property IOSTANDARD LVDS [get_ports {fpga_A20}]
set_property IOSTANDARD LVDS [get_ports {fpga_A21}]
set_property IOSTANDARD LVDS [get_ports {fpga_A22}]
set_property IOSTANDARD LVDS [get_ports {fpga_A24}]
set_property IOSTANDARD LVDS [get_ports {fpga_A25}]
set_property IOSTANDARD LVDS [get_ports {fpga_A26}]
set_property IOSTANDARD LVDS [get_ports {fpga_A27}]
set_property IOSTANDARD LVDS [get_ports {fpga_A29}]
set_property IOSTANDARD LVDS [get_ports {fpga_A30}]
set_property IOSTANDARD LVDS [get_ports {fpga_A31}]
set_property IOSTANDARD LVDS [get_ports {fpga_A32}]
set_property IOSTANDARD LVDS [get_ports {fpga_B14}]
set_property IOSTANDARD LVDS [get_ports {fpga_B16}]
set_property IOSTANDARD LVDS [get_ports {fpga_B17}]
set_property IOSTANDARD LVDS [get_ports {fpga_B19}]
set_property IOSTANDARD LVDS [get_ports {fpga_B21}]
set_property IOSTANDARD LVDS [get_ports {fpga_B22}]
set_property IOSTANDARD LVDS [get_ports {fpga_B23}]
set_property IOSTANDARD LVDS [get_ports {fpga_B26}]
set_property IOSTANDARD LVDS [get_ports {fpga_B27}]
set_property IOSTANDARD LVDS [get_ports {fpga_B28}]
set_property IOSTANDARD LVDS [get_ports {fpga_B29}]
set_property IOSTANDARD LVDS [get_ports {fpga_B31}]
set_property IOSTANDARD LVDS [get_ports {fpga_C13}]
set_property IOSTANDARD LVDS [get_ports {fpga_C14}]
set_property IOSTANDARD LVDS [get_ports {fpga_C15}]
set_property IOSTANDARD LVDS [get_ports {fpga_C16}]
set_property IOSTANDARD LVDS [get_ports {fpga_C18}]
set_property IOSTANDARD LVDS [get_ports {fpga_C19}]
set_property IOSTANDARD LVDS [get_ports {fpga_C20}]
set_property IOSTANDARD LVDS [get_ports {fpga_C21}]
set_property IOSTANDARD LVDS [get_ports {fpga_C23}]
set_property IOSTANDARD LVDS [get_ports {fpga_C24}]
set_property IOSTANDARD LVDS [get_ports {fpga_C25}]
#set_property IOSTANDARD LVCMOS18 [get_ports {fpga_C26}]
set_property IOSTANDARD LVDS [get_ports {fpga_C28}]
set_property IOSTANDARD LVDS [get_ports {fpga_C29}]
set_property IOSTANDARD LVDS [get_ports {fpga_C30}]
set_property IOSTANDARD LVDS [get_ports {fpga_C31}]
set_property IOSTANDARD LVDS [get_ports {fpga_D13}]
set_property IOSTANDARD LVDS [get_ports {fpga_D15}]
set_property IOSTANDARD LVDS [get_ports {fpga_D16}]
set_property IOSTANDARD LVDS [get_ports {fpga_D17}]
set_property IOSTANDARD LVDS [get_ports {fpga_D18}]
set_property IOSTANDARD LVDS [get_ports {fpga_D20}]
set_property IOSTANDARD LVDS [get_ports {fpga_D21}]
set_property IOSTANDARD LVDS [get_ports {fpga_D22}]
set_property IOSTANDARD LVDS [get_ports {fpga_D23}]
set_property IOSTANDARD LVDS [get_ports {fpga_D25}]
set_property IOSTANDARD LVDS [get_ports {fpga_D26}]
set_property IOSTANDARD LVDS [get_ports {fpga_D27}]
set_property IOSTANDARD LVDS [get_ports {fpga_D28}]
set_property IOSTANDARD LVDS [get_ports {fpga_D30}]
set_property IOSTANDARD LVDS [get_ports {fpga_E12}]
set_property IOSTANDARD LVDS [get_ports {fpga_E13}]
set_property IOSTANDARD LVDS [get_ports {fpga_E14}]
set_property IOSTANDARD LVDS [get_ports {fpga_E15}]
set_property IOSTANDARD LVDS [get_ports {fpga_E17}]
set_property IOSTANDARD LVDS [get_ports {fpga_E18}]
set_property IOSTANDARD LVDS [get_ports {fpga_E19}]
set_property IOSTANDARD LVDS [get_ports {fpga_E20}]
set_property IOSTANDARD LVDS [get_ports {fpga_E22}]
set_property IOSTANDARD LVDS [get_ports {fpga_E23}]
set_property IOSTANDARD LVDS [get_ports {fpga_E24}]
set_property IOSTANDARD LVDS [get_ports {fpga_E25}]
set_property IOSTANDARD LVDS [get_ports {fpga_E27}]
set_property IOSTANDARD LVDS [get_ports {fpga_E28}]
set_property IOSTANDARD LVDS [get_ports {fpga_E29}]
set_property IOSTANDARD LVDS [get_ports {fpga_E30}]
set_property IOSTANDARD LVDS [get_ports {fpga_F12}]
set_property IOSTANDARD LVDS [get_ports {fpga_F14}]
set_property IOSTANDARD LVDS [get_ports {fpga_F15}]
#set_property IOSTANDARD LVCMOS18 [get_ports {fpga_F16}]
set_property IOSTANDARD LVDS [get_ports {fpga_F17}]
set_property IOSTANDARD LVDS [get_ports {fpga_F19}]
set_property IOSTANDARD LVDS [get_ports {fpga_F20}]
set_property IOSTANDARD LVDS [get_ports {fpga_F22}]
set_property IOSTANDARD LVDS [get_ports {fpga_F25}]
set_property IOSTANDARD LVDS [get_ports {fpga_F26}]
set_property IOSTANDARD LVDS [get_ports {fpga_F27}]
set_property IOSTANDARD LVDS [get_ports {fpga_F29}]
set_property IOSTANDARD LVDS [get_ports {fpga_F30}]
set_property IOSTANDARD LVDS [get_ports {fpga_F31}]
set_property IOSTANDARD LVDS [get_ports {fpga_G12}]
set_property IOSTANDARD LVDS [get_ports {fpga_G13}]
set_property IOSTANDARD LVDS [get_ports {fpga_G14}]
set_property IOSTANDARD LVDS [get_ports {fpga_G16}]
set_property IOSTANDARD LVDS [get_ports {fpga_G17}]
set_property IOSTANDARD LVDS [get_ports {fpga_G18}]
set_property IOSTANDARD LVDS [get_ports {fpga_G19}]
set_property IOSTANDARD LVDS [get_ports {fpga_H13}]
set_property IOSTANDARD LVDS [get_ports {fpga_H14}]
#set_property IOSTANDARD LVDS [get_ports {fpga_H15}]
set_property IOSTANDARD LVDS [get_ports {fpga_H16}]
set_property IOSTANDARD LVDS [get_ports {fpga_H18}]
set_property IOSTANDARD LVDS [get_ports {fpga_H19}]
set_property IOSTANDARD LVDS [get_ports {fpga_H20}]
set_property IOSTANDARD LVDS [get_ports {fpga_J12}]
set_property IOSTANDARD LVDS [get_ports {fpga_J13}]
set_property IOSTANDARD LVDS [get_ports {fpga_J15}]
set_property IOSTANDARD LVDS [get_ports {fpga_J17}]
set_property IOSTANDARD LVDS [get_ports {fpga_J18}]
set_property IOSTANDARD LVDS [get_ports {fpga_J20}]
set_property IOSTANDARD LVDS [get_ports {fpga_K12}]
set_property IOSTANDARD LVDS [get_ports {fpga_K13}]
set_property IOSTANDARD LVDS [get_ports {fpga_K14}]
set_property IOSTANDARD LVDS [get_ports {fpga_K15}]
set_property IOSTANDARD LVDS [get_ports {fpga_K17}]
set_property IOSTANDARD LVDS [get_ports {fpga_K19}]
set_property IOSTANDARD LVDS [get_ports {fpga_L12}]
set_property IOSTANDARD LVDS [get_ports {fpga_L14}]
set_property IOSTANDARD LVDS [get_ports {fpga_L15}]
set_property IOSTANDARD LVDS [get_ports {fpga_L16}]
#set_property IOSTANDARD LVDS [get_ports {fpga_L17}]
#set_property IOSTANDARD LVDS [get_ports {fpga_L19}]
#set_property IOSTANDARD LVDS [get_ports {fpga_L20}]
set_property IOSTANDARD LVDS [get_ports {fpga_M11}]
set_property IOSTANDARD LVDS [get_ports {fpga_M12}]
set_property IOSTANDARD LVDS [get_ports {fpga_M13}]
set_property IOSTANDARD LVDS [get_ports {fpga_M14}]
set_property IOSTANDARD LVDS [get_ports {fpga_M16}]
#set_property IOSTANDARD LVDS [get_ports {fpga_M17}]
#set_property IOSTANDARD LVDS [get_ports {fpga_M18}]
#set_property IOSTANDARD LVDS [get_ports {fpga_M19}]
set_property IOSTANDARD LVDS [get_ports {fpga_N13}]
set_property IOSTANDARD LVDS [get_ports {fpga_N14}]
set_property IOSTANDARD LVDS [get_ports {fpga_N15}]
set_property IOSTANDARD LVDS [get_ports {fpga_N16}]
#set_property IOSTANDARD LVDS [get_ports {fpga_N18}]
#set_property IOSTANDARD LVDS [get_ports {fpga_N19}]
#set_property IOSTANDARD LVDS [get_ports {fpga_N20}]
#set_property IOSTANDARD LVDS [get_ports {fpga_P18}]
#set_property IOSTANDARD LVDS [get_ports {fpga_P20}]

set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AH31}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AH35}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AJ33}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AK33}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AL31}]
#set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AM39}]
#set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AN39}]
#set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AP33}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AP37}]
#set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AP41}]
#set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AP42}]
#set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AR35}]
#set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AR37}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AT31}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AT35}]
#set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AT36}]
#set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AT37}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AU32}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AU33}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AU34}]
#set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AU36}]
#set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AU39}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AV30}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AV39}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AW30}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AY30}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AY33}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AY42}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_BA30}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_BA31}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_BA32}]
#set_property IOSTANDARD LVCMOS18 [get_ports {fpga_BA37}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_BB31}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_BB38}]
set_property IOSTANDARD LVDS [get_ports {fpga_AF30}]
set_property IOSTANDARD LVDS [get_ports {fpga_AG29}]
set_property IOSTANDARD LVDS [get_ports {fpga_AG31}]
set_property IOSTANDARD LVDS [get_ports {fpga_AG33}]
set_property IOSTANDARD LVDS [get_ports {fpga_AH28}]
set_property IOSTANDARD LVDS [get_ports {fpga_AH29}]
set_property IOSTANDARD LVDS [get_ports {fpga_AH30}]
set_property IOSTANDARD LVDS [get_ports {fpga_AH33}]
set_property IOSTANDARD LVDS [get_ports {fpga_AH34}]
set_property IOSTANDARD LVDS [get_ports {fpga_AJ20}]
set_property IOSTANDARD LVDS [get_ports {fpga_AJ21}]
set_property IOSTANDARD LVDS [get_ports {fpga_AJ22}]
set_property IOSTANDARD LVDS [get_ports {fpga_AJ23}]
set_property IOSTANDARD LVDS [get_ports {fpga_AJ28}]
set_property IOSTANDARD LVDS [get_ports {fpga_AJ30}]
set_property IOSTANDARD LVDS [get_ports {fpga_AJ31}]
set_property IOSTANDARD LVDS [get_ports {fpga_AJ35}]
set_property IOSTANDARD LVDS [get_ports {fpga_AJ36}]
set_property IOSTANDARD LVDS [get_ports {fpga_AJ37}]
set_property IOSTANDARD LVDS [get_ports {fpga_AK20}]
set_property IOSTANDARD LVDS [get_ports {fpga_AK22}]
set_property IOSTANDARD LVDS [get_ports {fpga_AK23}]
set_property IOSTANDARD LVDS [get_ports {fpga_AK28}]
set_property IOSTANDARD LVDS [get_ports {fpga_AK29}]
set_property IOSTANDARD LVDS [get_ports {fpga_AK30}]
set_property IOSTANDARD LVDS [get_ports {fpga_AK35}]
set_property IOSTANDARD LVDS [get_ports {fpga_AK37}]
set_property IOSTANDARD LVDS [get_ports {fpga_AL20}]
set_property IOSTANDARD LVDS [get_ports {fpga_AL21}]
set_property IOSTANDARD LVDS [get_ports {fpga_AL22}]
set_property IOSTANDARD LVDS [get_ports {fpga_AL29}]
set_property IOSTANDARD LVDS [get_ports {fpga_AL30}]
set_property IOSTANDARD LVDS [get_ports {fpga_AL35}]
set_property IOSTANDARD LVDS [get_ports {fpga_AL36}]
set_property IOSTANDARD LVDS [get_ports {fpga_AL37}]
set_property IOSTANDARD LVDS [get_ports {fpga_AM21}]
set_property IOSTANDARD LVDS [get_ports {fpga_AM22}]
set_property IOSTANDARD LVDS [get_ports {fpga_AM23}]
set_property IOSTANDARD LVDS [get_ports {fpga_AM24}]
set_property IOSTANDARD LVDS [get_ports {fpga_AM32}]
set_property IOSTANDARD LVDS [get_ports {fpga_AM33}]
set_property IOSTANDARD LVDS [get_ports {fpga_AM34}]
set_property IOSTANDARD LVDS [get_ports {fpga_AM36}]
set_property IOSTANDARD LVDS [get_ports {fpga_AM37}]
set_property IOSTANDARD LVDS [get_ports {fpga_AM41}]
set_property IOSTANDARD LVDS [get_ports {fpga_AM42}]
set_property IOSTANDARD LVDS [get_ports {fpga_AN21}]
set_property IOSTANDARD LVDS [get_ports {fpga_AN23}]
set_property IOSTANDARD LVDS [get_ports {fpga_AN24}]
set_property IOSTANDARD LVDS [get_ports {fpga_AN30}]
set_property IOSTANDARD LVDS [get_ports {fpga_AN33}]
set_property IOSTANDARD LVDS [get_ports {fpga_AN35}]
set_property IOSTANDARD LVDS [get_ports {fpga_AN36}]
set_property IOSTANDARD LVDS [get_ports {fpga_AN38}]
set_property IOSTANDARD LVDS [get_ports {fpga_AN40}]
set_property IOSTANDARD LVDS [get_ports {fpga_AN41}]
set_property IOSTANDARD LVDS [get_ports {fpga_AP21}]
set_property IOSTANDARD LVDS [get_ports {fpga_AP22}]
set_property IOSTANDARD LVDS [get_ports {fpga_AP23}]
set_property IOSTANDARD LVDS [get_ports {fpga_AP30}]
set_property IOSTANDARD LVDS [get_ports {fpga_AP32}]
set_property IOSTANDARD LVDS [get_ports {fpga_AP35}]
set_property IOSTANDARD LVDS [get_ports {fpga_AP38}]
set_property IOSTANDARD LVDS [get_ports {fpga_AP40}]
set_property IOSTANDARD LVDS [get_ports {fpga_AR22}]
set_property IOSTANDARD LVDS [get_ports {fpga_AR23}]
set_property IOSTANDARD LVDS [get_ports {fpga_AR24}]
set_property IOSTANDARD LVDS [get_ports {fpga_AR30}]
set_property IOSTANDARD LVDS [get_ports {fpga_AR32}]
set_property IOSTANDARD LVDS [get_ports {fpga_AR33}]
set_property IOSTANDARD LVDS [get_ports {fpga_AR34}]
set_property IOSTANDARD LVDS [get_ports {fpga_AR38}]
set_property IOSTANDARD LVDS [get_ports {fpga_AR39}]
set_property IOSTANDARD LVDS [get_ports {fpga_AR40}]
set_property IOSTANDARD LVDS [get_ports {fpga_AR42}]
set_property IOSTANDARD LVDS [get_ports {fpga_AT21}]
set_property IOSTANDARD LVDS [get_ports {fpga_AT22}]
set_property IOSTANDARD LVDS [get_ports {fpga_AT24}]
set_property IOSTANDARD LVDS [get_ports {fpga_AT30}]
set_property IOSTANDARD LVDS [get_ports {fpga_AT32}]
set_property IOSTANDARD LVDS [get_ports {fpga_AT34}]
set_property IOSTANDARD LVDS [get_ports {fpga_AT40}]
set_property IOSTANDARD LVDS [get_ports {fpga_AT41}]
set_property IOSTANDARD LVDS [get_ports {fpga_AT42}]
set_property IOSTANDARD LVDS [get_ports {fpga_AU21}]
set_property IOSTANDARD LVDS [get_ports {fpga_AU22}]
set_property IOSTANDARD LVDS [get_ports {fpga_AU23}]
set_property IOSTANDARD LVDS [get_ports {fpga_AU24}]
set_property IOSTANDARD LVDS [get_ports {fpga_AU31}]
set_property IOSTANDARD LVDS [get_ports {fpga_AU38}]
set_property IOSTANDARD LVDS [get_ports {fpga_AU41}]
set_property IOSTANDARD LVDS [get_ports {fpga_AU42}]
set_property IOSTANDARD LVDS [get_ports {fpga_AV21}]
set_property IOSTANDARD LVDS [get_ports {fpga_AV23}]
set_property IOSTANDARD LVDS [get_ports {fpga_AV24}]
set_property IOSTANDARD LVDS [get_ports {fpga_AV31}]
set_property IOSTANDARD LVDS [get_ports {fpga_AV34}]
set_property IOSTANDARD LVDS [get_ports {fpga_AV36}]
set_property IOSTANDARD LVDS [get_ports {fpga_AV38}]
set_property IOSTANDARD LVDS [get_ports {fpga_AV40}]
set_property IOSTANDARD LVDS [get_ports {fpga_AV41}]
set_property IOSTANDARD LVDS [get_ports {fpga_AW21}]
set_property IOSTANDARD LVDS [get_ports {fpga_AW22}]
set_property IOSTANDARD LVDS [get_ports {fpga_AW23}]
set_property IOSTANDARD LVDS [get_ports {fpga_AW31}]
set_property IOSTANDARD LVDS [get_ports {fpga_AW32}]
set_property IOSTANDARD LVDS [get_ports {fpga_AW33}]
set_property IOSTANDARD LVDS [get_ports {fpga_AW35}]
set_property IOSTANDARD LVDS [get_ports {fpga_AW36}]
set_property IOSTANDARD LVDS [get_ports {fpga_AW37}]
set_property IOSTANDARD LVDS [get_ports {fpga_AW40}]
set_property IOSTANDARD LVDS [get_ports {fpga_AW41}]
set_property IOSTANDARD LVDS [get_ports {fpga_AW42}]
set_property IOSTANDARD LVDS [get_ports {fpga_AY22}]
set_property IOSTANDARD LVDS [get_ports {fpga_AY23}]
set_property IOSTANDARD LVDS [get_ports {fpga_AY24}]
set_property IOSTANDARD LVDS [get_ports {fpga_AY25}]
set_property IOSTANDARD LVDS [get_ports {fpga_AY32}]
set_property IOSTANDARD LVDS [get_ports {fpga_AY35}]
set_property IOSTANDARD LVDS [get_ports {fpga_AY37}]
set_property IOSTANDARD LVDS [get_ports {fpga_AY39}]
set_property IOSTANDARD LVDS [get_ports {fpga_BA21}]
set_property IOSTANDARD LVDS [get_ports {fpga_BA22}]
set_property IOSTANDARD LVDS [get_ports {fpga_BA25}]
set_property IOSTANDARD LVDS [get_ports {fpga_BA34}]
set_property IOSTANDARD LVDS [get_ports {fpga_BA35}]
set_property IOSTANDARD LVDS [get_ports {fpga_BA36}]
set_property IOSTANDARD LVDS [get_ports {fpga_BA39}]
set_property IOSTANDARD LVDS [get_ports {fpga_BA40}]
set_property IOSTANDARD LVDS [get_ports {fpga_BA41}]
set_property IOSTANDARD LVDS [get_ports {fpga_BA42}]
set_property IOSTANDARD LVDS [get_ports {fpga_BB21}]
set_property IOSTANDARD LVDS [get_ports {fpga_BB22}]
set_property IOSTANDARD LVDS [get_ports {fpga_BB23}]
set_property IOSTANDARD LVDS [get_ports {fpga_BB24}]
set_property IOSTANDARD LVDS [get_ports {fpga_BB32}]
set_property IOSTANDARD LVDS [get_ports {fpga_BB33}]
set_property IOSTANDARD LVDS [get_ports {fpga_BB34}]
set_property IOSTANDARD LVDS [get_ports {fpga_BB36}]
set_property IOSTANDARD LVDS [get_ports {fpga_BB37}]
set_property IOSTANDARD LVDS [get_ports {fpga_BB39}]
set_property IOSTANDARD LVDS [get_ports {fpga_BB41}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_F21}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_F24}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_J11}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_J16}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_K18}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_K20}]
#set_property IOSTANDARD LVDS [get_ports {fpga_J40}]
#set_property IOSTANDARD LVDS [get_ports {fpga_J41}]
#set_property IOSTANDARD LVDS [get_ports {fpga_L39}]
#set_property IOSTANDARD LVDS [get_ports {fpga_L40}]
#set_property IOSTANDARD LVDS [get_ports {fpga_M31}]
#set_property IOSTANDARD LVDS [get_ports {fpga_N30}]
#set_property IOSTANDARD LVDS [get_ports {fpga_N41}]
#set_property IOSTANDARD LVDS [get_ports {fpga_P41}]
##set_property IOSTANDARD LVDS [get_ports {fpga_K39}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AL31}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AM39}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AP41}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AP42}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AR35}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AR37}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AT36}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AT37}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AU39}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_BA37}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AN39}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AP33}]
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_AU36}]
#set_property IOSTANDARD LVCMOS18 [get_ports {fpga_J42}]
#set_property IOSTANDARD LVCMOS18 [get_ports {fpga_K42}]


#########################################################
set_property CFGBVS GND [current_design]
#where value1 is either VCCO or GND
set_property CONFIG_VOLTAGE 1.8 [current_design]
#where value2 is the voltage provided to configuration bank 0

create_clock -period 8.000 -name sgmiiclk -waveform {0.000 4.00} [get_ports {fpga_AH7}]
create_clock -period 5.000 -name sysclk -waveform {0.000 2.500} [get_ports  {fpga_E19}]
