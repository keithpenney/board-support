IOBUFDS ibufds2(.IO(fmcpin.H4),.IOB(fmcpin.H5),.O(fmc120.llmk_dclkout_2),.I(1'b0),.T(1'b1));
IOBUFDS ibufds3(.IO(fmcpin.G2),.IOB(fmcpin.G3),.O(fmc120.llmk_sclkout_3),.I(1'b0),.T(1'b1));
IOBUFDS ibufds_dacsyncreq(.IO(fmcpin.H7),.IOB(fmcpin.H8),.O(fmc120.dac_sync_req_to_fpga),.I(1'b0),.T(1'b1));
IOBUFDS obufds_fpgasync_out(.I(fmc120.fpga_sync_out_to_trigmux),.IO(fmcpin.D8),.IOB(fmcpin.D9),.T(1'b0),.O());
IBUFDS_GTE2 ibufds8(.I(fmcpin.D4),.IB(fmcpin.D5),.CEB(1'b0),.O(fmc120.lmk_dclk8_m2c_to_fpga),.ODIV2());
IBUFDS_GTE2 ibufds10(.I(fmcpin.B20),.IB(fmcpin.B21),.O(fmc120.lmk_dclk10_m2c_to_fpga),.ODIV2(),.CEB(1'b0));
/*viapairs #(.WIDTH(16))
daclanes({fmcpin.C2,fmc120.dac_lane_p[0]
,fmcpin.C3,fmc120.dac_lane_n[0]
,fmcpin.A22,fmc120.dac_lane_p[1]
,fmcpin.A23,fmc120.dac_lane_n[1]
,fmcpin.A26,fmc120.dac_lane_p[2]
,fmcpin.A27,fmc120.dac_lane_n[2]
,fmcpin.A30,fmc120.dac_lane_p[3]
,fmcpin.A31,fmc120.dac_lane_n[3]
,fmcpin.A34,fmc120.dac_lane_p[4]
,fmcpin.A35,fmc120.dac_lane_n[4]
,fmcpin.A38,fmc120.dac_lane_p[5]
,fmcpin.A39,fmc120.dac_lane_n[5]
,fmcpin.B36,fmc120.dac_lane_p[6]
,fmcpin.B37,fmc120.dac_lane_n[6]
,fmcpin.B32,fmc120.dac_lane_p[7]
,fmcpin.B33,fmc120.dac_lane_n[7]
});*/

obufbus #(.WIDTH(8))
daclaneobufn(.O({fmcpin.B33,fmcpin.B37,fmcpin.A39,fmcpin.A35,fmcpin.A31,fmcpin.A27,fmcpin.A23,fmcpin.C3}),.I(fmc120.dac_lane_n));
obufbus #(.WIDTH(8))
daclaneobufp(.O({fmcpin.B32,fmcpin.B36,fmcpin.A38,fmcpin.A34,fmcpin.A30,fmcpin.A26,fmcpin.A22,fmcpin.C2}),.I(fmc120.dac_lane_p));
//assign fmcpin.C2=fmc120.dac_lane_p[0];
//assign fmcpin.C3=fmc120.dac_lane_n[0];
//assign fmcpin.A22=fmc120.dac_lane_p[1];
//assign fmcpin.A23=fmc120.dac_lane_n[1];
//assign fmcpin.A26=fmc120.dac_lane_p[2];
//assign fmcpin.A27=fmc120.dac_lane_n[2];
//assign fmcpin.A30=fmc120.dac_lane_p[3];
//assign fmcpin.A31=fmc120.dac_lane_n[3];
//assign fmcpin.A34=fmc120.dac_lane_p[4];
//assign fmcpin.A35=fmc120.dac_lane_n[4];
//assign fmcpin.A38=fmc120.dac_lane_p[5];
//assign fmcpin.A39=fmc120.dac_lane_n[5];
//assign fmcpin.B36=fmc120.dac_lane_p[6];
//assign fmcpin.B37=fmc120.dac_lane_n[6];
//assign fmcpin.B32=fmc120.dac_lane_p[7];
//assign fmcpin.B33=fmc120.dac_lane_n[7];
ibufbus #(.WIDTH(16))
ibufadc (.O({fmc120.adc0_da1_p,fmc120.adc0_da1_n,fmc120.adc0_da2_p,fmc120.adc0_da2_n,fmc120.adc0_db1_p,fmc120.adc0_db1_n,fmc120.adc0_db2_p,fmc120.adc0_db2_n,fmc120.adc1_da1_p,fmc120.adc1_da1_n,fmc120.adc1_da2_p,fmc120.adc1_da2_n,fmc120.adc1_db1_p,fmc120.adc1_db1_n,fmc120.adc1_db2_p,fmc120.adc1_db2_n}),.I({fmcpin.C6,fmcpin.C7,fmcpin.A2,fmcpin.A3,fmcpin.A6,fmcpin.A7,fmcpin.A10,fmcpin.A11,fmcpin.A14,fmcpin.A15,fmcpin.A18,fmcpin.A19,fmcpin.B16,fmcpin.B17,fmcpin.B12,fmcpin.B13}));

//assign fmc120.adc0_da1_p=fmcpin.C6;
//assign fmc120.adc0_da1_n=fmcpin.C7;
//assign fmc120.adc0_da2_p=fmcpin.A2;
//assign fmc120.adc0_da2_n=fmcpin.A3;
//assign fmc120.adc0_db1_p=fmcpin.A6;
//assign fmc120.adc0_db1_n=fmcpin.A7;
//assign fmc120.adc0_db2_p=fmcpin.A10;
//assign fmc120.adc0_db2_n=fmcpin.A11;
//assign fmc120.adc1_da1_p=fmcpin.A14;
//assign fmc120.adc1_da1_n=fmcpin.A15;
//assign fmc120.adc1_da2_p=fmcpin.A18;
//assign fmc120.adc1_da2_n=fmcpin.A19;
//assign fmc120.adc1_db1_p=fmcpin.B16;
//assign fmc120.adc1_db1_n=fmcpin.B17;
//assign fmc120.adc1_db2_p=fmcpin.B12;
//assign fmc120.adc1_db2_n=fmcpin.B13;

//IBUFDS ibufdsextrig (.I(fmcpin.G6),.IB(fmcpin.G7),.O(fmc120.buf_ext_trig_to_fpga));
IOBUFDS ibufdsextrig (.IO(fmcpin.G6),.IOB(fmcpin.G7),.O(fmc120.buf_ext_trig_to_fpga),.I(1'b0),.T(1'b1));

ibufbus #(.WIDTH(4))
ibufcpldctrl(.O(fmc120.cpld_ctrl),.I({fmcpin.H11,fmcpin.H10,fmcpin.G10,fmcpin.G9}));
//assign fmc120.cpld_ctrl[0]=fmcpin.G9;
//assign fmc120.cpld_ctrl[1]=fmcpin.G10;
//assign fmc120.cpld_ctrl[2]=fmcpin.H10;
//assign fmc120.cpld_ctrl[3]=fmcpin.H11;
/*
assign fmcpin.G9=fmc120.cpld_ctrl[0];
assign fmcpin.G10=fmc120.cpld_ctrl[1];
assign fmcpin.H10=fmc120.cpld_ctrl[2];
assign fmcpin.H11=fmc120.cpld_ctrl[3];
*/
/*viapairs #(.WIDTH(4))
viacpldctrl({fmcpin.G9,fmc120.cpld_ctrl[0]
,fmcpin.G10,fmc120.cpld_ctrl[1]
,fmcpin.H10,fmc120.cpld_ctrl[2]
,fmcpin.H11,fmc120.cpld_ctrl[3]}
);
*/
//assign fmcpin.D12=fmc120.fpga_sync_out_to_lmk_vadj;
OBUF fmcsyncoutobuf(.O(fmcpin.D12),.I(fmc120.fpga_sync_out_to_lmk_vadj));
OBUF adcasyncobuf(.O(fmcpin.C11),.I(fmc120.adca_sync_in_l_vadj));
OBUF adcbsyncobuf(.O(fmcpin.C10),.I(fmc120.adcb_sync_in_l_vadj));
//assign fmcpin.C10=fmc120.adcb_sync_in_l_vadj;
//assign fmcpin.C11=fmc120.adca_sync_in_l_vadj;
//assign fmc120.adcb_sdout_b_ovr_vadj=fmcpin.H14;
//assign fmc120.adca_sdout_b_ovr_vadj=fmcpin.G13;
ibufbus #(.WIDTH(2))
ibufadcsdout(.O({fmc120.adcb_sdout_b_ovr_vadj,fmc120.adca_sdout_b_ovr_vadj}),.I({fmcpin.H14,fmcpin.G13}));
obufbus #(.WIDTH(3))
fmcobuf (.O({fmcpin.H13,fmcpin.G12,fmcpin.D14})//,fmcpin.D1})
,.I({fmc120.adcb_power_down_a_ovr_vadj,fmc120.adca_power_down_a_ovr_vadj,fmc120.dac_txen_vadj}));//fmc120.pg_c2m}));

//assign fmcpin.B40=fmc120.reset;  not implemented, not on fmc standard

//assign fmcpin.H13=fmc120.adcb_power_down_a_ovr_vadj;
//assign fmcpin.G12=fmc120.adca_power_down_a_ovr_vadj;
//assign fmcpin.D14=fmc120.dac_txen_vadj;
//assign fmcpin.D1=fmc120.pg_c2m;

ibufbus #(.WIDTH(2))
ibuffmcprsntpg (.O({fmc120.prsnt,fmc120.pg_m2c}),.I({fmcpin.H2,fmcpin.F1}));
//assign fmc120.prsnt=fmcpin.H2;
//assign fmc120.pg_m2c=fmcpin.F1;
viapairs #(.WIDTH(2))
viai2c({fmcpin.C30,fmc120.i2c_scl
,fmcpin.C31,fmc120.i2c_sda
});
