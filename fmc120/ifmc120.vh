wire llmk_dclkout_2;
wire llmk_sclkout_3;
wire lmk_dclk8_m2c_to_fpga;
wire lmk_dclk10_m2c_to_fpga;
wire [7:0] dac_lane_p;
wire [7:0] dac_lane_n;
wire adc0_da1_p;
wire adc0_da1_n;
wire adc0_da2_p;
wire adc0_da2_n;
wire adc0_db1_p;
wire adc0_db1_n;
wire adc0_db2_p;
wire adc0_db2_n;
wire adc1_da1_p;
wire adc1_da1_n;
wire adc1_da2_p;
wire adc1_da2_n;
wire adc1_db1_p;
wire adc1_db1_n;
wire adc1_db2_p;
wire adc1_db2_n;

wire buf_ext_trig_to_fpga;
wire fpga_sync_out_to_trigmux;
wire dac_sync_req_to_fpga;

wire [3:0] cpld_ctrl;

wire fpga_sync_out_to_lmk_vadj;
wire adcb_sync_in_l_vadj;
wire adca_sync_in_l_vadj;
wire adcb_power_down_a_ovr_vadj;
wire adcb_sdout_b_ovr_vadj;
wire adca_power_down_a_ovr_vadj;
wire adca_sdout_b_ovr_vadj;
wire dac_txen_vadj;
wire prsnt;
wire pg_m2c;
wire pg_c2m;
wire i2c_scl;
wire i2c_sda;
wire reset;
modport hw(
output llmk_dclkout_2,llmk_sclkout_3,lmk_dclk8_m2c_to_fpga,lmk_dclk10_m2c_to_fpga,adc0_da1_p,adc0_da1_n,adc0_da2_p,adc0_da2_n,adc0_db1_p,adc0_db1_n,adc0_db2_p,adc0_db2_n,adc1_da1_p,adc1_da1_n,adc1_da2_p,adc1_da2_n,adc1_db1_p,adc1_db1_n,adc1_db2_p,adc1_db2_n,buf_ext_trig_to_fpga,dac_sync_req_to_fpga,adcb_sdout_b_ovr_vadj,adca_sdout_b_ovr_vadj,prsnt,pg_m2c
,inout cpld_ctrl,i2c_scl,i2c_sda
,input dac_lane_p, dac_lane_n,fpga_sync_out_to_trigmux,fpga_sync_out_to_lmk_vadj,adcb_power_down_a_ovr_vadj,adca_power_down_a_ovr_vadj,dac_txen_vadj,pg_c2m,reset,adcb_sync_in_l_vadj,adca_sync_in_l_vadj
);
modport cfg(
input llmk_dclkout_2,llmk_sclkout_3,lmk_dclk8_m2c_to_fpga,lmk_dclk10_m2c_to_fpga,adc0_da1_p,adc0_da1_n,adc0_da2_p,adc0_da2_n,adc0_db1_p,adc0_db1_n,adc0_db2_p,adc0_db2_n,adc1_da1_p,adc1_da1_n,adc1_da2_p,adc1_da2_n,adc1_db1_p,adc1_db1_n,adc1_db2_p,adc1_db2_n,buf_ext_trig_to_fpga,dac_sync_req_to_fpga,adcb_sdout_b_ovr_vadj,adca_sdout_b_ovr_vadj,prsnt,pg_m2c
,inout cpld_ctrl,i2c_scl,i2c_sda
,output dac_lane_p, dac_lane_n,fpga_sync_out_to_trigmux,fpga_sync_out_to_lmk_vadj,adcb_power_down_a_ovr_vadj,adca_power_down_a_ovr_vadj,dac_txen_vadj,pg_c2m,reset,adcb_sync_in_l_vadj,adca_sync_in_l_vadj
);
modport sim(
input llmk_dclkout_2,llmk_sclkout_3,lmk_dclk8_m2c_to_fpga,lmk_dclk10_m2c_to_fpga,adc0_da1_p,adc0_da1_n,adc0_da2_p,adc0_da2_n,adc0_db1_p,adc0_db1_n,adc0_db2_p,adc0_db2_n,adc1_da1_p,adc1_da1_n,adc1_da2_p,adc1_da2_n,adc1_db1_p,adc1_db1_n,adc1_db2_p,adc1_db2_n,buf_ext_trig_to_fpga,dac_sync_req_to_fpga,adcb_sdout_b_ovr_vadj,adca_sdout_b_ovr_vadj,prsnt,pg_m2c
,inout cpld_ctrl,i2c_scl,i2c_sda
,output dac_lane_p, dac_lane_n,fpga_sync_out_to_trigmux,fpga_sync_out_to_lmk_vadj,adcb_power_down_a_ovr_vadj,adca_power_down_a_ovr_vadj,dac_txen_vadj,pg_c2m,reset,adcb_sync_in_l_vadj,adca_sync_in_l_vadj
);
