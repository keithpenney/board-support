assign fmcpin.H4=fmc120.llmk_dclkout_2;
assign fmcpin.H5=~fmc120.llmk_dclkout_2;
assign fmcpin.G2=fmc120.llmk_sclkout_3;
assign fmcpin.G3=~fmc120.llmk_sclkout_3;
assign fmcpin.D4=fmc120.lmk_dclk8_m2c_to_fpga;
assign fmcpin.D5=~fmc120.lmk_dclk8_m2c_to_fpga;
assign fmcpin.B20=fmc120.lmk_dclk10_m2c_to_fpga;
assign fmcpin.B21=fmc120.lmk_dclk10_m2c_to_fpga;
assign fmc120.dac_lane_p[0] = fmcpin.C2;
assign fmc120.dac_lane_n[0] = fmcpin.C3;
assign fmc120.dac_lane_p[1] = fmcpin.A22;
assign fmc120.dac_lane_n[1] = fmcpin.A23;
assign fmc120.dac_lane_p[2] = fmcpin.A26;
assign fmc120.dac_lane_n[2] = fmcpin.A27;
assign fmc120.dac_lane_p[3] = fmcpin.A30;
assign fmc120.dac_lane_n[3] = fmcpin.A31;
assign fmc120.dac_lane_p[4] = fmcpin.A34;
assign fmc120.dac_lane_n[4] = fmcpin.A35;
assign fmc120.dac_lane_p[5] = fmcpin.A38;
assign fmc120.dac_lane_n[5] = fmcpin.A39;
assign fmc120.dac_lane_p[6] = fmcpin.B36;
assign fmc120.dac_lane_n[6] = fmcpin.B37;
assign fmc120.dac_lane_p[7] = fmcpin.B32;
assign fmc120.dac_lane_n[7] = fmcpin.B33;
assign fmcpin.C6 = fmc120.adc0_da1_p;
assign fmcpin.C7 = fmc120.adc0_da1_n;
assign fmcpin.A2 = fmc120.adc0_da2_p;
assign fmcpin.A3 = fmc120.adc0_da2_n;
assign fmcpin.A6 = fmc120.adc0_db1_p;
assign fmcpin.A7 = fmc120.adc0_db1_n;
assign fmcpin.A10 = fmc120.adc0_db2_p;
assign fmcpin.A11 = fmc120.adc0_db2_n;
assign fmcpin.A14 = fmc120.adc1_da1_p;
assign fmcpin.A15 = fmc120.adc1_da1_n;
assign fmcpin.A18 = fmc120.adc1_da2_p;
assign fmcpin.A19 = fmc120.adc1_da2_n;
assign fmcpin.B16 = fmc120.adc1_db1_p;
assign fmcpin.B17 = fmc120.adc1_db1_n;
assign fmcpin.B12 = fmc120.adc1_db2_p;
assign fmcpin.B13 = fmc120.adc1_db2_n;
assign fmcpin.G6=fmc120.buf_ext_trig_to_fpga;
assign fmcpin.G7=fmc120.buf_ext_trig_to_fpga;
assign fmc120.fpga_sync_out_to_trigmux=fmcpin.D8; //~D9
assign fmcpin.H7=fmc120.dac_sync_req_to_fpga;
assign fmcpin.H8=~fmc120.dac_sync_req_to_fpga;
assign fmcpin.G9 = fmc120.cpld_ctrl[0];
assign fmcpin.G10 = fmc120.cpld_ctrl[1];
assign fmcpin.H10 = fmc120.cpld_ctrl[2];
assign fmcpin.H11 = fmc120.cpld_ctrl[3];
/*
assign fmc120.cpld_ctrl[0] = fmcpin.G9;
assign fmc120.cpld_ctrl[1] = fmcpin.G10;
assign fmc120.cpld_ctrl[2] = fmcpin.H10;
assign fmc120.cpld_ctrl[3] = fmcpin.H11;
*/
/*viapairs #(.WIDTH(4))
viacpldctrl({fmcpin.G9,fmc120.cpld_ctrl[0]
,fmcpin.G10,fmc120.cpld_ctrl[1]
,fmcpin.H10,fmc120.cpld_ctrl[2]
,fmcpin.H11,fmc120.cpld_ctrl[3]}
);
*/
assign fmc120.fpga_sync_out_to_lmk_vadj = fmcpin.D12;
assign fmc120.adcb_sync_in_l_vadj = fmcpin.C10;
assign fmc120.adca_sync_in_l_vadj = fmcpin.C11;
assign fmc120.adcb_power_down_a_ovr_vadj = fmcpin.H13;
assign fmcpin.H14 = fmc120.adcb_sdout_b_ovr_vadj;
assign fmc120.adca_power_down_a_ovr_vadj = fmcpin.G12;
assign fmcpin.G13 = fmc120.adca_sdout_b_ovr_vadj;
assign fmc120.dac_txen_vadj = fmcpin.D14;
assign fmcpin.H2 = fmc120.prsnt;
assign fmcpin.F1 = fmc120.pg_m2c;
assign fmc120.pg_c2m = fmcpin.D1;
viapairs #(.WIDTH(2))
viai2c({fmcpin.C30,fmc120.i2c_scl
,fmcpin.C31,fmc120.i2c_sda
});
assign fmc120.reset = fmcpin.B40;
