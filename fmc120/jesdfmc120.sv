module jesdfmc120(
input qpll_refclk
,input core_clk
,input drpclk
,input tx_sysref
,input rx_sysref
,input [7:0] rxp_in
,input [7:0] rxn_in
,output [7:0] txp_out
,output [7:0] txn_out
,output [63:0] adc0
,output [63:0] adc1
,output [63:0] adc2
,output [63:0] adc3
,output adc01_valid
,output adc23_valid
,input tx_sys_reset
,input rx_sys_reset
,input rx_reset
,input tx_reset
,output rx_aresetn_0
,output rx_aresetn_1
,output tx_aresetn
,input [63:0] dac0
,input [63:0] dac1
,input [63:0] dac2
,input [63:0] dac3
,output tx_tready
,output common0_qpll_lock_out
,output common1_qpll_lock_out
,output [31:0] rx_frame_error
,output [1:0] rx_sync
,input tx_sync
,axi4lite axi_adc0
,axi4lite axi_adc1
,axi4lite axi_dac
,output reset_done
,output [1:0] reset_status
,output rxencommaalign_0
,output rxencommaalign_1
,output [31:0] dbgt0_rxdata
,output [31:0] dbgt0_txdata
,output [31:0] dbgt1_rxdata
,output [31:0] dbgt1_txdata
,output [31:0] dbgt2_rxdata
,output [31:0] dbgt2_txdata
,output [31:0] dbgt3_rxdata
,output [31:0] dbgt3_txdata
,output [31:0] dbgt4_rxdata
,output [31:0] dbgt4_txdata
,output [31:0] dbgt5_rxdata
,output [31:0] dbgt5_txdata
,output [31:0] dbgt6_rxdata
,output [31:0] dbgt6_txdata
,output [31:0] dbgt7_rxdata
,output [31:0] dbgt7_txdata
);
wire [3:0] gt0_rxcharisk,gt0_rxdisperr,gt0_rxnotintable,gt0_txcharisk,gt1_rxcharisk,gt1_rxdisperr,gt1_rxnotintable,gt1_txcharisk,gt2_rxcharisk,gt2_rxdisperr,gt2_rxnotintable,gt2_txcharisk,gt3_rxcharisk,gt3_rxdisperr,gt3_rxnotintable,gt3_txcharisk,gt4_rxcharisk,gt4_rxdisperr,gt4_rxnotintable,gt4_txcharisk,gt5_rxcharisk,gt5_rxdisperr,gt5_rxnotintable,gt5_txcharisk,gt6_rxcharisk,gt6_rxdisperr,gt6_rxnotintable,gt6_txcharisk,gt7_rxcharisk,gt7_rxdisperr,gt7_rxnotintable,gt7_txcharisk;
wire [31:0] gt0_rxdata,gt0_txdata,gt1_rxdata,gt1_txdata,gt2_rxdata,gt2_txdata,gt3_rxdata,gt3_txdata,gt4_rxdata,gt4_txdata,gt5_rxdata,gt5_txdata,gt6_rxdata,gt6_txdata,gt7_rxdata,gt7_txdata;
assign {dbgt0_rxdata,dbgt0_txdata,dbgt1_rxdata,dbgt1_txdata,dbgt2_rxdata,dbgt2_txdata,dbgt3_rxdata,dbgt3_txdata,dbgt4_rxdata,dbgt4_txdata,dbgt5_rxdata,dbgt5_txdata,dbgt6_rxdata,dbgt6_txdata,dbgt7_rxdata,dbgt7_txdata}={gt0_rxdata,gt0_txdata,gt1_rxdata,gt1_txdata,gt2_rxdata,gt2_txdata,gt3_rxdata,gt3_txdata,gt4_rxdata,gt4_txdata,gt5_rxdata,gt5_txdata,gt6_rxdata,gt6_txdata,gt7_rxdata,gt7_txdata};
wire [2:0] gt_prbssel;
wire tx_reset_gt;
wire tx_reset_done;
wire rx_reset_gt_0;
wire rx_reset_gt_1;
wire rx_reset_done;
assign reset_done=&{tx_reset_done,rx_reset_done};//rx_reset_gt_0,rx_reset_gt_1,
assign reset_status={tx_reset_done,rx_reset_done};//rx_reset_gt_0,rx_reset_gt_1,

jesd204_phy_8lane
jesd204_phy_8lane(.gt0_rxcharisk,.gt0_rxdata,.gt0_rxdisperr,.gt0_rxnotintable,.gt0_txcharisk,.gt0_txdata,.gt1_rxcharisk,.gt1_rxdata,.gt1_rxdisperr,.gt1_rxnotintable,.gt1_txcharisk,.gt1_txdata,.gt2_rxcharisk,.gt2_rxdata,.gt2_rxdisperr,.gt2_rxnotintable,.gt2_txcharisk,.gt2_txdata,.gt3_rxcharisk,.gt3_rxdata,.gt3_rxdisperr,.gt3_rxnotintable,.gt3_txcharisk,.gt3_txdata,.gt4_rxcharisk,.gt4_rxdata,.gt4_rxdisperr,.gt4_rxnotintable,.gt4_txcharisk,.gt4_txdata,.gt5_rxcharisk,.gt5_rxdata,.gt5_rxdisperr,.gt5_rxnotintable,.gt5_txcharisk,.gt5_txdata,.gt6_rxcharisk,.gt6_rxdata,.gt6_rxdisperr,.gt6_rxnotintable,.gt6_txcharisk,.gt6_txdata,.gt7_rxcharisk,.gt7_rxdata,.gt7_rxdisperr,.gt7_rxnotintable,.gt7_txcharisk,.gt7_txdata
,.gt_prbssel
,.rxn_in(rxn_in),.rxp_in(rxp_in),.txn_out(txn_out),.txp_out(txp_out)
,.rxencommaalign(rxencommaalign_0|rxencommaalign_1),.rx_reset_gt(rx_reset_gt_0|rx_reset_gt_1),.rx_reset_done,.tx_reset_done,.tx_reset_gt
,.rx_sys_reset,.tx_sys_reset
,.qpll_refclk
,.rx_core_clk(core_clk)
,.tx_core_clk(core_clk)
,.rxoutclk()
,.txoutclk()
,.common0_qpll_clk_out()
,.common0_qpll_refclk_out()
,.common1_qpll_clk_out()
,.common1_qpll_refclk_out()
,.common0_qpll_lock_out
,.common1_qpll_lock_out
,.drpclk
);
wire [127:0] rx_tdata0;
assign {adc1[63:56],adc1[47:40],adc1[31:24],adc1[15:8],adc1[55:48],adc1[39:32],adc1[23:16],adc1[7:0],adc0[63:56],adc0[47:40],adc0[31:24],adc0[15:8],adc0[55:48],adc0[39:32],adc0[23:16],adc0[7:0]}=rx_tdata0;
jesd204_ads54j60
jesd204_ads54j60_0(.gt0_rxcharisk,.gt0_rxdata,.gt0_rxdisperr,.gt0_rxnotintable,.gt1_rxcharisk,.gt1_rxdata,.gt1_rxdisperr,.gt1_rxnotintable,.gt2_rxcharisk,.gt2_rxdata,.gt2_rxdisperr,.gt2_rxnotintable,.gt3_rxcharisk,.gt3_rxdata,.gt3_rxdisperr,.gt3_rxnotintable
,.s_axi_aclk(axi_adc0.aclk),.s_axi_araddr(axi_adc0.araddr),.s_axi_aresetn(axi_adc0.aresetn),.s_axi_arready(axi_adc0.arready),.s_axi_arvalid(axi_adc0.arvalid),.s_axi_awaddr(axi_adc0.awaddr),.s_axi_awready(axi_adc0.awready),.s_axi_awvalid(axi_adc0.awvalid),.s_axi_bready(axi_adc0.bready),.s_axi_bresp(axi_adc0.bresp),.s_axi_bvalid(axi_adc0.bvalid),.s_axi_rdata(axi_adc0.rdata),.s_axi_rready(axi_adc0.rready),.s_axi_rresp(axi_adc0.rresp),.s_axi_rvalid(axi_adc0.rvalid),.s_axi_wdata(axi_adc0.wdata),.s_axi_wready(axi_adc0.wready),.s_axi_wstrb(axi_adc0.wstrb),.s_axi_wvalid(axi_adc0.wvalid)
,.rxencommaalign_out(rxencommaalign_0)
,.rx_reset_done
,.rx_reset_gt(rx_reset_gt_0)
,.rx_aresetn(rx_aresetn_0)
,.rx_reset
,.rx_core_clk(core_clk)
,.rx_tdata(rx_tdata0)
//,.rx_tdata({adc1[63:56],adc1[47:40],adc1[31:24],adc1[15:8],adc1[55:48],adc1[39:32],adc1[23:16],adc1[7:0],adc0[63:56],adc0[47:40],adc0[31:24],adc0[15:8],adc0[55:48],adc0[39:32],adc0[23:16],adc0[7:0]})
,.rx_tvalid(adc01_valid)
,.rx_sysref
,.rx_start_of_frame()
,.rx_start_of_multiframe()
,.rx_end_of_frame()
,.rx_end_of_multiframe()
,.rx_frame_error(rx_frame_error[15:0])
,.rx_sync(rx_sync[0])
);
wire [127:0] rx_tdata1;
assign {adc3[63:56],adc3[47:40],adc3[31:24],adc3[15:8],adc3[55:48],adc3[39:32],adc3[23:16],adc3[7:0],adc2[63:56],adc2[47:40],adc2[31:24],adc2[15:8],adc2[55:48],adc2[39:32],adc2[23:16],adc2[7:0]}=rx_tdata1;
jesd204_ads54j60
jesd204_ads54j60_1(
.gt0_rxcharisk(gt4_rxcharisk),.gt0_rxdata(gt4_rxdata),.gt0_rxdisperr(gt4_rxdisperr),.gt0_rxnotintable(gt4_rxnotintable),.gt1_rxcharisk(gt5_rxcharisk),.gt1_rxdata(gt5_rxdata),.gt1_rxdisperr(gt5_rxdisperr),.gt1_rxnotintable(gt5_rxnotintable),.gt2_rxcharisk(gt6_rxcharisk),.gt2_rxdata(gt6_rxdata),.gt2_rxdisperr(gt6_rxdisperr),.gt2_rxnotintable(gt6_rxnotintable),.gt3_rxcharisk(gt7_rxcharisk),.gt3_rxdata(gt7_rxdata),.gt3_rxdisperr(gt7_rxdisperr),.gt3_rxnotintable(gt7_rxnotintable)
,.rx_tdata(rx_tdata1)
//,.rx_tdata({adc3[63:56],adc3[47:40],adc3[31:24],adc3[15:8],adc3[55:48],adc3[39:32],adc3[23:16],adc3[7:0],adc2[63:56],adc2[47:40],adc2[31:24],adc2[15:8],adc2[55:48],adc2[39:32],adc2[23:16],adc2[7:0]})
,.rx_tvalid(adc23_valid)
,.rxencommaalign_out(rxencommaalign_1)
,.s_axi_aclk(axi_adc1.aclk),.s_axi_araddr(axi_adc1.araddr),.s_axi_aresetn(axi_adc1.aresetn),.s_axi_arready(axi_adc1.arready),.s_axi_arvalid(axi_adc1.arvalid),.s_axi_awaddr(axi_adc1.awaddr),.s_axi_awready(axi_adc1.awready),.s_axi_awvalid(axi_adc1.awvalid),.s_axi_bready(axi_adc1.bready),.s_axi_bresp(axi_adc1.bresp),.s_axi_bvalid(axi_adc1.bvalid),.s_axi_rdata(axi_adc1.rdata),.s_axi_rready(axi_adc1.rready),.s_axi_rresp(axi_adc1.rresp),.s_axi_rvalid(axi_adc1.rvalid),.s_axi_wdata(axi_adc1.wdata),.s_axi_wready(axi_adc1.wready),.s_axi_wstrb(axi_adc1.wstrb),.s_axi_wvalid(axi_adc1.wvalid)
,.rx_aresetn(rx_aresetn_1)
,.rx_reset
,.rx_reset_done
,.rx_reset_gt(rx_reset_gt_1)
,.rx_core_clk(core_clk)
,.rx_start_of_frame()
,.rx_start_of_multiframe()
,.rx_end_of_frame()
,.rx_end_of_multiframe()
,.rx_frame_error(rx_frame_error[31:16])
,.rx_sync(rx_sync[1])
,.rx_sysref
);

jesd204_dac39j84 jesd204_dac39j84(
	.gt0_txcharisk,.gt0_txdata,.gt1_txcharisk,.gt1_txdata,.gt2_txcharisk,.gt2_txdata,.gt3_txcharisk,.gt3_txdata,.gt4_txcharisk,.gt4_txdata,.gt5_txcharisk,.gt5_txdata,.gt6_txcharisk,.gt6_txdata,.gt7_txcharisk,.gt7_txdata
,.gt_prbssel_out(gt_prbssel)
,.s_axi_aclk(axi_dac.aclk),.s_axi_araddr(axi_dac.araddr),.s_axi_aresetn(axi_dac.aresetn),.s_axi_arready(axi_dac.arready),.s_axi_arvalid(axi_dac.arvalid),.s_axi_awaddr(axi_dac.awaddr),.s_axi_awready(axi_dac.awready),.s_axi_awvalid(axi_dac.awvalid),.s_axi_bready(axi_dac.bready),.s_axi_bresp(axi_dac.bresp),.s_axi_bvalid(axi_dac.bvalid),.s_axi_rdata(axi_dac.rdata),.s_axi_rready(axi_dac.rready),.s_axi_rresp(axi_dac.rresp),.s_axi_rvalid(axi_dac.rvalid),.s_axi_wdata(axi_dac.wdata),.s_axi_wready(axi_dac.wready),.s_axi_wstrb(axi_dac.wstrb),.s_axi_wvalid(axi_dac.wvalid)
,.tx_aresetn(tx_aresetn),.tx_reset,.tx_reset_done,.tx_reset_gt
,.tx_tdata({dac3[55:48], dac3[39:32], dac3[23:16], dac3[7:0],dac3[63:56], dac3[47:40], dac3[31:24], dac3[15:8],dac2[55:48], dac2[39:32], dac2[23:16], dac2[7:0],dac2[63:56], dac2[47:40], dac2[31:24], dac2[15:8],dac1[55:48], dac1[39:32], dac1[23:16], dac1[7:0],dac1[63:56], dac1[47:40], dac1[31:24], dac1[15:8],dac0[55:48], dac0[39:32], dac0[23:16], dac0[7: 0],dac0[63:56], dac0[47:40], dac0[31:24], dac0[15:8]})
,.tx_tready
,.tx_core_clk(core_clk)
,.tx_start_of_frame()
,.tx_start_of_multiframe()
,.tx_sync
,.tx_sysref
);
endmodule
