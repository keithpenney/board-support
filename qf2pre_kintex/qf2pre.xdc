set_property CONFIG_VOLTAGE  3.3 [current_design]
set_property CFGBVS VCCO [current_design]
create_clock -period 20.000 -name SYSCLK -waveform {0.000 10.000} [get_ports {fpga\.G11}]
create_clock -period 5.385 -name "SI57X_A" [get_ports {fpga\.H6}]
