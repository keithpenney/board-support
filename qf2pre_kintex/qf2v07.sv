module qf2v07#(parameter SIMULATION="")(fpga fpga
,ifqf2v07.hw hw
,iffmc.mastersig fmc1
,iffmc.mastersig fmc2
,ifqsfp qsfp1
,ifqsfp qsfp2
);


// fmc1 LPC
viapairs #(.WIDTH(68)) fmc1_via({fmc1.LA00_N_CC,fpga.P21
,fmc1.LA00_P_CC,fpga.R21
,fmc1.LA01_N_CC,fpga.N22
,fmc1.LA01_P_CC,fpga.N21
,fmc1.LA02_N,fpga.P25
,fmc1.LA02_P,fpga.R25
,fmc1.LA03_N,fpga.P26
,fmc1.LA03_P,fpga.R26
,fmc1.LA04_N,fpga.U20
,fmc1.LA04_P,fpga.U19
,fmc1.LA05_N,fpga.R20
,fmc1.LA05_P,fpga.T20
,fmc1.LA06_N,fpga.M26
,fmc1.LA06_P,fpga.N26
,fmc1.LA07_N,fpga.N23
,fmc1.LA07_P,fpga.P23
,fmc1.LA08_N,fpga.T23
,fmc1.LA08_P,fpga.T22
,fmc1.LA09_N,fpga.N24
,fmc1.LA09_P,fpga.P24
,fmc1.LA10_N,fpga.P20
,fmc1.LA10_P,fpga.P19
,fmc1.LA11_N,fpga.M22
,fmc1.LA11_P,fpga.M21
,fmc1.LA12_N,fpga.P18
,fmc1.LA12_P,fpga.R18
,fmc1.LA13_N,fpga.K26
,fmc1.LA13_P,fpga.K25
,fmc1.LA14_N,fpga.T17
,fmc1.LA14_P,fpga.U17
,fmc1.LA15_N,fpga.L24
,fmc1.LA15_P,fpga.M24
,fmc1.LA16_N,fpga.L25
,fmc1.LA16_P,fpga.M25
,fmc1.LA17_N_CC,fpga.E23
,fmc1.LA17_P_CC,fpga.F22
,fmc1.LA18_N_CC,fpga.F23
,fmc1.LA18_P_CC,fpga.G22
,fmc1.LA19_N,fpga.G26
,fmc1.LA19_P,fpga.G25
,fmc1.LA20_N,fpga.H26
,fmc1.LA20_P,fpga.J26
,fmc1.LA21_N,fpga.G21
,fmc1.LA21_P,fpga.H21
,fmc1.LA22_N,fpga.H24
,fmc1.LA22_P,fpga.H23
,fmc1.LA23_N,fpga.F24
,fmc1.LA23_P,fpga.G24
,fmc1.LA24_N,fpga.C24
,fmc1.LA24_P,fpga.C23
,fmc1.LA25_N,fpga.C26
,fmc1.LA25_P,fpga.D26
,fmc1.LA26_N,fpga.D25
,fmc1.LA26_P,fpga.E25
,fmc1.LA27_N,fpga.E22
,fmc1.LA27_P,fpga.E21
,fmc1.LA28_N,fpga.A24
,fmc1.LA28_P,fpga.A23
,fmc1.LA29_N,fpga.A25
,fmc1.LA29_P,fpga.B24
,fmc1.LA30_N,fpga.A22
,fmc1.LA30_P,fpga.B22
,fmc1.LA31_N,fpga.C22
,fmc1.LA31_P,fpga.D21
,fmc1.LA32_N,fpga.A20
,fmc1.LA32_P,fpga.B20
,fmc1.LA33_N,fpga.B21
,fmc1.LA33_P,fpga.C21
});
//viapairs #(.WIDTH(61)) fmc1_via({fmc1.LA00_N_CC,fpga.P21,fmc1.LA00_P_CC,fpga.R21,fmc1.LA01_N_CC,fpga.N22,fmc1.LA01_P_CC,fpga.N21,fmc1.LA02_N,fpga.P25,fmc1.LA02_P,fpga.R25,fmc1.LA03_N,fpga.P26,fmc1.LA03_P,fpga.R26,fmc1.LA04_N,fpga.U20,fmc1.LA04_P,fpga.U19,fmc1.LA05_N,fpga.R20,fmc1.LA05_P,fpga.T20,fmc1.LA06_N,fpga.M26,fmc1.LA06_P,fpga.N26,fmc1.LA07_N,fpga.N23,fmc1.LA07_P,fpga.P23,fmc1.LA08_N,fpga.T23,fmc1.LA08_P,fpga.T22,fmc1.LA09_N,fpga.N24,fmc1.LA09_P,fpga.P24,fmc1.LA10_N,fpga.P20,fmc1.LA10_P,fpga.P19,fmc1.LA11_N,fpga.M22,fmc1.LA11_P,fpga.M21,fmc1.LA14_N,fpga.T17,fmc1.LA14_P,fpga.U17,fmc1.LA15_N,fpga.L24,fmc1.LA15_P,fpga.M24,fmc1.LA16_N,fpga.L25,fmc1.LA16_P,fpga.M25,fmc1.LA17_N_CC,fpga.E23,fmc1.LA17_P_CC,fpga.F22,fmc1.LA18_N_CC,fpga.AC16,fmc1.LA18_P_CC,fpga.AB16,fmc1.LA19_N,fpga.G26,fmc1.LA19_P,fpga.Y15,fmc1.LA20_N,fpga.AA20,fmc1.LA20_P,fpga.AA19,fmc1.LA21_N,fpga.AA15,fmc1.LA21_P,fpga.H21,fmc1.LA22_N,fpga.H24,fmc1.LA22_P,fpga.H23,fmc1.LA24_N,fpga.C24,fmc1.LA24_P,fpga.C23,fmc1.LA25_N,fpga.C26,fmc1.LA25_P,fpga.D26,fmc1.LA26_N,fpga.AF20,fmc1.LA26_P,fpga.AF19,fmc1.LA27_N,fpga.W14,fmc1.LA27_P,fpga.V14,fmc1.LA28_N,fpga.A24,fmc1.LA28_P,fpga.A23,fmc1.LA29_N,fpga.A25,fmc1.LA29_P,fpga.B24,fmc1.LA30_N,fpga.A22,fmc1.LA30_P,fpga.B22,fmc1.LA31_N,fpga.C22,fmc1.LA31_P,fpga.D21,fmc1.LA32_N,fpga.A20,fmc1.LA32_P,fpga.B20,fmc1.LA33_N,fpga.V19,fmc1.LA33_P,fpga.V18
//,fmc1.CLK0_M2C_N,mar.E17
//,fmc1.CLK0_M2C_P,mar.F17
//,fmc1.CLK1_M2C_N,mar.D18
//,fmc1.CLK1_M2C_P,mar.E18
//,fmc1.LA12_N,mar.F15
//,fmc1.LA12_P,mar.G15
//,fmc1.LA13_N,mar.D16
//,fmc1.LA13_P,mar.D15
//,fmc1.LA23_N,mar.F12
//,fmc1.LA23_P,mar.G12
//});

// fmc2 HPC
viapairs #(.WIDTH(160)) fmc2_via({fmc2.HA00_N_CC,fpga.AD9
,fmc2.HA00_P_CC,fpga.AC9
,fmc2.HA01_N_CC,fpga.AB9
,fmc2.HA01_P_CC,fpga.AA9
,fmc2.HA02_N,fpga.W9
,fmc2.HA02_P,fpga.W10
,fmc2.HA03_N,fpga.Y7
,fmc2.HA03_P,fpga.Y8
,fmc2.HA04_N,fpga.AF7
,fmc2.HA04_P,fpga.AE7
,fmc2.HA05_N,fpga.AA7
,fmc2.HA05_P,fpga.AA8
,fmc2.HA06_N,fpga.AE10
,fmc2.HA06_P,fpga.AD10
,fmc2.HA07_N,fpga.AC7
,fmc2.HA07_P,fpga.AB7
,fmc2.HA08_N,fpga.AD8
,fmc2.HA08_P,fpga.AC8
,fmc2.HA09_N,fpga.AF9
,fmc2.HA09_P,fpga.AF10
,fmc2.HA10_N,fpga.Y10
,fmc2.HA10_P,fpga.Y11
,fmc2.HA11_N,fpga.Y12
,fmc2.HA11_P,fpga.Y13
,fmc2.HA12_N,fpga.AE11
,fmc2.HA12_P,fpga.AD11
,fmc2.HA13_N,fpga.AF8
,fmc2.HA13_P,fpga.AE8
,fmc2.HA14_N,fpga.AD13
,fmc2.HA14_P,fpga.AC13
,fmc2.HA15_N,fpga.AA12
,fmc2.HA15_P,fpga.AA13
,fmc2.HA16_N,fpga.AC12
,fmc2.HA16_P,fpga.AB12
,fmc2.HA17_N_CC,fpga.AC11
,fmc2.HA17_P_CC,fpga.AB11
,fmc2.HA18_N,fpga.AB10
,fmc2.HA18_P,fpga.AA10
,fmc2.HA19_N,fpga.AF13
,fmc2.HA19_P,fpga.AE13
,fmc2.HA20_N,fpga.AF12
,fmc2.HA20_P,fpga.AE12
,fmc2.HA21_N,fpga.W11
,fmc2.HA21_P,fpga.V11
,fmc2.HA22_N,fpga.W8
,fmc2.HA22_P,fpga.V9
,fmc2.HA23_N,fpga.V7
,fmc2.HA23_P,fpga.V8
,fmc2.HB00_N_CC,fpga.AA22
,fmc2.HB00_P_CC,fpga.Y22
,fmc2.HB01_N,fpga.AE21
,fmc2.HB01_P,fpga.AD21
,fmc2.HB02_N,fpga.W21
,fmc2.HB02_P,fpga.V21
,fmc2.HB03_N,fpga.V22
,fmc2.HB03_P,fpga.U22
,fmc2.HB04_N,fpga.AF23
,fmc2.HB04_P,fpga.AE23
,fmc2.HB05_N,fpga.AF22
,fmc2.HB05_P,fpga.AE22
,fmc2.HB06_N_CC,fpga.AC24
,fmc2.HB06_P_CC,fpga.AC23
,fmc2.HB07_N,fpga.AC22
,fmc2.HB07_P,fpga.AB22
,fmc2.HB08_N,fpga.AC21
,fmc2.HB08_P,fpga.AB21
,fmc2.HB09_N,fpga.AD24
,fmc2.HB09_P,fpga.AD23
,fmc2.HB10_N,fpga.AF25
,fmc2.HB10_P,fpga.AF24
,fmc2.HB11_N,fpga.W24
,fmc2.HB11_P,fpga.W23
,fmc2.HB12_N,fpga.U25
,fmc2.HB12_P,fpga.U24
,fmc2.HB13_N,fpga.Y26
,fmc2.HB13_P,fpga.Y25
,fmc2.HB14_N,fpga.AE26
,fmc2.HB14_P,fpga.AD26
,fmc2.HB15_N,fpga.AE25
,fmc2.HB15_P,fpga.AD25
,fmc2.HB16_N,fpga.AC26
,fmc2.HB16_P,fpga.AB26
,fmc2.HB17_N_CC,fpga.AA24
,fmc2.HB17_P_CC,fpga.Y23
,fmc2.HB18_N,fpga.V26
,fmc2.HB18_P,fpga.U26
,fmc2.HB19_N,fpga.AB25
,fmc2.HB19_P,fpga.AA25
,fmc2.HB20_N,fpga.W26
,fmc2.HB20_P,fpga.W25
,fmc2.HB21_N,fpga.V24
,fmc2.HB21_P,fpga.V23
,fmc2.LA00_N_CC,fpga.AA2
,fmc2.LA00_P_CC,fpga.AA3
,fmc2.LA01_N_CC,fpga.AB4
,fmc2.LA01_P_CC,fpga.AA4
,fmc2.LA02_N,fpga.AE1
,fmc2.LA02_P,fpga.AD1
,fmc2.LA03_N,fpga.AF4
,fmc2.LA03_P,fpga.AF5
,fmc2.LA04_N,fpga.V1
,fmc2.LA04_P,fpga.V2
,fmc2.LA05_N,fpga.AC1
,fmc2.LA05_P,fpga.AB1
,fmc2.LA06_N,fpga.AC6
,fmc2.LA06_P,fpga.AB6
,fmc2.LA07_N,fpga.Y1
,fmc2.LA07_P,fpga.W1
,fmc2.LA08_N,fpga.Y2
,fmc2.LA08_P,fpga.Y3
,fmc2.LA09_N,fpga.AE5
,fmc2.LA09_P,fpga.AE6
,fmc2.LA10_N,fpga.AC3
,fmc2.LA10_P,fpga.AC4
,fmc2.LA11_N,fpga.W4
,fmc2.LA11_P,fpga.V4
,fmc2.LA12_N,fpga.AF2
,fmc2.LA12_P,fpga.AF3
,fmc2.LA13_N,fpga.W5
,fmc2.LA13_P,fpga.W6
,fmc2.LA14_N,fpga.U1
,fmc2.LA14_P,fpga.U2
,fmc2.LA15_N,fpga.AC2
,fmc2.LA15_P,fpga.AB2
,fmc2.LA16_N,fpga.Y5
,fmc2.LA16_P,fpga.Y6
,fmc2.LA17_N_CC,fpga.AC16
,fmc2.LA17_P_CC,fpga.AB16
,fmc2.LA18_N_CC,fpga.AD18
,fmc2.LA18_P_CC,fpga.AC18
,fmc2.LA19_N,fpga.AF15
,fmc2.LA19_P,fpga.AF14
,fmc2.LA20_N,fpga.Y18
,fmc2.LA20_P,fpga.Y17
,fmc2.LA21_N,fpga.AD14
,fmc2.LA21_P,fpga.AC14
,fmc2.LA22_N,fpga.AF20
,fmc2.LA22_P,fpga.AF19
,fmc2.LA23_N,fpga.AE15
,fmc2.LA23_P,fpga.AD15
,fmc2.LA24_N,fpga.AB20
,fmc2.LA24_P,fpga.AB19
,fmc2.LA25_N,fpga.AE20
,fmc2.LA25_P,fpga.AD20
,fmc2.LA26_N,fpga.AF17
,fmc2.LA26_P,fpga.AE17
,fmc2.LA27_N,fpga.AA15
,fmc2.LA27_P,fpga.AA14
,fmc2.LA28_N,fpga.Y16
,fmc2.LA28_P,fpga.Y15
,fmc2.LA29_N,fpga.AA20
,fmc2.LA29_P,fpga.AA19
,fmc2.LA30_N,fpga.W16
,fmc2.LA30_P,fpga.W15
,fmc2.LA31_N,fpga.W14
,fmc2.LA31_P,fpga.V14
,fmc2.LA32_N,fpga.V17
,fmc2.LA32_P,fpga.V16
,fmc2.LA33_N,fpga.V19
,fmc2.LA33_P,fpga.V18
});
//viapairs #(.WIDTH(62)) fmc2_via({fmc2.LA00_N_CC,fpga.AA2,fmc2.LA00_P_CC,fpga.AA3,fmc2.LA02_N,fpga.AE1,fmc2.LA02_P,fpga.AD1,fmc2.LA03_N,fpga.AF4,fmc2.LA03_P,fpga.AF5,fmc2.LA04_N,fpga.V1,fmc2.LA04_P,fpga.V2,fmc2.LA05_N,fpga.AC1,fmc2.LA05_P,fpga.AB1,fmc2.LA06_N,fpga.AC6,fmc2.LA06_P,fpga.AB6,fmc2.LA07_N,fpga.Y1,fmc2.LA07_P,fpga.W1,fmc2.LA08_N,fpga.Y2,fmc2.LA08_P,fpga.Y3,fmc2.LA09_N,fpga.AE5,fmc2.LA09_P,fpga.AE6,fmc2.LA10_N,fpga.AC3,fmc2.LA10_P,fpga.AC4,fmc2.LA11_N,fpga.W4,fmc2.LA11_P,fpga.V4,fmc2.LA12_N,fpga.AF2,fmc2.LA12_P,fpga.AF3,fmc2.LA13_N,fpga.W5,fmc2.LA13_P,fpga.W6,fmc2.LA14_N,fpga.U1,fmc2.LA14_P,fpga.U2,fmc2.LA15_N,fpga.AC2,fmc2.LA15_P,fpga.AB2,fmc2.LA16_N,fpga.Y5,fmc2.LA16_P,fpga.Y6,fmc2.LA17_N_CC,fpga.AC16,fmc2.LA17_P_CC,fpga.AB16,fmc2.LA18_N_CC,fpga.AD18,fmc2.LA18_P_CC,fpga.AC18,fmc2.LA19_N,fpga.AF15,fmc2.LA19_P,fpga.AF14,fmc2.LA20_N,fpga.Y18,fmc2.LA20_P,fpga.Y17,fmc2.LA21_N,fpga.AD14,fmc2.LA21_P,fpga.AC14,fmc2.LA22_N,fpga.AF20,fmc2.LA22_P,fpga.AF19,fmc2.LA23_N,fpga.AE15,fmc2.LA23_P,fpga.AD15,fmc2.LA24_N,fpga.AB20,fmc2.LA24_P,fpga.AB19,fmc2.LA25_N,fpga.AE20,fmc2.LA25_P,fpga.AD20,fmc2.LA26_N,fpga.AF17,fmc2.LA26_P,fpga.AE17,fmc2.LA27_N,fpga.AC14,fmc2.LA27_P,fpga.AA14,fmc2.LA28_N,fpga.Y16,fmc2.LA28_P,fpga.Y15,fmc2.LA29_N,fpga.AA20,fmc2.LA29_P,fpga.AA19,fmc2.LA31_N,fpga.W14,fmc2.LA31_P,fpga.V14,fmc2.LA33_N,fpga.V19,fmc2.LA33_P,fpga.V18});

wire [7:0] mgttx_p;
wire [7:0] mgtrx_p;
wire [7:0] mgttx_n;
wire [7:0] mgtrx_n;
assign fpga.B2=qsfp1.tx_p[0];
assign fpga.B1=qsfp1.tx_n[0];
assign qsfp1.rx_p[0]=fpga.C4;
assign qsfp1.rx_n[0]=fpga.C3;
assign fpga.A4=qsfp1.tx_p[1];
assign fpga.A3=qsfp1.tx_n[1];
assign qsfp1.rx_p[1]=fpga.B6;
assign qsfp1.rx_n[1]=fpga.B5;
assign fpga.D2=qsfp1.tx_p[2];
assign fpga.D1=qsfp1.tx_n[2];
assign qsfp1.rx_p[2]=fpga.E4;
assign qsfp1.rx_n[2]=fpga.E3;
assign fpga.F2=qsfp1.tx_p[3];
assign fpga.F1=qsfp1.tx_n[3];
assign qsfp1.rx_p[3]=fpga.G4;
assign qsfp1.rx_n[3]=fpga.G3;

assign fpga.K2=qsfp2.tx_p[0];
assign fpga.K1=qsfp2.tx_n[0];
assign qsfp2.rx_p[0]=fpga.L4;
assign qsfp2.rx_n[0]=fpga.L3;
assign fpga.H2=qsfp2.tx_p[1];
assign fpga.H1=qsfp2.tx_n[1];
assign qsfp2.rx_p[1]=fpga.J4;
assign qsfp2.rx_n[1]=fpga.J3;
assign fpga.M2=qsfp2.tx_p[2];
assign fpga.M1=qsfp2.tx_n[2];
assign qsfp2.rx_p[2]=fpga.N4;
assign qsfp2.rx_n[2]=fpga.N3;
assign fpga.P2=qsfp2.tx_p[3];
assign fpga.P1=qsfp2.tx_n[3];
assign qsfp2.rx_p[3]=fpga.R4;
assign qsfp2.rx_n[3]=fpga.R3;
// leave them in the qf2_core
wire ceb=1'b0;
IBUFDS_GTE2 ibufgds_mgt115refclk0(.I(fpga.H6),.IB(fpga.H5),.O(hw.U5_clk),.CEB(ceb),.ODIV2());   // SI57x_A
IBUFDS_GTE2 ibufgds_mgt115refclk1(.I(fpga.K6),.IB(fpga.K5),.O(hw.U19_clk),.CEB(ceb),.ODIV2());  // SI57x_B
IBUFDS_GTE2 ibufgds_mgt116refclk0(.I(fpga.D6),.IB(fpga.D5),.O(hw.Y4_clk),.CEB(ceb),.ODIV2());   // SIT9121
IBUFDS_GTE2 ibufgds_mgt116refclk1(.I(fpga.F6),.IB(fpga.F5),.O(hw.ext_clk),.CEB(ceb),.ODIV2());  // ext_clk
//IBUFGDS ibufgds_sysclk(.I(fpga.G11),.IB(fpga.F10),.O(hw.sys_clk));
//IBUFDS ibufds_kintex_data_in(.I(fpga.J13),.IB(fpga.H13),.O(hw.kintex_data_in));
//OBUFDS ibufds_kintex_data_out(.O(fpga.J11),.OB(fpga.J10),.I(hw.kintex_data_out));
assign hw.kintex_data_in_p=fpga.J13;
assign hw.kintex_data_in_n=fpga.H13;
assign fpga.J11=hw.kintex_data_out_p;
assign fpga.J10=hw.kintex_data_out_n;
assign fpga.J8=hw.SIT9122_OE;
//assign fpga.J10=hw.kintex_data_out_n;
//assign fpga.J11=hw.kintex_data_out_p;
assign fpga.M16=hw.D4;
assign fpga.K15=hw.D5;
//assign {hw.U19_p,hw.U19_n}={fpga.K6,fpga.K5};
//assign {hw.U5_p,hw.U5_n}={fpga.H6,fpga.H5};
//assign {hw.Y4_p,hw.Y4_n}={fpga.D6,fgpa.D5}
assign fpga.F14=hw.k7_done_or_rxlocked;
//assign hw.kintex_data_in_p=fpga.J13;
//assign hw.kintex_data_in_n=fpga.H13;
assign hw.sys_clk_p=fpga.G11;
assign hw.sys_clk_n=fpga.F10;

endmodule




module qf2v07_sim#(parameter SIMULATION="")(fpga fpga
,ifqf2v07.sim hw
,iffmc.mastersig fmc1
,iffmc.mastersig fmc2
,ifqsfp qsfp1
,ifqsfp qsfp2
);
assign fpga.G11=hw.sys_clk_p;
assign fpga.F10=hw.sys_clk_n;
endmodule
