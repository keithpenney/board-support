interface ifqf2v07(
	);
	wire D4;
	wire D5;
	//	wire U19_p,U19_n;
	//	wire U5_p,U5_n;
	//	wire Y4_p,Y4_n;
	wire U5_clk;
	wire U19_clk;
	wire Y4_clk;
	wire ext_clk;
	wire k7_done_or_rxlocked;
	wire kintex_data_in_p;
	wire kintex_data_in_n;
	wire kintex_data_out_p;
	wire kintex_data_out_n;
	wire sys_clk;
	wire sys_clk_p;
	wire sys_clk_n;
	logic SIT9122_OE=1'b1;

	wire bmb7_clk,bmb7_clk_4x;
	wire [7:0] port_50006_word_k7tos6;
	wire [7:0] port_50006_word_s6tok7;
	wire port_50006_tx_available,port_50006_tx_complete;
	wire port_50006_rx_available,port_50006_rx_complete;
	wire port_50006_word_read;

	modport hw(input D4,D5,k7_done_or_rxlocked,SIT9122_OE
	,output U5_clk,U19_clk,Y4_clk,ext_clk
	,output sys_clk_p,sys_clk_n
	,output kintex_data_in_p,kintex_data_in_n
	,input kintex_data_out_p,kintex_data_out_n
	//U5_p,U5_n,Y4_p,Y4_n,U19_p,U19_n
	);
	modport sim(output D4,D5,k7_done_or_rxlocked,SIT9122_OE
	,input U5_clk,U19_clk,Y4_clk,ext_clk
	,input sys_clk_p,sys_clk_n
	,input kintex_data_in_p,kintex_data_in_n
	,output kintex_data_out_p,kintex_data_out_n
	//U5_p,U5_n,Y4_p,Y4_n,U19_p,U19_n
	);
	modport cfg(output D4,D5,k7_done_or_rxlocked,SIT9122_OE
	,input U5_clk,U19_clk,Y4_clk,ext_clk
	,input sys_clk_p,sys_clk_n
	,output sys_clk
	,input kintex_data_in_p,kintex_data_in_n
	,output kintex_data_out_p,kintex_data_out_n
	,output bmb7_clk,bmb7_clk_4x
	,output port_50006_word_s6tok7,port_50006_rx_available,port_50006_rx_complete,port_50006_word_read
	,input port_50006_word_k7tos6,port_50006_tx_available,port_50006_tx_complete

	//U5_p,U5_n,Y4_p,Y4_n,U19_p,U19_n
	);
	modport driver(input port_50006_word_s6tok7,port_50006_rx_available,port_50006_rx_complete,port_50006_word_read
	,input sys_clk
	,input bmb7_clk,bmb7_clk_4x,Y4_clk,U5_clk
	,output port_50006_word_k7tos6,port_50006_tx_available,port_50006_tx_complete
	);
	//modport sim(
	//);
endinterface
