// FMC test on Avnet Spartan 6 LX150T through GMII
`define ETH_IP {8'd131,8'd243,8'd168,8'd81} //lrd2.lbl.gov
`define ETH_MAC 48'h00150ad152b4
`define READ_PIP_LEN 12
`define N_LED 8
`define 100MHZ_CLK_SINGLE

`include "ether_fmc_mc.vh"
