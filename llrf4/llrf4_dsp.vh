(
	input DSPCLK,
	inout [15:0] FD,
	input [2:0] CTL,
	output [1:0] RDY,
	input IFCLK,
	input CLKOUT,
	output INIT_B,
	input [13:0] IN0D, input IN0OVR, output IN0PD,
	input [13:0] IN1D, input IN1OVR, output IN1PD,
	input [13:0] IN2D, input IN2OVR, output IN2PD,
	input [13:0] IN3D, input IN3OVR, output IN3PD,
	output [13:0] ID,
	output [13:0] QD,
	output IF_SLEEP,
	inout DALLAS,
	inout TRIG1,
	inout TRIG2,
	output DAC_CLK,
	output [3:1] DAC_DIN,
	output DAC_CS,
	inout [6:1] GEEK_DIO,
// SNS-compatible interlock I/O, generally not even populated
	input N_GOT_OK,
	output N_SEND_OK,
	output ADC_CLK,
	output ADC_CS,
	input ADC_SDO,
	output ADC_SDI,
	output DIV_CSB,
	output SLED,
	output XERR,
	output SYNC,
	output [5:4] LOCAL_LED,
	inout [4:1] GEEK_LED,
	output GEEK_PIN,
	inout L0N, inout L0P,
	inout L1N, inout L1P,
	inout L2N, inout L2P,
	inout L3N, inout L3P,
	inout L4N, inout L4P,
	inout L5N, inout L5P,
	inout L6N, inout L6P,
	inout L7N, inout L7P
);
parameter circle_aw=10;


// BUFG usbclk_buffer(usbclk,IFCLK);
wire usbclk=IFCLK;  // Xilinx is smart enough to infer the BUFG
wire dspclk=DSPCLK;

reg [23:0] led_counter=0;
always @(posedge usbclk)
	led_counter<= led_counter+1;

// Not used
/*assign L0N=0; assign L0P=1;
assign L1N=0; assign L1P=1;
assign L2N=0; assign L2P=1;
assign L3N=0; assign L3P=1;
assign L4N=0; assign L4P=1;
assign L5N=0; assign L5P=1;
assign L6N=0; assign L6P=1;
assign L7N=0; assign L7P=1;
*/
// abstract control interface in USB clock domain
// (implementation hooks into JTAG subsystem)
wire [39:0] control_bus;
wire control_strobe;
control_if control_interface(
	.usbclk(usbclk),
	.interrupt(1'b0),
	.control_bus(control_bus),
	.control_strobe(control_strobe)
);
wire [6:0]  control_addr = control_bus[38:32];
wire [31:0] control_data = control_bus[31:0];


// Keep the regmap.vh macros happy
wire            clk = usbclk;
wire [6:0]  lb_addr = control_addr;
wire [31:0] lb_data = control_data;
wire       lb_write = control_strobe;


// Host-settable registers (usb clock domain)
`include "regmap_support.vh"
`REG_reset_rx // u 4[3:3] -
`REG_reset_tx // u 4[2:2] -
`REG_enable_rx // u 4[1:1] -
`REG_enable_tx // u 4[0:0] -
`REG_src_sel   // u 68[10:8] -
`REG_dbg_sel   // u 68[13:12] -
`REG_tpat_speed // u 67[15:0] -
`REG_en_adc0  // u 80[0:0] 1
`REG_en_adc1  // u 80[1:1] 1
`REG_en_adc2  // u 80[2:2] 1
`REG_en_adc3  // u 80[3:3] 1
`REG_en_dac  // u 80[4:4] 1
`REG_en_dallas  // u 80[5:5] 1
`REG_en_pin  // u 80[7:7] 0
`REG_max1820_sync_enable // u 80[8:8] 0
`REG_max1820_mux_ctl // u 80[9:9] 0
`REG_max1820_div_ctl    // u 80[14:12] 0
`STB_stb_ad95xx  // a 5 -
// XXX should be in dspclk domain

wire [15:0] stream_addr;  // driven by packet_build

// GEEK_DIO[6] and GEEK_DIO[5] used for SPI port of AD95XX

// Instantiate DS1822 driver
wire [7:0] dallas_out;
ds1822_driver
 ds1822(.clk(usbclk), .rst(1'b0), .run(en_dallas),
	.DS1(DALLAS),
	.address(stream_addr[8:4]), .result(dallas_out)
);

wire lo_led;
    // Instantiate AD95xx driver
    ad95xx_driver ad9512_driver(.clk(usbclk),
	    .send_data(lb_data[23:0]),
	    .write_strobe(stb_ad95xx),
	    .chip_sclk(GEEK_DIO[6]), .chip_sdio(GEEK_DIO[5]), .chip_csb(DIV_CSB)
    );

    // Instantiate MAX1820Y sync generator
    max1820 max1820(.dspclk(dspclk), .usbclk(usbclk), .sync(SYNC),
	    .sync_enable(max1820_sync_enable), .mux_ctl(max1820_mux_ctl),
	    .div_ctl(max1820_div_ctl)
    );

    // Instantiate MCP3208 driver (wrapper)
        wire [7:0] mcp3208_out;
        mcp3208_wrap mcp3208(.clk(usbclk),
	        .lb_data(lb_data), .lb_addr(lb_addr), .lb_write(lb_write),
            .lo_led(lo_led),
	        .address(stream_addr[7:4]), .result(mcp3208_out),
            .adc_cs(ADC_CS), .adc_clk(ADC_CLK), .adc_dout(ADC_SDO), .adc_din(ADC_SDI)
        );

// Test pattern generator (tests USB correctness and speed)
//  also used as pacing for production data flow
reg [15:0] tpat_data=0;
reg [15:0] dds_acc=0;
reg carry=0;
always @(posedge usbclk) begin
	{carry, dds_acc} <= {1'b0,dds_acc} + tpat_speed;
	if (carry) tpat_data <= tpat_data+1;
end
wire tpat_strobe = carry;

// Instantiate frequency counter
wire [27:0] frequency;
wire [15:0] diff_stream;
wire diff_stream_strobe;
wire glitch_catcher;
freq_count freq_count(.clk(dspclk), .usbclk(usbclk),
	.frequency(frequency),
	.diff_stream(diff_stream), .diff_stream_strobe(diff_stream_strobe),
	.glitch_catcher(glitch_catcher)
);


// Create IOB latches for high speed ADC inputs; also convert to signed
reg signed [15:0] adc1=0, adc2=0, adc3=0, adc4=0;
always @(posedge dspclk) begin
	adc1 <= {~IN0D[13],IN0D[12:0],2'b0};
	adc2 <= {~IN1D[13],IN1D[12:0],2'b0};
	adc3 <= {~IN2D[13],IN2D[12:0],2'b0};
	adc4 <= {~IN3D[13],IN3D[12:0],2'b0};
end

// Instantiate main application (LLRF) data path
wire [15:0] dsp_stream_data;  // primary data output stream
wire dsp_stream_strobe;  // driven by packet_build
wire trig_row;    // driven by dsp_layer1, signals OK to start new row
wire rf_on;
wire slow_op, slow_snap; // driven by packet_build in USB domain
wire [7:0] slow_data;    // driven by dsp_layer1
wire [15:0] app_config;

wire sb_sdac_ref_sclk,sb_sdac_trig;
wire sb_switch_op;
wire [2:0] sb_photo_sw;
wire [15:0] sb_sdac_out1,sb_sdac_out2,sb_sdac_out3,sb_sdac_out4;
wire [13:0] sb_v10,sb_v11,sb_v12,sb_v13,sb_v14,sb_v15,sb_v16,sb_v17;
wire [23:0] sb_v20,sb_v21,sb_v22,sb_v23;
dsp_layer1 #(.circle_aw(circle_aw)) dsp_layer1(
	.clk(dspclk),
	.tx_clk(usbclk),
	.adc1(adc1),
	.adc2(adc2),
	.adc3(adc3),
	.adc4(adc4),
	.dac1(ID),
	.dac2(QD),

	.user_io(GEEK_DIO[4:1]),

	.trig_in(TRIG2),
	.trig_out(TRIG1),
	.heartbeat(1'b0),//led_counter[23]),
	.lo_led(lo_led),
	.GEEK_LED(GEEK_LED),
	.control_addr(control_addr),
	.control_strobe(control_strobe),
	.control_data(control_data),
	.DAC_CLK(DAC_CLK),
	.DAC_DIN(DAC_DIN),
	.DAC_CS(DAC_CS),
	.N_GOT_OK(N_GOT_OK),
	.N_SEND_OK(N_SEND_OK),
	.strobe_enable(trig_row),
	.stream_addr(stream_addr[13:0]),
	.stream_gate(dsp_stream_strobe),
	.dsp_data(dsp_stream_data),
	.app_config(app_config),
	.slow_op(slow_op),
	.slow_snap(slow_snap),
	.slow_out(slow_data)
    ,
	.L0N(L0N),.L0P(L0P),
	.L1N(L1N),.L1P(L1P),
	.L2N(L2N),.L2P(L2P),
	.L3N(L3N),.L3P(L3P),
	.L4N(L4N),.L4P(L4P),
	.L5N(L5N),.L5P(L5P),
	.L6N(L6N),.L6P(L6P),
	.L7N(L7N),.L7P(L7P),
    .sb_switch_op(sb_switch_op),.sb_photo_sw(sb_photo_sw),
    .sb_sdac_ref_sclk(sb_sdac_ref_sclk),.sb_sdac_trig(sb_sdac_trig),.sb_sdac_out1(sb_sdac_out1),.sb_sdac_out2(sb_sdac_out2),.sb_sdac_out3(sb_sdac_out3),.sb_sdac_out4(sb_sdac_out4),
    .sb_v10(sb_v10),.sb_v11(sb_v11),.sb_v12(sb_v12),.sb_v13(sb_v13),.sb_v14(sb_v14),.sb_v15(sb_v15),.sb_v16(sb_v16),.sb_v17(sb_v17),.sb_v20(sb_v20),.sb_v21(sb_v21),.sb_v22(sb_v22),.sb_v23(sb_v23)

);

// Packet building
//   Various inputs merged to single production data stream
wire [15:0] prod_stream;
wire prod_stream_strobe;
packet_build packet(
	.clk(usbclk),
	.frequency(frequency),
	.lb_addr(control_addr),
	.lb_write(control_strobe),
	.lb_data(control_data),
	.pace(trig_row&tpat_strobe),  // look at me!
	.stream_addr(stream_addr),
	.dallas_data(dallas_out),
	.mcp3208_data(mcp3208_out),
	.stream_gate(dsp_stream_strobe),
	.dsp_data(dsp_stream_data),
	.slow_op(slow_op),
	.slow_snap(slow_snap),
	.slow_data(slow_data),
	.app_config(app_config),
	.prod_stream(prod_stream),
	.stream_strobe(prod_stream_strobe)
);

//`REG_switch_op // u 1 0
//`REG_photo_sw  // u 3 0
wire [1:0] dummy2b;
sideboard sideboard(.clk(clk),.sconfig(9'h28),
//	.dac_sb_sclk(L3P),.dac_sb_sdio({L3N,dummy2b}),.dac_sb_csb(L2N),
    .SCK(L6N), .SDI(L5N),
    .CS_3548(L5P),  .CS_7794(L6P),  .CS_BIAS(L4N),
    .SDO_3548(L1P), .SDO_7794(L1N), .SDO_BIAS(L2P),
    .sdac_ref_sclk(sb_sdac_ref_sclk),.sdac_trig(sb_sdac_trig),.out1(sb_sdac_out1),.out2(sb_sdac_out2),.out3(sb_sdac_out3),.out4(sb_sdac_out4),
    .switch_op(sb_switch_op),.photo_sw(sb_photo_sw),
    .v10(sb_v10),.v11(sb_v11),.v12(sb_v12),.v13(sb_v13),.v14(sb_v14),.v15(sb_v15),.v16(sb_v16),.v17(sb_v17),.v20(sb_v20),.v21(sb_v21),.v22(sb_v22),.v23(sb_v23));


wire [15:0] sb_stream_usb;  wire sb_strobe_usb;     // sport debugging
/*`REG_switch_op // u 1 0
`REG_photo_sw  // u 3 0
wire [5:0] switch_sel = {~photo_sw[2],photo_sw[2],~photo_sw[1],photo_sw[1],~photo_sw[0],photo_sw[0]};
wire [8:0] sconfig = 9'h28;
sporta sport(.clk(usbclk),  // timing is different from actual use case, but
	// usbclk is needed to directly connect to the src multiplexer below
	.SCK(L6N), .SDI(L5N),
	.CS_3548(L5P),  .CS_7794(L6P),  .CS_BIAS(L4N),
	.SDO_3548(L1P), .SDO_7794(L1N), .SDO_BIAS(L2P),
	.sconfig(sconfig), .switch_sel(switch_sel), .switch_op(switch_op),
	.stream(sb_stream_usb), .stream_tick(sb_strobe_usb)
);
*/
// Stream sources not implemented here
wire [15:0] jtag_dbg1=0;      wire jtag_dbg_strobe=0;   // JTAG debugging
wire [15:0] aux_stream=0;     wire aux_stream_strobe=0; // USB clock only

// Host-selectable multiplex of possible data streams to USB
// Streams other than prod_stream are for debug and checkout
reg [15:0] src=0;
reg ssb;
always @(posedge usbclk) case (src_sel)
	3'b000: begin src <= tpat_data;    ssb <= tpat_strobe;         end
	3'b001: begin src <= diff_stream;  ssb <= diff_stream_strobe;  end
	3'b010: begin src <= prod_stream;  ssb <= prod_stream_strobe;  end
	3'b011: begin src <= aux_stream;   ssb <= aux_stream_strobe;   end
	3'b100: begin src <= jtag_dbg1;    ssb <= jtag_dbg_strobe;     end
	3'b101: begin src <= sb_stream_usb;ssb <= sb_strobe_usb;       end
	3'b110: begin src <= 0;            ssb <= 0;                   end
	3'b111: begin src <= 0;            ssb <= 0;                   end
endcase

// 16-bit parallel USB interface to CY7C68013A
// Only output (FPGA->FX2) capability is used,
// input (register setting) comes via JTAG bit-banged by the FX2
// reg rdy1=0;
// always @(posedge usbclk) rdy1 <= CTL[0];
wire [15:0] usb_data;
assign FD = CTL[2] ? usb_data : 16'bzzzzzzzzzzzzzzzz;
wire usb_rdy1;
assign RDY = {usb_rdy1, 1'b0};
wire [15:0] rx_debug;
wire rx_overrun;   // XXX hook me up
wire clr_status=0;    // XXX hook me up
rx_buffer rx_buffer(.rst(~reset_rx),
	.usbclk(usbclk), .usbdata(usb_data),
	.RD(CTL[1]), .have_pkt_rdy(usb_rdy1),
	.rx_overrun(rx_overrun), .clear_status(clr_status),
	.rxclk(usbclk), .rxstrobe(ssb), .rxdata(src),
	.debugbus(rx_debug)
);

// Power control for ADCs, DACs, and possible external PIN diode
assign IN0PD = ~en_adc0;
assign IN1PD = ~en_adc1;
assign IN2PD = ~en_adc2;
assign IN3PD = ~en_adc3;
assign IF_SLEEP = ~en_dac;
assign GEEK_PIN = ~en_pin;

// Simple LED heartbeat
// Feel free to change it to something more complex.
// This pattern keeps power dissipation constant,
// a good thing during board checkout.
wire led0 =  led_counter[23];
wire led1 = ~led_counter[23];
assign LOCAL_LED={led1,led0};


// Unused, just like LCLS
assign INIT_B=0;
assign SLED=0;
assign XERR=0;


endmodule
