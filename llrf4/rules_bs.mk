#Register map code generation
DSP_LAYER1_BASE_V := $(BS_LLRF4_DIR)/dsp_layer1.v $(PERIPHERAL_DRIVERS_DIR)/ad56x4_driver4.v $(COMMON_HDL_DIR)/decay_buf.v $(COMMON_HDL_DIR)/circle_buf.v $(COMMON_HDL_DIR)/dpram.v $(COMMON_HDL_DIR)/minmax.v $(COMMON_HDL_DIR)/data_xdomain.v $(COMMON_HDL_DIR)/flag_xdomain.v

BSP1_V := $(BS_LLRF4_DIR)/packet_build.v $(PERIPHERAL_DRIVERS_DIR)/ds1822_driver.v $(COMMON_HDL_DIR)/freq_count.v $(PERIPHERAL_DRIVERS_DIR)/sporta.v

FIFO_V = $(COMMON_HDL_DIR)/rx_buffer.v $(COMMON_HDL_DIR)/fifo2.v $(COMMON_HDL_DIR)/dpram.v

BSP_V := $(BSP1_V) $(PERIPHERAL_DRIVERS_DIR)/ad95xx_driver.v $(PERIPHERAL_DRIVERS_DIR)/max1820.v $(FIFO_V) $(BS_LLRF4_DIR)/mcp3208_wrap.v $(PERIPHERAL_DRIVERS_DIR)/mcp3208_rcvr.v $(COMMON_HDL_DIR)/dpram.v $(COMMON_HDL_DIR)/data_xdomain.v

