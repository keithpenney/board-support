`timescale 1ns / 1ns
`include "constants.vams"

module data_test_tb;

integer den;
real fden, fnum, ampi, phsi;
reg ch0s, ch1s, ch2s, ch3s;  // choose ADC channels to excite with test pattern
integer cyc, defcyc; reg sweep;   // other test modes
initial begin
`ifdef DSP_FLAVOR_SEL
	fnum=11.005;
	den=47;
	defcyc=500;
`else
	fnum=2.005;
	den=14;
	defcyc=4480;
`endif
	fden=den;
	// Test not designed to work with phsi near pi
	// 4 <= den <= 128, no factor of fnum
	// ampi should be slightly less than 32K to avoid clipping
	// Expected output is ampi*79590*1.64676/2^17*14^2/16^2
	if (!$value$plusargs("amp=%f", ampi)) ampi=10000.0;
	if (!$value$plusargs("phs=%f", phsi)) phsi=0.0;
	//if (!$value$plusargs("den=%d", den )) den=16;
	if (!$value$plusargs("ch0=%d", ch0s)) ch0s=1;
	if (!$value$plusargs("ch1=%d", ch1s)) ch1s=0;
	if (!$value$plusargs("ch2=%d", ch2s)) ch2s=0;
	if (!$value$plusargs("ch3=%d", ch3s)) ch3s=0;
	if (!$value$plusargs("cyc=%d", cyc)) cyc=defcyc;
	if (!$value$plusargs("sweep=%d", sweep)) sweep=0;
end

reg dspclk;
integer cc, errors;
initial begin
	if ($test$plusargs("vcd")) begin
		$dumpfile(`DATA_TEST_VCD);
		$dumpvars(7,data_test_tb);
	end
	errors=0;
	for (cc=0; cc<cyc*den; cc=cc+1) begin
		dspclk=0; #5;
		dspclk=1; #5;
	end
	//$display("%s",errors==0?"PASS":"FAIL");
	$finish();
end

reg signed [15:0] adc=0;
integer noise;
integer nseed=1234;

real th0;
integer ax;  // can be huge in the face of clipping.  Don't be stupid and
	// set amplitude larger than 2^31 in ADC sine wave below.
	// 100 X overdrive is plenty for this purpose.
always @(posedge dspclk) begin
	noise = $dist_normal(nseed,0,1024);
	th0 = (cc-1)*`M_TWO_PI*fnum/fden - phsi;
	if (sweep) ampi = 500+cc;
	ax = $floor(ampi*$cos(th0)+0.5+noise/1024.0);
	if (ax >  32767) ax =  32767;
	if (ax < -32678) ax = -32768;
	adc <= ax;
	// $display("%d adc", adc);
	// Force restart of counters after configuration
	// Stupid trick to keep simulations short
	if (cc==220) begin
		#1; // win the race
`ifdef DSP_FLAVOR_APEX
		dut.dsp_layer1.dsp_inst.timing.rep_cnt=2;
`endif
`ifdef DSP_FLAVOR_SEL
		dut.dsp_layer1.dsp_inst.wave_cnt=2;
`endif
	end
end

reg usbclk=0;
always begin
	#11;
	usbclk=~usbclk;
end

// Local bus emulation
integer file1;
reg [319:0] file1_name;
initial begin
	if (!$value$plusargs("reg_default_sim_file=%s", file1_name)) file1_name="reg_top_default_sim";
	file1 = $fopen(file1_name,"r");
end

integer rc=2;
reg [31:0] control_data, cd;
reg [6:0] control_addr, ca;
reg control_strobe;
integer control_cnt=0;
always @(posedge usbclk) begin
	control_cnt <= control_cnt+1;
	if (control_cnt > 5 && control_cnt%3==1 && rc==2) begin
		rc=$fscanf(file1,"%d %d\n",ca,cd);
		if (rc==2) begin
			$display("local bus[%d] = 0x%x (%d)", ca, cd, cd);
			control_data <= cd;
			control_addr <= ca;
			control_strobe <= 1;
		end
	end else begin
		control_data <= 32'hx;
		control_addr <= 7'hx;
		control_strobe <= 0;
	end
end

// Like the hardware, data_test takes ADC data in offset binary
wire [13:0] adc0 = ch0s ? {~adc[15],adc[14:2]} : 14'h2010;
wire [13:0] adc1 = ch1s ? {~adc[15],adc[14:2]} : 14'h2020;
wire [13:0] adc2 = ch2s ? {~adc[15],adc[14:2]} : 14'h2030;
wire [13:0] adc3 = ch3s ? {~adc[15],adc[14:2]} : 14'h2040;

wire [15:0] prod_stream;
wire prod_stream_strobe;
tri1 onewire;  // pull-up
wire user_io_0 = 1'b0;    //ignore
wire user_io_1 = 1'b1;    //drive rf_permit2
wire user_io_2 = 1'b1;    //drive rf_permit1
wire user_io_3 = 1'b0;    //ignore

wire [3:0] user_io = {user_io_3,user_io_2,user_io_1,user_io_0};
wire [6:1] geek_dio = {2'b0, user_io};

data_test dut(.dspclk(dspclk), .usbclk(usbclk),
	.IN0D(adc0), .IN1D(adc1), .IN2D(adc2), .IN3D(adc3),
	.IN0OVR(1'b0), .IN1OVR(1'b0), .IN2OVR(1'b0), .IN3OVR(1'b0),
	.DALLAS(onewire), .TRIG1(), .TRIG2(), .GEEK_DIO(geek_dio),
	.N_GOT_OK(1'b1),
	.control_data(control_data), .control_addr(control_addr), .control_strobe(control_strobe),
	.prod_stream(prod_stream), .prod_stream_strobe(prod_stream_strobe)
);

// Output file is shorter than, but otherwise compatible with,
// output from test_usrp_standard_rx running on the hardware.
integer file2;
initial file2 = $fopen("ostream.dat","w");
integer prc;
always @(negedge usbclk) if (prod_stream_strobe) begin
	prc=$fputc(prod_stream[7:0],file2);
	prc=$fputc(prod_stream[15:8],file2);
end

// Pre-fill memories
integer jx;
initial begin
	for (jx=0; jx<8192; jx=jx+1) dut.dsp_layer1.circle_buf.mem.mem[jx]=jx*jx+1;
	for (jx=0; jx<128; jx=jx+1) dut.dsp_layer1.decay_buf.mem.mem[jx]=jx*jx+2;
end

endmodule
