`timescale 1ns / 1ns

// Consider adding instantiation of dallas and mcp3208 modules.
module packet_build (
	input clk,

	input [27:0] frequency,   // from freq_count module
	// Local bus -- we have the shadow memory
	input [6:0] lb_addr,
	input lb_write,
	input [31:0] lb_data,

	// Connection to DSP
	input pace,
	output [15:0] stream_addr,
	output stream_gate,  // stream_gate leads stream_strobe by one cycle
	input [15:0] dsp_data,

	// Slow data from DSP
	output slow_op,
	output slow_snap,
	input [7:0] slow_data,

	// SEL uses this for ch_keep
	input [15:0] app_config,

	// Dallas and mcp3208 input also based on stream_addr
	input [7:0] dallas_data,
	input [7:0] mcp3208_data,

	// Connection to rx_buffer (via src_sel mux)
	output [15:0] prod_stream,
	output stream_strobe
);

`include "regmap_support.vh"
`REG_wave_sel_1  // u 32[3:0] 0
`REG_wave_sel_2  // u 32[7:4] 1
`REG_wave_sel_3  // u 32[11:8] 1
`REG_wave_sel_4  // u 32[15:12] 1
`REG_wave_sel_5  // u 32[19:16] 1
`REG_wave_sel_6  // u 32[23:20] 1
`REG_wave_sel_7  // u 32[27:24] 1
`REG_wave_sel_8  // u 32[31:28] 1
`REG_wave_sel_9  // u 33[3:0] 1
`REG_wave_sel_10  // u 33[7:4] 1
`REG_wave_sel_11  // u 33[11:8] 1
`REG_wave_sel_12  // u 33[15:12] 1

// Shadow memory
reg [31:0] ctrl_if_buf [0:127];
always @(posedge clk) begin
	if(lb_write) begin
		ctrl_if_buf[lb_addr]<=lb_data;
	end
end

wire [7:0] config_data_romx;
wire [7:0] config_data_predictable=8'h33;
wire [4:0] config_addr = {stream_addr[14:12], stream_addr[5:4]};
config_romx rom(.address(config_addr), .data(config_data_romx));

reg predictable=0;
`ifdef SIMULATE
initial if ($test$plusargs("predictable")) predictable=1;
`endif

reg [7:0] config_data=0;
always @(posedge clk) config_data <= predictable ? config_data_predictable : config_data_romx;

// Pacing counter
//  input comes from test pattern generator, we want to trigger
//  one frame (16 words) for every 16 input pulses.
reg [3:0] pace_cnt=0;
reg stream_trig_row=0;
always @(posedge clk) begin
	if (pace) pace_cnt <= pace_cnt+1;
	stream_trig_row <= pace & (&pace_cnt);
end

// Cycle counter
reg [55:0] stream_cnt0=0, stream_cnt1=0;
wire stream_gate_p = stream_trig_row || (|stream_cnt0[3:0]);
always @(posedge clk) begin
	if (stream_gate_p) stream_cnt0 <= stream_cnt0+1;
	stream_cnt1 <= stream_cnt0;
end
wire [15:0] frame_addr=stream_cnt1[19:4];

wire [7:0] test1=8'hde;
wire [11:0] test2=12'hadb;
wire [3:0] test3=4'hf;

reg [7:0] checksum=0;
reg stream_gate_q=0, stream_gate_r=0, stream_gate_s=0;
reg frame_end_pulse=0, slow_op_r=0, slow_snap_r=0;
reg [7:0] reg_value=0;
reg [15:0] stream_value=0;
reg [11:0] stream_cnt2=0;
wire [6:0] shadow_raddr = {stream_cnt1[14:12],stream_cnt1[9:6]};
always @(posedge clk) begin
	if (stream_gate_s) begin
		checksum<= (stream_cnt2[11:0]==12'b0)?8'b0:(checksum+stream_value[15:8]+stream_value[7:0]);
	end
	stream_gate_q <= stream_gate_p;
	stream_gate_r <= stream_gate_q;
	stream_gate_s <= stream_gate_r;
	stream_cnt2 <= stream_cnt1[11:0];
	frame_end_pulse <= stream_cnt0[3:0]==15;
	casex (frame_addr)
		16'bxxxx_xx00_0000_0000: begin slow_op_r <= frame_end_pulse; slow_snap_r <= 1; end
		16'hxx5x:                begin slow_op_r <= frame_end_pulse; slow_snap_r <= 0; end
		16'hxx6x:                begin slow_op_r <= frame_end_pulse; slow_snap_r <= 0; end
		default:                 begin slow_op_r <= 0;               slow_snap_r <= 0; end
	endcase
	casex (frame_addr)
// Want this case statement auto-generated from reg_map.ini
		16'hxx00: reg_value<= stream_cnt1[55:48];
		16'hxx01: reg_value<= stream_cnt1[47:40];
		16'hxx02: reg_value<= stream_cnt1[39:32];
		16'hxx03: reg_value<= stream_cnt1[31:24];
		16'hxx04: reg_value<= stream_cnt1[23:16];
		16'hxx05: reg_value<= stream_cnt1[15:8];
		16'hxx06: reg_value<= {wave_sel_1,wave_sel_2};
		16'hxx07: reg_value<= {wave_sel_3,wave_sel_4};
		16'hxx08: reg_value<= {wave_sel_5,wave_sel_6};
		16'hxx09: reg_value<= {wave_sel_7,wave_sel_8};
		16'hxx0a: reg_value<= {wave_sel_9,wave_sel_10};
		16'hxx0b: reg_value<= {wave_sel_11,wave_sel_12};
		16'hxx0c: reg_value<= config_data;
		16'hxx0d: reg_value<= config_data;
		16'hxx0e: reg_value<= config_data;
		16'hxx0f: reg_value<= config_data;
		//  xx1x described later, will depend on upper frame_addr bits
		16'hxx2x: reg_value<= dallas_data;
		16'hxx3x: reg_value<= dallas_data;
		16'hxx4x: reg_value<= mcp3208_data;
		16'hxx5x: reg_value<= slow_data;
		16'hxx6x: reg_value<= slow_data;

		// 16'hxx80 through 16'hxxbf, a quarter of the overall stream
		16'bxxxxxxxx10xxxx00: reg_value<= ctrl_if_buf[shadow_raddr][31:24];
		16'bxxxxxxxx10xxxx01: reg_value<= ctrl_if_buf[shadow_raddr][23:16];
		16'bxxxxxxxx10xxxx10: reg_value<= ctrl_if_buf[shadow_raddr][15:8];
		16'bxxxxxxxx10xxxx11: reg_value<= ctrl_if_buf[shadow_raddr][7:0];

		16'hxxf0: reg_value<= stream_cnt1[55:48];
		16'hxxf1: reg_value<= stream_cnt1[47:40];
		16'hxxf2: reg_value<= stream_cnt1[39:32];
		16'hxxf3: reg_value<= stream_cnt1[31:24];
		16'hxxf4: reg_value<= stream_cnt1[23:16];
		16'hxxf5: reg_value<= stream_cnt1[15:8];
		16'hxxf6: reg_value<= {wave_sel_1,wave_sel_2};
		16'hxxf7: reg_value<= {wave_sel_3,wave_sel_4};
		16'hxxf8: reg_value<= {wave_sel_5,wave_sel_6};
		16'hxxf9: reg_value<= {wave_sel_7,wave_sel_8};
		16'hxxfa: reg_value<= {wave_sel_9,wave_sel_10};
		16'hxxfb: reg_value<= {wave_sel_11,wave_sel_12};
		16'hxxfc: reg_value<= app_config[15:8];
		16'hxxfd: reg_value<= app_config[7:0];
		16'hxxfe: reg_value<= checksum[7:0];//15:8];
		16'hxxff: reg_value<= checksum[7:0];//+stream_value[15:8]+stream_value[7:0];

		16'hx010: reg_value<= test1;
		16'hx011: reg_value<= test2[11:4];
		16'hx012: reg_value<= {test2[3:0],test3};
		16'hx013: reg_value<= 8'hee;
		16'hx014: reg_value<= {4'b0,frequency[27:24]};
		16'hx015: reg_value<= frequency[23:16];
		16'hx016: reg_value<= frequency[15:8];
		16'hx017: reg_value<= frequency[7:0];
		16'hx03x: reg_value<= 8'hde;

		16'hxxe0: reg_value<= stream_cnt1[39:32];
		16'hxxe1: reg_value<= stream_cnt1[31:24];
		16'hxxe2: reg_value<= stream_cnt1[23:16];
		16'hxxe3: reg_value<= stream_cnt1[15:8];
		16'hxxe4: reg_value<= stream_cnt1[7:0];

		default: reg_value<= stream_cnt1[15:8];//8'h50;
	endcase
end

wire [15:0] reg_wvalue = {reg_value,stream_cnt2[11:4]};
reg reg_choose=0;
always @(posedge clk) if (stream_gate_r) begin
	reg_choose <= stream_cnt1[3:0]==15;
	stream_value <= reg_choose ? reg_wvalue : dsp_data;
end

// Signals that drive the (upstream) DSP module
assign stream_gate = stream_gate_p;
assign stream_addr = stream_cnt0[15:0];
assign slow_op = slow_op_r;
assign slow_snap = slow_snap_r;

// Results for the (downstream) communications channel (e.g., USB or Ethernet)
assign stream_strobe = stream_gate_s;
assign prod_stream = stream_value;
endmodule
