module fnal_pab(// ad5781 parents 
output  SCLK
,output  SDIO
,output  SYNC
,output EN
,output  busy
,input  clk
,output  sdio_as_sdo
,input  sdo
,output [ADDR_WIDTH-1:0] sdo_addr
,input [ADDR_WIDTH-1:0] spi_addr
,input [DATA_WIDTH-1:0] spi_data
,output [DATA_WIDTH-1:0] spi_rdbk
,input  spi_read
,output  spi_ready
,input  spi_start
);
parameter TSCKHALF=11;
parameter ADDR_WIDTH=4;
parameter DATA_WIDTH=20;
assign EN=1'b1;
ad5781#(.TSCKHALF(TSCKHALF),.ADDR_WIDTH(4),.DATA_WIDTH(20))
ad5781(.SCLK(SCLK),.SDIO(SDIO),.SYNC(SYNC)
,.busy(busy)
,.clk(clk)
,.sdio_as_sdo(sdio_as_sdo)
,.sdo(sdo)
,.sdo_addr(sdo_addr)
,.spi_addr(spi_addr)
,.spi_data(spi_data)
,.spi_rdbk(spi_rdbk)
,.spi_read(spi_read)
,.spi_ready(spi_ready)
,.spi_start(spi_start)
);
endmodule
