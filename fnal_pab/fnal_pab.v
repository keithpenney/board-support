module fnal_pab(output SCLK
,output CSN
,output MOSI
,input clk
,input rst_n
,input enable
,input ext_spi_clk
,input trig_in
,output module_busy
,input sig_in_valid
,input [19:0] sig_in
,input dac_mode
);
parameter SIG_WIDTH=18;
parameter SPI_WIDTH=24;
parameter NUM_DEVICES=2;
parameter DAC_CLK_DIV_VAL=8;
parameter USE_EXTERNAL_SPI_CLK=0;
parameter DAC_RST_VALUE=20'b0;

reg [24:0] clk_counter=25'd0;
reg [17:0] reg_input=18'd0;
reg rstn=1;
reg flag_rst=0;
reg dac_trig=0;
wire ext_spi_clk,sig_valid;
wire module_busy;
wire [1:0] dac_select;
reg dac_enable=1'b0;
wire [1:0] debug_control;
wire [19:0] sig_in={dac_select,reg_input};

wire mosi_new,sclk_new,flag_led,clk_led;
wire [1:0] csn_new;

assign L3N = mosi_new;
assign L3P = sclk_new;
//assign L3P = clk_counter[5];
assign L2N = csn_new[0];
assign ext_spi_clk = 1'b0;
//assign dac_enable = 1'b0;
assign sig_valid = 1'b1;
assign dac_select = 2'b11;
assign debug_control = 1'b0;
assign flag_led = dac_enable;
assign clk_led = clk_counter[20];

always @(posedge clk) begin
	clk_counter<=clk_counter+1'b1;
	if ((clk_counter[12]==1) && (module_busy==0) && (dac_trig==0)) dac_trig<=1;
	if ((clk_counter[12]==0) && (dac_trig==1)) dac_trig<=0;
	if (clk_counter[16:0]==17'd0) begin
		reg_input[17:15]<=reg_input[17:15]+1'b1;
		end
	if ((clk_counter[19:0]==20'd0) && (flag_rst==1) && (rstn==1)) dac_enable<=1'b1;
	if ((clk_counter[18:17]==2'b10) && (flag_rst==0)) begin
		rstn <=0;
		flag_rst<=1;
	end
	if ((clk_counter[18:17]==2'b11) && (rstn==0)) rstn <=1;
end

dac_manager #(.SIG_WIDTH(18),.SPI_WIDTH(24),.NUM_DEVICES(2),.DAC_CLK_DIV_VAL(8),.USE_EXTERNAL_SPI_CLK(0),.DAC_RST_VALUE(20'b0))
slow_dac1(.clk(clk), .rst_n(rstn),
	.enable(dac_enable), .ext_spi_clk(ext_spi_clk), .trig_in(dac_trig),
	.module_busy(module_busy), .sig_in_valid(1'b1), .sig_in(sig_in),
	.debug_mode(debug_control), .MOSI(mosi_new), .SCLK(sclk_new), .CSn(csn_new)
);/**/
endmodule
