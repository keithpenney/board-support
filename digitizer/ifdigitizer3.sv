interface if_digitizer();

/*wire [7:0] U18_spi_addr, U18_sdo_addr;
wire [23:0] U18_spi_data, U18_spi_rdbk;
wire U18_clkin, U18_spi_start, U18_spi_read, U18_spi_ready, U18_spi_busy, U18_sdio_as_sdo;
wire U18_sclk_in, U18_mosi_in, U18_ssb_in;
wire U18_ss_in, U18_miso_out;
wire U18_ssb_out;
wire [15:0] U15_spi_addr,U15_spi_data,U15_sdo_addr,U15_spi_rdbk;
wire U15_clk, U15_spi_start, U15_spi_read, U15_spi_ready, U15_spi_busy, U15_sdio_as_sdo;
wire U15_sclk_in, U15_mosi_in, U15_ssb_in;
wire U15_ss_in, U15_miso_out;
wire U15_sclk_out, U15_mosi_out, U15_ssb_out;
*/
wire U15_ss;
wire U15_miso;
wire U15_mosi;
wire U15_sclk;
wire U18_CLK;
wire U18_CS;
wire U18_DOUT_RDY;
wire U18_SCLK;
wire U18_MOSI;
wire U27dir;
wire U33U1_pwr_sync,U33U1_pwr_en;
//wire U18_sclk_out;
//wire U18_mosi_out;  //  to  application_top
//wire U15_U18_sclk;
//wire U15_U18_mosi;  // from application_top
//wire U18_clk_in;  // from application_top (for 64kHz or 128kHz CLK pin)
wire U1_datauwire_inout,U1_leuwire_in;
wire U1_clkuwire_in; 
wire U1_clkout3;
wire U2_csb_in;
wire U3_csb_in;
wire U2_sclk_in;    // this is used as the real source of P2_SCLK bus_digitizer_U4[26]
wire U3_sclk_in;  // fake, U3 CLKUWIRE attached to shared P2_SCLK
wire [15:0] U2DA, U2DB, U2DC, U2DD;
wire [15:0] U3DA, U3DB, U3DC, U3DD;
wire [127:0] adc_data = {U2DD, U2DC, U2DB, U2DA, U3DD, U3DC, U3DB, U3DA};
wire U2_clk_div_bufr,U3_clk_div_bufr;
wire U2_clk_div_bufg,U3_clk_div_bufg;
wire [39:0] U2_idelay_value_in;
wire [39:0] U3_idelay_value_in;
wire [39:0] U2_idelay_value_out;
wire [39:0] U3_idelay_value_out;
wire [7:0] U2_bitslip;
wire [7:0] U3_bitslip;
wire [7:0] U2_idelay_ld;
wire [7:0] U3_idelay_ld;
wire U2_pdwn,U3_pdwn;
wire U2_iserdes_reset,U3_iserdes_reset;
wire U2_clk_reset,U3_clk_reset;
wire mmcm_reset, mmcm_locked;
wire U23sdio_miso,U23sdio_mosi,U23sdio_as_mosi;
wire U2_mmcm_psclk, U2_mmcm_psen, U2_mmcm_psincdec, U2_mmcm_psdone;
wire U3_mmcm_psclk, U3_mmcm_psen, U3_mmcm_psincdec, U3_mmcm_psdone;
wire U4_csb_in,U4_sdo_out,U4_sdio_inout;
wire U4_sclk_in;  // fake, U4 SCLK attached to shared P2_SCLK
wire U4_dco_clk_out,U4_dci,U4_reset;
wire [13:0] U4_data_i,U4_data_q;
wire [3:0] J17_pmod_4321;
wire [3:0] J17_pmod_a987;
wire [3:0] J18_pmod_4321;
wire [3:0] J18_pmod_a987;
wire [5:0] J19_hdmi_data;
wire [5:0] J19_hdmi_ctrl;

modport cfg(inout J17_pmod_4321,J17_pmod_a987,J18_pmod_4321,J18_pmod_a987,J19_hdmi_ctrl,J19_hdmi_data
,output U15_ss,U15_mosi,U15_sclk,U18_CLK,U18_CS,U18_SCLK,U18_MOSI
//U15_U18_mosi,U15_U18_sclk,U15_clk,U15_mosi_in,U15_sclk_in,U15_sdio_as_sdo,U15_sdo_addr,U15_spi_addr,U15_spi_data,U15_spi_read,U15_spi_start,U15_ss_in,U15_ssb_in,U18_clk_in,U18_clkin,U18_mosi_in,U18_sclk_in,U18_spi_addr,U18_spi_data,U18_spi_read,U18_spi_start,U18_ss_in,U18_ssb_in,
,output U1_clkuwire_in,U1_datauwire_inout,U1_leuwire_in,U2_bitslip,U2_clk_reset,U2_csb_in,U2_idelay_ld,U2_idelay_value_in,U2_iserdes_reset,U2_mmcm_psclk,U2_mmcm_psen,U2_mmcm_psincdec,U2_pdwn,U2_sclk_in,U33U1_pwr_en,U33U1_pwr_sync,U3_bitslip,U3_clk_reset,U3_csb_in,U3_idelay_ld,U3_idelay_value_in,U3_idelay_value_out,U3_iserdes_reset,U3_mmcm_psclk,U3_mmcm_psen,U3_mmcm_psincdec,U3_pdwn,U3_sclk_in,U4_csb_in,U4_data_i,U4_data_q,U4_dci,U4_reset,U4_sclk_in,U4_sdio_inout,mmcm_reset
//U15_miso_out,U15_spi_busy,U15_spi_rdbk,U15_spi_ready,U15_mosi_out,U15_sclk_out,U15_ssb_out,U18_miso_out,U18_mosi_out,U18_sclk_out,U18_sdio_as_sdo,U18_sdo_addr,U18_spi_rdbk,U18_spi_busy,U18_spi_ready,U18_ssb_out,
,input 	U1_clkout3,U2_clk_div_bufg,U2_clk_div_bufr,U2DA,U2DB,U2DC,U2DD,U2_idelay_value_out,U2_mmcm_psdone,U3_mmcm_psdone,U3_clk_div_bufg,U3_clk_div_bufr,U3DA,U3DB,U3DC,U3DD,U4_dco_clk_out,U4_sdo_out,mmcm_locked,adc_data
,input U15_miso,U18_DOUT_RDY
,output U23sdio_miso
,output U23sdio_mosi,U23sdio_as_mosi
);
modport driver(input U1_clkout3
	);
modport hw(inout J17_pmod_4321,J17_pmod_a987,J18_pmod_4321,J18_pmod_a987,J19_hdmi_ctrl,J19_hdmi_data
,input U15_ss,U15_mosi,U15_sclk,U18_CLK,U18_CS,U18_SCLK,U18_MOSI
//U15_U18_mosi,U15_U18_sclk,U15_clk,U15_mosi_in,U15_sclk_in,U15_sdio_as_sdo,U15_sdo_addr,U15_spi_addr,U15_spi_data,U15_spi_read,U15_spi_start,U15_ss_in,U15_ssb_in,U18_clk_in,U18_clkin,U18_mosi_in,U18_sclk_in,U18_spi_addr,U18_spi_data,U18_spi_read,U18_spi_start,U18_ss_in,U18_ssb_in,
,input U1_clkuwire_in,U1_datauwire_inout,U1_leuwire_in,U2_bitslip,U2_clk_reset,U2_csb_in,U2_idelay_ld,U2_idelay_value_in,U2_iserdes_reset,U2_mmcm_psclk,U2_mmcm_psen,U2_mmcm_psincdec,U2_pdwn,U2_sclk_in,U33U1_pwr_en,U33U1_pwr_sync,U3_bitslip,U3_clk_reset,U3_csb_in,U3_idelay_ld,U3_idelay_value_in,U3_idelay_value_out,U3_iserdes_reset,U3_mmcm_psclk,U3_mmcm_psen,U3_mmcm_psincdec,U3_pdwn,U3_sclk_in,U4_csb_in,U4_data_i,U4_data_q,U4_dci,U4_reset,U4_sclk_in,U4_sdio_inout,mmcm_reset
//U15_miso_out,U15_spi_busy,U15_spi_rdbk,U15_spi_ready,U15_mosi_out,U15_sclk_out,U15_ssb_out,U18_miso_out,U18_mosi_out,U18_sclk_out,U18_sdio_as_sdo,U18_sdo_addr,U18_spi_rdbk,U18_spi_busy,U18_spi_ready,U18_ssb_out,
,output	U1_clkout3,U2_clk_div_bufg,U2_clk_div_bufr,U2DA,U2DB,U2DC,U2DD,U2_idelay_value_out,U2_mmcm_psdone,U3_mmcm_psdone,U3_clk_div_bufg,U3_clk_div_bufr,U3DA,U3DB,U3DC,U3DD,U4_dco_clk_out,U4_sdo_out,mmcm_locked,adc_data
,output U15_miso,U18_DOUT_RDY
,input U23sdio_miso
,output U27dir
,input U23sdio_mosi,U23sdio_as_mosi
);
modport sim(inout J17_pmod_4321,J17_pmod_a987,J18_pmod_4321,J18_pmod_a987,J19_hdmi_ctrl,J19_hdmi_data
,output 
//U15_U18_mosi,U15_U18_sclk,U15_clk,U15_mosi_in,U15_sclk_in,U15_sdio_as_sdo,U15_sdo_addr,U15_spi_addr,U15_spi_data,U15_spi_read,U15_spi_start,U15_ss_in,U15_ssb_in,U18_clk_in,U18_clkin,U18_mosi_in,U18_sclk_in,U18_spi_addr,U18_spi_data,U18_spi_read,U18_spi_start,U18_ss_in,U18_ssb_in,
	U1_clkuwire_in,U1_datauwire_inout,U1_leuwire_in,U2_bitslip,U2_clk_reset,U2_csb_in,U2_idelay_ld,U2_idelay_value_in,U2_iserdes_reset,U2_mmcm_psclk,U2_mmcm_psen,U2_mmcm_psincdec,U2_pdwn,U2_sclk_in,U33U1_pwr_en,U33U1_pwr_sync,U3_bitslip,U3_clk_reset,U3_csb_in,U3_idelay_ld,U3_idelay_value_in,U3_idelay_value_out,U3_iserdes_reset,U3_mmcm_psclk,U3_mmcm_psen,U3_mmcm_psincdec,U3_pdwn,U3_sclk_in,U4_csb_in,U4_data_i,U4_data_q,U4_dci,U4_reset,U4_sclk_in,U4_sdio_inout,mmcm_reset
//U15_miso_out,U15_spi_busy,U15_spi_rdbk,U15_spi_ready,U15_mosi_out,U15_sclk_out,U15_ssb_out,U18_miso_out,U18_mosi_out,U18_sclk_out,U18_sdio_as_sdo,U18_sdo_addr,U18_spi_rdbk,U18_spi_busy,U18_spi_ready,U18_ssb_out,
,input U1_clkout3,U2_clk_div_bufg,U2_clk_div_bufr,U2DA,U2DB,U2DC,U2DD,U2_idelay_value_out,U2_mmcm_psdone,U3_mmcm_psdone,U3_clk_div_bufg,U3_clk_div_bufr,U3DA,U3DB,U3DC,U3DD,U4_dco_clk_out,U4_sdo_out,mmcm_locked,adc_data
,input U23sdio_miso,U27dir
,output U23sdio_mosi,U23sdio_as_mosi
);

//	);
endinterface

