`timescale 1ns / 1ns
module llspi_host_interface(
input clk
,input [8:0] host_din
,input host_we
,output reg [7:0] host_result
,input pull_fifo
,output [8:0] fdata
,input result_we
,input [7:0] result
// Host read port
,input result_re
,output empty
,output [7:0] status
);
// FIFO of commands from host
wire full, pull_fifo;
wire [8:0] fdata;
parameter infifo_aw=5;
shortfifo #(.dw(9), .aw(infifo_aw)) input_fifo(.clk(clk),
	.full(full), .empty(empty),
	.we(host_we), .din(host_din),
	.re(pull_fifo), .dout(fdata));

// Read results get pushed into this FIFO
wire [7:0] result_unlatched;
wire [3:0] result_count;
shortfifo #(.dw(8), .aw(4)) output_fifo(.clk(clk),
	.we(result_we), .din(result),
	.re(result_re), .dout(result_unlatched),
	.count(result_count));

always @(posedge clk) if (result_re) host_result <= result_unlatched;

// Status register available for host polling
assign status = {3'b0, empty, result_count};

endmodule
