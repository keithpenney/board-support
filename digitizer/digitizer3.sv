module digitizer3#(parameter SIM=0) (iffmc.slavepin P1
,iffmc.slavepin P2
,if_digitizer.hw hw
);
wire U1_SDIO,U1_SCLK;
wire U2_SDIO,U2_SCLK;
wire U3_SDIO,U3_SCLK;
wire U4_SDIO,U4_SCLK;
wire U2_dco_clk_out,U3_dco_clk_out;

assign P2.G31=U4_SCLK|U1_SCLK|U2_SCLK|U3_SCLK;
assign P2.G30=U4_SDIO|U1_SDIO;


//,.CLKOUT4(P2.H4),.CLKOUT4_INV(P2.H5)
lmk01801 digitizer_U1(.CLKOUT3_INV(P1.C15),.CLKOUT3(P1.C14),.LEUWIRE(P2.H25),.DATAUWIRE_IN(P2.G25)
,.DATAUWIRE(U1_SDIO),.CLKUWIRE(U1_SCLK)
,.clkuwire_in(hw.U1_clkuwire_in)
,.datauwire_inout(hw.U1_datauwire_inout)
,.leuwire_in(hw.U1_leuwire_in)
,.clkout(hw.U1_clkout3)
);

ad9653 #(.FLIP_D(8'b11111111),.FLIP_DCO(1'b1),.FLIP_FRAME(1'b1),.BANK_CNT(1))
digitizer_U2(.D0NA(P1.D14),.D0NB(P1.D11),.D0NC(P1.H7),.D0ND(P1.G9),.D0PA(P1.D15),.D0PB(P1.D12),.D0PC(P1.H8),.D0PD(P1.G10),.D1NA(P1.G12),.D1NB(P1.H13),.D1NC(P1.C10),.D1ND(P1.D8),.D1PA(P1.G13),.D1PB(P1.H14),.D1PC(P1.C11),.D1PD(P1.D9),.DCON(P1.G6),.DCOP(P1.G7),.FCON(P1.H10),.FCOP(P1.H11)
,.PDWN(P2.G18),.SYNC(P2.G36),.CSB(P2.G33)

,.SCLK(U2_SCLK)
,.SDIO(U2_SDIO)

,.csb_in(hw.U2_csb_in),
.sclk_in(hw.U2_sclk_in),
.sdi(),//hw.U2_miso),
.sdo(1'b0),//hw.U2_moso),
.sdio_as_i(1'b0),//hw.U2_sdio_as_miso),
.clk_reset(hw.U2_clk_reset),
.mmcm_reset(hw.mmcm_reset),
.mmcm_locked(hw.mmcm_locked),
.mmcm_psclk(hw.U2_mmcm_psclk),
.mmcm_psen(hw.U2_mmcm_psen),
.mmcm_psincdec(hw.U2_mmcm_psincdec),
.mmcm_psdone(hw.U2_mmcm_psdone),
.iserdes_reset(hw.U2_iserdes_reset),
.bitslip(hw.U2_bitslip),
.idelay_ce(8'b0),
.dout({hw.U2DA,hw.U2DB,hw.U2DC,hw.U2DD}),
.clk_div_bufr(hw.U2_clk_div_bufr),
.clk_div_bufg(hw.U2_clk_div_bufg),
.clk_div_in(hw.U2_clk_div_bufr),
.dco_clk_out(U2_dco_clk_out),
.dco_clk_in(U2_dco_clk_out),
.idelay_value_in(hw.U2_idelay_value_in),
.idelay_value_out(hw.U2_idelay_value_out),
.idelay_ld(hw.U2_idelay_ld),
.pdwn_in(hw.U2_pdwn)
);


ad9653 #(.FLIP_D(8'b11111111),.FLIP_DCO(1'b1),.FLIP_FRAME(1'b1),.BANK_CNT(2),.BANK_SEL({2'b0,2'b0,2'b0,2'b0,2'b1,2'b1,2'b1,2'b1}))
digitizer_U3(.D0NA(P1.G24),.D0NB(P1.C22),.D0NC(P1.H19),.D0ND(P1.C18),.D0PA(P1.G25),.D0PB(P1.C23),.D0PC(P1.H20),.D0PD(P1.C19),.D1NA(P1.G21),.D1NB(P1.H25),.D1NC(P1.G18),.D1ND(P1.H16),.D1PA(P1.G22),.D1PB(P1.H26),.D1PC(P1.G19),.D1PD(P1.H17),.DCON(P1.D20),.DCOP(P1.D21),.FCON(P1.H22),.FCOP(P1.H23),.PDWN(P2.G18),.SYNC(P2.G36),.CSB(P2.G34)
,.SDIO(U3_SDIO)
,.SCLK(U3_SCLK)

,.csb_in(hw.U3_csb_in),
.sclk_in(hw.U3_sclk_in),
.sdi(),
.sdo(1'b0),
.sdio_as_i(1'b0),
.clk_reset(hw.U3_clk_reset),
.mmcm_reset(hw.mmcm_reset),
.mmcm_psclk(hw.U3_mmcm_psclk),
.mmcm_psen(hw.U3_mmcm_psen),
.mmcm_psincdec(hw.U3_mmcm_psincdec),
.mmcm_psdone(hw.U3_mmcm_psdone),
.iserdes_reset({hw.U2_iserdes_reset,hw.U3_iserdes_reset}),
.bitslip(hw.U3_bitslip),//[15:8]),
.idelay_ce(8'b0),
.dout({hw.U3DA,hw.U3DB,hw.U3DC,hw.U3DD}),
.clk_div_bufr(hw.U3_clk_div_bufr),
.clk_div_bufg(hw.U3_clk_div_bufg),
.clk_div_in({hw.U2_clk_div_bufr,hw.U3_clk_div_bufr}),
.dco_clk_out(U3_dco_clk_out),
.dco_clk_in({U2_dco_clk_out,U3_dco_clk_out}),
.idelay_value_in(hw.U3_idelay_value_in),
.idelay_value_out(hw.U3_idelay_value_out),
.idelay_ld(hw.U3_idelay_ld),
.pdwn_in(hw.U3_pdwn)
);

ad9781
digitizer_U4(.D0N(P2.C19),.D0P(P2.C18),.D1N(P2.D18),.D1P(P2.D17),.D2N(P2.G16),.D2P(P2.G15),.D3N(P2.D15),.D3P(P2.D14),.D4N(P2.G13),.D4P(P2.G12),.D5N(P2.H20),.D5P(P2.H19),.D6N(P2.H17),.D6P(P2.H16),.D7N(P2.H14),.D7P(P2.H13),.D8N(P2.H11),.D8P(P2.H10),.D9N(P2.D12),.D9P(P2.D11),.D10N(P2.C11),.D10P(P2.C10),.D11N(P2.G10),.D11P(P2.G9),.D12N(P2.G7),.D12P(P2.G6),.D13N(P2.H8),.D13P(P2.H7),.DCIN(P2.C15),.DCIP(P2.C14),.DCON(P2.D21),.DCOP(P2.D20),.RESET(P2.H26),.CSB(P2.H23),.SDO(P2.H22)
,.SDIO(U4_SDIO)
,.SCLK(U4_SCLK)
,.csb_in(hw.U4_csb_in)
,.sclk_in(hw.U4_sclk_in)
,.sdo_out(hw.U4_sdo_out)
,.sdio_inout(hw.U4_sdio_inout)
,.data_i(hw.U4_data_i)
,.data_q(hw.U4_data_q)
,.dco_clk_out(hw.U4_dco_clk_out)
,.dci(hw.U4_dci)
,.reset_in(hw.U4_reset)
);


pmod digitizer_J18(.P1(P1.G30),.P2(P1.G27),.P3(P1.C26),.P4(P1.D26),.P7(P1.G31),.P8(P1.G28),.P9(P1.C27),.P10(P1.D27)
,.pmod_4321(hw.J18_pmod_4321)
,.pmod_a987(hw.J18_pmod_a987)
);
pmod digitizer_J17(.P1(P2.C22),.P2(P2.C23),.P3(P2.D23),.P4(P2.D24),.P7(P2.G21),.P8(P2.G22),.P9(P2.G27),.P10(P2.G28)
,.pmod_4321(hw.J17_pmod_4321)
,.pmod_a987(hw.J17_pmod_a987)
);

hdmi digitizer_J19(.CEC(P1.G33),.CKN(P1.H38),.CKP(P1.H37),.D0N(P1.H35),.D0P(P1.H34),.D1N(P1.H32),.D1P(P1.H31),.D2N(P1.H29),.D2P(P1.H28),.DET(P1.G37),.SCL(P1.G34),.SDA(P1.G36)
,.hdmi_data(hw.J19_hdmi_data)
,.hdmi_ctrl(hw.J19_hdmi_ctrl)
);

NXP_74AVC4T245 digitizer_U27(.DIR(P2.H32),.dirin(hw.U27dir));

TPS62110 digitizer_U33U1(.EN(P2.C27),.SYNC(P2.C26)
,.pwr_sync(hw.U33U1_pwr_sync)
,.pwr_en(hw.U33U1_pwr_en)
);

assign P2.H29=hw.U15_ss;
assign P2.G24=hw.U18_CLK;
assign P2.D26=hw.U18_CS;
assign P2.G19=hw.U18_SCLK|hw.U15_sclk;
assign P2.G37=hw.U18_MOSI|hw.U15_mosi;
assign hw.U15_miso=P2.H28;
assign hw.U18_DOUT_RDY=P2.D27;



IOBUF IOBUF(.O(hw.U23sdio_miso), .T(~hw.U23sdio_as_mosi),.I(hw.U23sdio_mosi),.IO(P2.H31));
assign hw.U27dir=hw.U23sdio_as_mosi;
//assign U23sdio_as_miso=(hw.U2_sdio_as_miso | hw.U3_sdio_as_miso);
//assign hw.U2_miso=U23sdio_miso;
//assign hw.U3_miso=U23sdio_miso;
//assign U23sdio_mosi=hw.U2_mosi|hw.U3_mosi;
/*
wire U23sdio_miso,U23sdio_mosi,U23sdio_as_miso;
generate
if (SIM==1) begin
	assign P2.H31= U23sdio_as_miso ? 1'bz : U23sdio_mosi;
	assign U23sdio_miso = P2.H31;
end
else begin
	IOBUF IOBUF(.O(U23sdio_miso), .T(U23sdio_as_miso),.I(U23sdio_mosi),.IO(P2.H31));
end
endgenerate
assign P2.G31=U4_sclk|U1_clkuwire|U2_sclk|U3_sclk;
assign P2.G30=U4_sdio_mosi|U1_datauwire;

assign U27dir=~U23sdio_as_miso;

assign U23sdio_as_miso=(U2_sdio_as_miso | U3_sdio_as_miso);
assign U2_sdio_miso=U23sdio_miso;
assign U3_sdio_miso=U23sdio_miso;
assign U23sdio_mosi=U2_sdio_mosi|U3_sdio_mosi;
*/

endmodule

module digitizer3_sim (iffmc.slavepin P1 
,iffmc.slavepin P2
,if_digitizer.sim hw);

assign P1.C14=hw.U1_clkout3;
assign P1.C15=~hw.U1_clkout3;

endmodule
