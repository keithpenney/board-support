#set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets digitizer_U1/clk_ibufgds]

create_clock -period 8.8000 -name U1_clkout3     -waveform {0.000 5.000}  [get_ports -of_objects [get_nets -of_objects [ get_pins -hierarchical -filter "NAME=~*P1*C14"]]]
create_clock -period 2.200  -name U2_DCO         -waveform {1.000 2.250} [get_ports -of_objects [get_nets -of_objects [ get_pins -hierarchical -filter "NAME=~*P1*G7"]]] 
create_clock -period 2.200  -name U3_DCO         -waveform {1.000 2.250} [get_ports -of_objects [get_nets -of_objects [ get_pins -hierarchical -filter "NAME=~*P1*D21"]]] 
create_clock -period 4.400  -name U4_DCO         -waveform {1.000 3.500} [get_ports -of_objects [get_nets -of_objects [ get_pins -hierarchical -filter "NAME=~*P1*D20"]]] 
create_generated_clock -name U4_DCI   -source [get_pins {P2\\.C14}] -divide_by 1 -invert [get_ports -of_objects [get_nets -of_objects [ get_pins -hierarchical -filter "NAME=~*P2*C14"]]]







