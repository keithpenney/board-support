module digitizer (
fmclpc P1
,fmchpc P2
,input [0:0] U1_clkuwire_in
,inout [0:0] U1_datauwire_inout
,output [0:0] U1_datauwire_in
,input [0:0] U1_leuwire_in
,output [0:0] U1_clkout3
,input [8-1:0] U2_bitslip
,output [0:0] U2_clk_div_bufg
,input [0:0] U2_clk_reset
,input [0:0] U2_csb_in
,output [8*8-1:0] U2_dout
,input [8-1:0] U2_idelay_ce
,input [8-1:0] U2_idelay_ld
,input [5*8-1:0] U2_idelay_value_in
,output [5*8-1:0] U2_idelay_value_out
,input [1-1:0] U2_iserdes_reset
,output [0:0] U2_mmcm_locked
,input [0:0] U2_mmcm_psclk
,output [0:0] U2_mmcm_psdone
,input [0:0] U2_mmcm_psen
,input [0:0] U2_mmcm_psincdec
,input [0:0] U2_mmcm_reset
,input [0:0] U2_pdwn_in
,input [0:0] U2_sclk_in
,output [0:0] U2_sdi
,input [0:0] U2_sdio_as_i
,input [0:0] U2_sdo
,input [8-1:0] U3_bitslip
,output [0:0] U3_clk_div_bufg
,input [0:0] U3_clk_reset
,input [0:0] U3_csb_in
,output [8*8-1:0] U3_dout
,input [8-1:0] U3_idelay_ce
,input [8-1:0] U3_idelay_ld
,input [5*8-1:0] U3_idelay_value_in
,output [5*8-1:0] U3_idelay_value_out
,input [2-1:0] U3_iserdes_reset
,output [0:0] U3_mmcm_locked
,input [0:0] U3_mmcm_psclk
,output [0:0] U3_mmcm_psdone
,input [0:0] U3_mmcm_psen
,input [0:0] U3_mmcm_psincdec
,input [0:0] U3_mmcm_reset
,input [0:0] U3_pdwn_in
,input [0:0] U3_sclk_in
,output [0:0] U3_sdi
,input [0:0] U3_sdio_as_i
,input [0:0] U3_sdo
,input [0:0] U4_csb_in
,input [13:0] U4_data_i
,input [13:0] U4_data_q
,output [0:0] U4_dco_clk_out
,output [0:0] U4_dco_clk_out2
,input [0:0] U4_reset_in
,input [0:0] U4_sclk_in
,inout [0:0] U4_sdio_inout
,output [0:0] U4_mmcm_locked
,input [0:0] U4_mmcm_psclk
,output [0:0] U4_mmcm_psdone
,input [0:0] U4_mmcm_psen
,input [0:0] U4_mmcm_psincdec
,input [0:0] U4_mmcm_reset
,output [0:0] U4_sdo_out
,input [0:0] U27dir
,input [0:0] U33U1_pwr_sync
,input [0:0] U33U1_pwr_en
,input [0:0] U15_clk
,input [0:0] U15_sclk_in
,output [16-1:0] U15_sdo_addr
,input [16-1:0] U15_spi_addr
,input [16-1:0] U15_spi_data
,output [16-1:0] U15_spi_rdbk
,input [0:0] U15_spi_read
,output [0:0] U15_spi_ready
,input [0:0] U15_spi_start
,input [0:0] U18_adcclk
,input [0:0] U18_clkin
,output [8-1:0] U18_sdo_addr
,input [8-1:0] U18_spi_addr
,input [24-1:0] U18_spi_data
,output [24-1:0] U18_spi_rdbk
,input [0:0] U18_spi_read
,output [0:0] U18_spi_ready
,input [0:0] U18_spi_start
,inout [3:0] J17_pmod_4321
,inout [3:0] J17_pmod_a987
,inout [3:0] J18_pmod_4321
,inout [3:0] J18_pmod_a987
,inout [5:0] J19_hdmi_data
,inout [5:0] J19_hdmi_ctrl
,output [0:0] U3_clk_div_bufr
,output [0:0] U2_clk_div_bufr
,input [0:0] U2_clk_div_in
,input [0:0] U3_clk_div_in
);
parameter DEBUG="true";
parameter BUF_AW=13;
parameter cmoc_circle_aw=13;

wire [0:0] U2_dco_clk_out;
wire [0:0] U3_dco_clk_out;


lmk01801 digitizer_U1(.CLKOUT3_INV(P1.LA10_N),.CLKOUT3(P1.LA10_P)
,.CLKUWIRE(P2.LA29_N),.DATAUWIRE(P2.LA29_P),.LEUWIRE(P2.LA21_P),.DATAUWIRE_IN(P2.LA22_N)
,.clkuwire_in(U1_clkuwire_in),.datauwire_inout(U1_datauwire_inout),.leuwire_in(U1_leuwire_in),.datauwire_in(U1_datauwire_in)
,.clkout(U1_clkout3)
);

ad9653 #(.FLIP_D(8'b11111111),.FLIP_DCO(1'b1),.FLIP_FRAME(1'b1),.BANK_CNT(1)) digitizer_U2(
.D0NA(P1.LA09_P),.D0NB(P1.LA05_P),.D0NC(P1.LA02_P),.D0ND(P1.LA03_P),.D0PA(P1.LA09_N),.D0PB(P1.LA05_N),.D0PC(P1.LA02_N),.D0PD(P1.LA03_N),.D1NA(P1.LA08_P),.D1NB(P1.LA07_P),.D1NC(P1.LA06_P),.D1ND(P1.LA01_P_CC),.D1PA(P1.LA08_N),.D1PB(P1.LA07_N),.D1PC(P1.LA06_N),.D1PD(P1.LA01_N_CC),.DCON(P1.LA00_P_CC),.DCOP(P1.LA00_N_CC),.FCON(P1.LA04_P),.FCOP(P1.LA04_N),.PDWN(P2.LA16_P),.SYNC(P2.LA33_P),.CSB(P2.LA31_P),.SCLK(P2.LA29_N),.SDIO(P2.LA28_P)
,.bitslip(U2_bitslip),.clk_div_bufg(U2_clk_div_bufg),.clk_div_bufr(U2_clk_div_bufr),.clk_div_in(U2_clk_div_in),.clk_reset(U2_clk_reset),.csb_in(U2_csb_in),.dco_clk_in(U2_dco_clk_out),.dco_clk_out(U2_dco_clk_out),.dout(U2_dout),.idelay_ce(U2_idelay_ce),.idelay_ld(U2_idelay_ld),.idelay_value_in(U2_idelay_value_in),.idelay_value_out(U2_idelay_value_out),.iserdes_reset(U2_iserdes_reset),.mmcm_locked(U2_mmcm_locked),.mmcm_psclk(U2_mmcm_psclk),.mmcm_psdone(U2_mmcm_psdone),.mmcm_psen(U2_mmcm_psen),.mmcm_psincdec(U2_mmcm_psincdec),.mmcm_reset(U2_mmcm_reset),.pdwn_in(U2_pdwn_in),.sclk_in(U2_sclk_in),.sdi(U2_sdi),.sdio_as_i(U2_sdio_as_i),.sdo(U2_sdo));
//,.clk_div_in(U4_dco_clk_out2)
ad9653 #(.FLIP_D(8'b11111111),.FLIP_DCO(1'b1),.FLIP_FRAME(1'b1),.BANK_CNT(2),.BANK_SEL({2'b0,2'b0,2'b0,2'b0,2'b1,2'b1,2'b1,2'b1})) digitizer_U3(
.D0NA(P1.LA22_P),.D0NB(P1.LA18_P_CC),.D0NC(P1.LA15_P),.D0ND(P1.LA14_P),.D0PA(P1.LA22_N),.D0PB(P1.LA18_N_CC),.D0PC(P1.LA15_N),.D0PD(P1.LA14_N),.D1NA(P1.LA20_P),.D1NB(P1.LA21_P),.D1NC(P1.LA16_P),.D1ND(P1.LA11_P),.D1PA(P1.LA20_N),.D1PB(P1.LA21_N),.D1PC(P1.LA16_N),.D1PD(P1.LA11_N),.DCON(P1.LA17_P_CC),.DCOP(P1.LA17_N_CC),.FCON(P1.LA19_P),.FCOP(P1.LA19_N),.PDWN(P2.LA16_P),.SYNC(P2.LA33_P),.CSB(P2.LA31_N),.SCLK(P2.LA29_N)//,.SDIO(P2.LA28_P)
,.bitslip(U3_bitslip),.clk_div_bufg(U3_clk_div_bufg),.clk_div_bufr(U3_clk_div_bufr),.clk_div_in({U2_clk_div_in,U3_clk_div_in}),.clk_reset(U3_clk_reset),.csb_in(U3_csb_in),.dco_clk_in({U2_dco_clk_out,U3_dco_clk_out}),.dco_clk_out(U3_dco_clk_out),.dout(U3_dout),.idelay_ce(U3_idelay_ce),.idelay_ld(U3_idelay_ld),.idelay_value_in(U3_idelay_value_in),.idelay_value_out(U3_idelay_value_out),.iserdes_reset(U3_iserdes_reset),.mmcm_locked(U3_mmcm_locked),.mmcm_psclk(U3_mmcm_psclk),.mmcm_psdone(U3_mmcm_psdone),.mmcm_psen(U3_mmcm_psen),.mmcm_psincdec(U3_mmcm_psincdec),.mmcm_reset(U3_mmcm_reset),.pdwn_in(U3_pdwn_in),.sclk_in(U3_sclk_in),.sdi(U3_sdi),.sdio_as_i(U3_sdio_as_i),.sdo(U3_sdo));
//,.clk_div_in({U4_dco_clk_out2,U4_dco_clk_out2})


ad9781 digitizer_U4(.D0N(P2.LA14_N),.D0P(P2.LA14_P),.D1N(P2.LA13_N),.D1P(P2.LA13_P),.D2N(P2.LA12_N),.D2P(P2.LA12_P),.D3N(P2.LA09_N),.D3P(P2.LA09_P),.D4N(P2.LA08_N),.D4P(P2.LA08_P),.D5N(P2.LA15_N),.D5P(P2.LA15_P),.D6N(P2.LA11_N),.D6P(P2.LA11_P),.D7N(P2.LA07_N),.D7P(P2.LA07_P),.D8N(P2.LA04_N),.D8P(P2.LA04_P),.D9N(P2.LA05_N),.D9P(P2.LA05_P),.D10N(P2.LA06_N),.D10P(P2.LA06_P),.D11N(P2.LA03_N),.D11P(P2.LA03_P),.D12N(P2.LA00_N_CC),.D12P(P2.LA00_P_CC),.D13N(P2.LA02_N),.D13P(P2.LA02_P),.DCIN(P2.LA10_N),.DCIP(P2.LA10_P),.DCON(P2.LA17_N_CC),.DCOP(P2.LA17_P_CC),.RESET(P2.LA21_N)
,.CSB(P2.LA19_N),.SDO(P2.LA19_P)
//,.SCLK(P2.LA29_N),.SDIO(P2.LA29_P)
,.dci(1'b0)
,.csb_in(U4_csb_in),.data_i(U4_data_i),.data_q(U4_data_q),.dco_clk_out(U4_dco_clk_out),.reset_in(U4_reset_in),.sclk_in(U4_sclk_in),.sdio_inout(U4_sdio_inout),.sdo_out(U4_sdo_out),.dco_clk_out2(U4_dco_clk_out2)
,.mmcm_locked(U4_mmcm_locked)
,.mmcm_psclk(U4_mmcm_psclk)
,.mmcm_psdone(U4_mmcm_psdone)
,.mmcm_psen(U4_mmcm_psen)
,.mmcm_psincdec(U4_mmcm_psincdec)
,.mmcm_reset(U4_mmcm_reset)
);

NXP_74AVC4T245 digitizer_U27(.DIR(P2.LA28_N),.dirin(U27dir));

TPS62110 digitizer_U33U1(.EN(P2.LA27_N),.SYNC(P2.LA27_P)
,.pwr_sync(U33U1_pwr_sync)
,.pwr_en(U33U1_pwr_en)
);

wire U15_mosi_in=1'b0;
wire U15_spi_ssb_in=1'b1;
wire U15_ss_in=1'bz;
wire U15_sdio_as_sdo=1'b0;
wire U15_sclk_out;
wire U15_mosi_out;
wire U15_miso_out;
wire U15_spi_ssb_out;
wire U18_sclk_out;
wire U18_mosi_out;
wire U18_miso_out;
wire U18_sclk_in=U15_sclk_out;
wire U18_mosi_in=U15_mosi_out;
wire U18_spi_ssb_in=U15_spi_ssb_out;
wire U18_ss_in=1'bz;
wire U18_sdio_as_sdo=1'b0;
wire U18_spi_ssb_out;
assign P2.LA33_N = U18_mosi_out;
assign P2.LA16_N = U18_sclk_out;

amc7823 #(.SPIMODE("chain"),.DEBUG(DEBUG))
digitizer_U15(.ss(P2.LA24_N),.miso(P2.LA24_P),.mosi(U15_mosi_out),.sclk(U15_sclk_out)
,.clk(U15_clk)
,.miso_out(U15_miso_out)
,.mosi_in(U15_mosi_in)
,.sclk_in(U15_sclk_in)
,.sdio_as_sdo(U15_sdio_as_sdo)
,.sdo_addr(U15_sdo_addr)
,.spi_addr(U15_spi_addr)
,.spi_data(U15_spi_data)
,.spi_rdbk(U15_spi_rdbk)
,.spi_read(U15_spi_read)
,.spi_ready(U15_spi_ready)
,.spi_ssb_in(U15_spi_ssb_in)
,.spi_ssb_out(U15_spi_ssb_out)
,.spi_start(U15_spi_start)
,.ss_in(U15_ss_in)
);
ad7794 #(.SPIMODE("chain"),.DEBUG(DEBUG))
digitizer_U18(.CLK(P2.LA22_P),.CS(P2.LA26_P),.DIN(U18_mosi_out),.DOUT_RDY(P2.LA26_N),.SCLK(U18_sclk_out)
,.adcclk(U18_adcclk),.clkin(U18_clkin),.miso_out(U18_miso_out),.mosi_in(U18_mosi_in),.sclk_in(U18_sclk_in),.sdio_as_sdo(U18_sdio_as_sdo),.sdo_addr(U18_sdo_addr),.spi_addr(U18_spi_addr),.spi_data(U18_spi_data),.spi_rdbk(U18_spi_rdbk),.spi_read(U18_spi_read),.spi_ready(U18_spi_ready),.spi_ssb_in(U18_spi_ssb_in),.spi_ssb_out(U18_spi_ssb_out),.spi_start(U18_spi_start),.ss_in(U18_ss_in));


pmod digitizer_J18(.P1(P2.LA18_P_CC),.P10(P2.LA25_N),.P2(P2.LA18_N_CC),.P3(P2.LA23_P),.P4(P2.LA23_N),.P7(P2.LA20_P),.P8(P2.LA20_N),.P9(P2.LA25_P)
,.pmod_4321(J18_pmod_4321)
,.pmod_a987(J18_pmod_a987)
);
pmod digitizer_J17(.P1(P1.LA29_P),.P10(P1.LA26_N),.P2(P1.LA25_P),.P3(P1.LA27_P),.P4(P1.LA26_P),.P7(P1.LA29_N),.P8(P1.LA25_N),.P9(P1.LA27_N)
,.pmod_4321(J17_pmod_4321)
,.pmod_a987(J17_pmod_a987)
);
hdmi digitizer_J19(.CEC(P1.LA28_N),.CKN(P1.LA32_N),.CKP(P1.LA33_P),.D0N(P1.LA30_N),.D0P(P1.LA30_P),.D1N(P1.LA33_N),.D1P(P1.LA31_P),.D2N(P1.LA32_P),.D2P(P1.LA24_P),.DET(P1.LA24_N),.SCL(P1.LA31_N),.SDA(P1.LA28_P)
,.hdmi_data(J19_hdmi_data)
,.hdmi_ctrl(J19_hdmi_ctrl)
);
endmodule
