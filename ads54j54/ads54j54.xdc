# Max sampling rate 500MHz
# 16bits per 2 ns or 8 Gbps, 2-Lanes per channel, 4 Gbps per lane, 32bit per frame
# max global clock is 4Gbps/32bits = 125MHz
# or serial line rate = 10 Gbps, or 5 Gbps per lane, so JESD204 core clock = 5Gbps/40 = 125MHz
create_clock -period 8.0 -name ads54j54_glb_clk [get_ports TI_EVM_GTX_CLKP]

# http://www.ti.com/lit/ug/slau616a/slau616a.pdf
# PG066 Table 6-1
# M: number of channels : 4
# L: number of lanes total : 8
# K: number of frames per multi-frame: 32
# F: number of octets per frame : 2
create_clock -period 4.0 -name ads54j54_gtref_clk [get_ports TI_EVM_CLK_LA0_P]
